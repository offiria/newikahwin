@extends('layouts.default')

@section('content')
<section>
	<div class="container">
	
		<div class="page-header">
			<h1 class="main-title">Sertai progam pengesahan (Verified) vendor iKahwin</h1>
		</div>
	</div>

</section>


<div class="container">
	
	<div class="row">
		<div class="col-md-4">
			<div class="well well-lg"><p>Sessions</p><h2 class="center-block"> 100,118 </h2></div>
		</div>

		<div class="col-md-4">
			<div class="well well-lg"><p>Users</p><h2 class="center-block"> 77,798 </h2></div>
		</div>

		<div class="col-md-4">
			<div class="well well-lg center-block"><p>Page Views</p><h2 class="center-block"> 227,659 </h2></div>
		</div>

		<div class="col-md-12">
			<p>Misi kami ialah untuk membantu vendor-vendor perkahwinan di Malaysia mendapat lebih liputan and jualan. Kami juga ingin membantu bakal-bakal pengantin memilih vendor yang dipercayai dan mengelakkan merake dari ditipu.</p>

			<p>Program pengesahan (Verified) vendor iKahwin membolehkan para peniaga mendapat lebih kepercayaan and membantu bakal pengantin berbelanja dengan tenang</p> 

			<div class="page-header">
				<h2>Kenapa vendor perkahwinan perlu menyertai program ini?</h2>
			</div>

			<div class="page-header">
				<h2>1. Vendor "Page"</h2>
			</div>
			
			<p>Setiap vendor akan mendpat satu laman web untuk mempamerkan perniagaan anda. Laman web ini direka khas supaya anda dapat mempromosikan jenama and produk dengan lebih berkesan.</p>

			<p>
			<img src="{{ asset('assets/img/vendor-sample.png') }}" style="width:100%;border: 2px solid #bdc3c7;padding: 4px;" class="" />
			</p>

			<p>Di setiap laman web, anda boleh memuat naik gambar-gambar produk anda tanpa had.</p>
			<p>Vendor juga boleh mempamerkan pakej-pakej yang sedia ada berserta harga untuk membantu para pelawat.</p>
				

			<div class="page-header">
				<h2>2. Notis melalui SMS</h2>
				<p></p>
			</div>

			<p>
			<img src="{{ asset('assets/img/vendor-sms.jpg') }}" style="width:100%;" class="" />
			</p>

			<p>Bakal-bakal pengantin boleh menghubungi vendor yang disenaraikan di laman web iKahwin secara percuma. Setiap vendor yang telah disahkan boleh menerima sebanyak 30 sms setiap bulan.</p>
		</div>

		
			<div class="col-md-12">
				<div class="page-header">
					<h2>Apa kata vendor kami?</h2>
					<p></p>
				</div>
			</div>
		

		
			<div class="col-md-4">
				<blockquote class="vendor-feedback">Feedback advertorial sebelum ini memang sangat baik. Alhamdulillah jualan meningkat, oleh kerana itu pihak Adnan Catering bersetuju untuk mengiklankan sekali lagi bersama iKahwin.my

					<small>- Adnan Al Ayubi, Al-Ayubi Catering</small>
				</blockquote>
			</div>

			<div class="col-md-4">
				<blockquote class="vendor-feedback">You really help us on Online marketing. The annual subscription is well worth it. Keep up the good work.

					<small>- Alinda, Dewan Perkahwinan IAIS</small>
				</blockquote>
			</div>

			<div class="col-md-4">
				<blockquote class="vendor-feedback">Perniagaan saya yang dahulunya suam-suam kuku, sekarang alhamdulillah, dah makin baik. Insyaallah next year saya sambung mengiklankan di iKahwin.my

					<small>- Norasyikin. Fresh Garden Cake</small>
				</blockquote>
			</div>

			<div class="clearfix"></div>

			<div class="col-md-12">
				<div class="page-header">
					<h2>Saya cuma berniaga kecil-kecilan, bolehkah saya menyertai?</h2>
					<p></p>
				</div>

				<p>Kami pengalu-alukan peniaga-peniaga kecik dan besar. Asalkan anda mempunyai produk yang menarik, telah berniaga lebih dari 6-bulan dan mempunyai alamat yang tetap, anda boleh menyertai program pengesahan iKahwin.</p>
			</div>

			<div class="col-md-12">
				<div class="page-header">
					<h2>Kena bayar ke ni?</h2>
					<p></p>
				</div>

				<p>Peniaga yang menyertai program pengesahan perlu mambayar kos pengesahan sebanyak RM 10 / sebulan ataupun RM 50 untuk 6 bulan. Anda juga dikehendaki mempamerkan pelekat pengesahan program ini di premis perniagaan anda. Jika anda tidak mempunya kedai (untuk mereka yang berkerja di rumah), anda perlu mempamerkan pelekat kereta yang akan kami berikan.</p>

				<p>Bayaran ini akan digunakan oleh kami untuk mempromosikan vendor-vendor yang telah disahkan kepada baka-bakal pengantin</p>
			</div>

			<div class="col-md-12">
				<div class="page-header">
					<h2>Bagaimana caranya untuk saya menyertai?</h2>
					
				</div>
				<p><img src="{{ asset('assets/img/verified-process.png') }}" style="width:100%;" class="" /></p>

				<p>Sila hantarkan email kepada verified@ikahwin.my , atau smsm/WhatsApp kami di nombor 017 - 311 0057. </p>
			</div>

			

			<div class="col-md-6 col-md-offset-3">
				<a href="{{ url('register/vendor') }}" class="btn btn-block btn-lg btn-success">SERTAI KAMI SEKARANG</a>
			</div>
		
	</div>
	
</div>

@stop