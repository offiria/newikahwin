@extends('layouts.default')

@section('content')

<div class="page-title no-title"></div>


@if($user->is('vendor'))
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<div class="stepwizard">
			    <div class="stepwizard-row">
			        <div class="stepwizard-step">
			            <button type="button" class="btn btn-default btn-circle">1</button>
			            <p>Daftar Akaun</p>
			        </div>
			        <div class="stepwizard-step">
			            <button type="button" class="btn btn-primary btn-circle">2</button>
			            <p>Maklumat Vendor</p>
			        </div>
			        <div class="stepwizard-step">
			            <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
			            <p>Pilih Pakej</p>
			        </div> 
			    </div>
			</div>
			</div>					
		</div>
	</div>
@endif

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="inner-col new-complete">

					<div class="listing-header center-block" style="margin-bottom:30px">
						<h1 class="listing-title" style="text-align: center;">{{ $title }}</h1>
						<small></small>
					</div>

					@if($user->is('user'))
					<p class="text-center alert alert-info">Thank you for you registering. @if(empty($user->provider)) Please check your e-mail to activate your account. @endif</p>
					@else
					<p class="text-center alert alert-info">Thank you for joining the iKahwin. Now, let's create your first listing.
					<br />
					</p>

					<h4>Pengesahan Vendor</h4>
					<p>Semua vendor yang mendaftar perlu menjalani proses pengesan yang ringkas. Sila hantarkan salinan dokumen-document berikut ke admin@ikahwin.my atau hantar terus melalui WhatsApp di nombor 016-2386155</p>

					<div class="well">
						<p>1. Salinan borang pendaftran perniagaan</p>
						<p>2. Salinan kad pengenalan (hadapan sahaja)</p>
					</div>

					<a href="{{ url('admin/vendors/create') }}" target="_blank" class="btn btn-lg btn-success btn-block">Let's create your first listing</a>
					@endif

				</div>
			</div>
		</div>
	</div>
</section>

@stop