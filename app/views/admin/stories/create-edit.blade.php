@extends('admin.layouts.default')

@section('content')

<div class="admin-search">
	<form role="form" id="user-filter" action="{{ url('admin/stories')}}">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>
	</form>
</div>


<h2>{{ isset($story) ? 'Edit' : 'New' }} Story  @if(isset($story)) <span class="small"> <a href="{{ url('blog/' . $story->slug) }}" target="_blank"><i class="fa fa-external-link"></i> </a></span> @endif</h2>
<hr>

{{ Form::token() }}

<div class="row">
	<div class="col-md-9">
		@if(isset($story))
		{{ Form::model($story, array('url' => 'admin/stories/' . $story->id . '/edit', 'method' => 'post', 'class' => 'form-lm clearfix', 'id' => 'main-form')) }}
		@else
		{{ Form::open(array('url' => 'admin/stories/create', 'method' => 'post', 'class' => 'form-lm clearfix', 'id' => 'main-form')) }}
		@endif

		<div class="row">
			<div class="col-md-4">
				<div class="form-group {{ Form::errorClass('category_id') }}">
					<label for="" class="control-label">Story Category</label>
					{{ Form::select('category_id', array('' => '-- Select --') + $categories, null, array('class' => 'form-control')) }}
					{{ Form::errorMsg('category_id') }}
				</div>
			</div>

		@if(Auth::user()->is('administrator'))
			<div class="col-md-4">
				<div class="form-group {{ Form::errorClass('capacity') }}">
					<label for="" class="control-label">Contributor</label>
					{{ Form::select('user_id', $owners , Input::old('user_id'), array('class' => 'form-control')) }}
					{{ Form::errorMsg('user_id') }}
				</div>
			</div>
		@endif

			<div class="col-md-4">
				<div class="form-group {{ Form::errorClass('published') }}">
					<label for="" class="control-label">Published</label>
					{{ Form::select('published', array('1' => 'Yes', '0' => 'No'), null, array('class' => 'form-control')) }}
					{{ Form::errorMsg('published') }}
				</div>
			</div>

		</div>

		<div class="">
			<div class="form-group {{ Form::errorClass('title') }}">
			{{ Form::label('title', 'Story Title') }}
			{{ Form::text('title', Input::old('title'), array('class' => 'form-control input-lg')) }}
			{{ Form::errorMsg('title') }}
			</div>
		</div>



		<div class="row">
		<div class="col-md-6">
			<div class="form-group {{ Form::errorClass('metakey') }}">
			{{ Form::label('metakey', 'Meta Keywords') }}
			{{ Form::textarea('metakey', Input::old('metakey'), array('class' => 'form-control nowysiwyg', 'maxlength' =>'255', 'rows' => '3')) }}
			{{ Form::errorMsg('metakey') }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group {{ Form::errorClass('metadesc') }}">
			{{ Form::label('metadesc', 'Meta Description') }}
			{{ Form::textarea('metadesc', Input::old('metadesc'), array('class' => 'form-control nowysiwyg', 'maxlength' =>'255', 'rows' => '3')) }}
			{{ Form::errorMsg('metadesc') }}
			</div>
		</div>
		</div>


		<div class="">
			<div class="form-group {{ Form::errorClass('story') }}">
			{{ Form::label('story', 'Story') }}
			{{ Form::textarea('story', Input::old('story'), array('class' => 'form-control', 'style' => 'resize: vertical;')) }}
			{{ Form::errorMsg('story') }}
			</div>
		</div>

		<div class="">
			<div class="form-group {{ Form::errorClass('excerpt') }}">
			{{ Form::label('excerpt', 'Excerpt') }}
			{{ Form::textarea('excerpt', Input::old('excerpt'), array('class' => 'form-control nowysiwyg', 'maxlength' =>'158', 'rows' => '3')) }}
			{{ Form::errorMsg('excerpt') }}
			</div>
		</div>




		{{--
		<h2>Other images</h2>
		@if(isset($story))
		<ul class="list-unstyled" id="photos-list">
			@foreach($story->photos as $photo)
				@include('admin.common.photolist', ['photo' => $photo])
			@endforeach
		</ul>
		<div id="photo-fine-uploader" class="attachment" data-action="{{ url('/admin/Story/photoUpload') }}/{{$story->id}}"></div>
		@else
		<div class="alert alert-warning" role="alert">You can add/edit the addtional image after you save this entry</div>
		@endif
		--}}

		{{ Form::submit(((isset($story)) ? 'Update' : 'New'), array('class' => 'btn btn-primary btn-lg')) }}
	</div>
	<div class="col-md-3">
		<div class="btn-group" data-toggle="buttons">
			<label class="btn btn-default @if(isset($story) && $story->featured == 1) active @endif">
				{{ Form::checkbox('featured', 1, Input::old('featured')) }} <span class="glyphicon glyphicon-star"></span> Featured
			</label>
		</div>
		<h4>Tags</h4>
		<input type="text" @if(isset($story) && $story->tags) value="{{ implode(',', $story->tags->lists('name')) }}" @endif class="tagsinput" name="tags" autocomplete="off" />

		{{ Form::close() }}

		<h4>Cover thumb</h4>
		@if(isset($story))
		<div class="thumbnail">
			<img style="width:100%" src="{{ asset($story->cover_thumb)}}" alt="" id="cover-thumb">
		</div>
		<div id="cover-fine-uploader" class="attachment" data-action="{{ url('/admin/Story/coverUpload') }}/{{$story->id}}"></div>
		@else
		<div class="alert alert-warning" role="alert">You can add/edit the cover image after you save this entry</div>
		@endif

		
		
		
		

	</div>
</div>

@stop

@if(isset($story))
@section('photo-modal'){{ url("/admin/selectPhoto/story/". $story->id ) }}@stop
@endif

@section('scripts')
	<script>
		$(document).ready(function() {
			// enable wysiwyg editor
			$(function()
			{
			    $('textarea:not(.nowysiwyg)').redactor({
	                focus: true,
	                imageUpload: '{{ url('admin/stories/images/upload') }}',
                	imageManagerJson: '{{ url('/admin/stories/blogimages') }}',
	                plugins: ['fontcolor', 'imagemanager', 'table','video']
	            });
			});

			// Content tagging
			$('.tagsinput').tagsinput({
				typeahead: {
					source: function(query) {
							return $.getJSON('/tags');
						},
					freeInput: false
				}});
		});
	</script>
@stop