<?php

class Photo extends \Eloquent {
	protected $fillable = ['imageable_id', 'imageable_type', 'path', 'album_id', 'caption'];

	public function imageable()
    {
        return $this->morphTo();
    }
}