<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});


// filter cache
Route::filter('cache', function($route, $request, $response = null)
{
    $key = 'route-'.Str::slug(Request::url());
    if(is_null($response) && Cache::has($key))
    {
        return Cache::get($key);
    }
    elseif(!is_null($response) && !Cache::has($key))
    {
        Cache::put($key, $response->getContent(), 30);
    }
});

// newsletter 
Route::filter( 'newsletter_output', function($route, $request, $response = null){

	if(!is_null($response)){
		$content = $response->getContent();
		return $content;
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			Session::put('redirect', URL::full());
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

// Administrator filter
Route::filter('administrator', function()
{
	if(Auth::guest()){
		Session::put('redirect', URL::full());
		return Redirect::guest('login');
	}

	if( ! User::find( Auth::user()->id)->is('administrator')){
		return Redirect::to('/')
			->with('message', 'Only Administrators can access this page');
	}
});

// Managers filter (admin, owner, contributor)
Route::filter('managers', function()
{
	if(Auth::guest()){
		Session::put('redirect', URL::full());
		return Redirect::guest('login');
	}

	if( !( User::find( Auth::user()->id)->is('administrator') || User::find( Auth::user()->id)->is('vendor') || User::find( Auth::user()->id)->is('contributor') )){
		return Redirect::to('/')
			->with('message', 'Only Administrators can access this page');
	}
});

// User filter
Route::filter('user', function()
{
	if(Auth::guest()){
		Session::put('redirect', URL::full());
		return Redirect::guest('login');
	}

	if( ! User::find( Auth::user()->id)->is('user')){
		return Redirect::to('/')
			->with('message', 'Only registered users can access this page');
	}
});


// Pagination with url query
View::composer(Paginator::getViewName(), function($view) {
	$query = array_except( Input::query(), Paginator::getPageName() );
	$view->paginator->appends($query);
});