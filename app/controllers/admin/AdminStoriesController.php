<?php

class AdminStoriesController extends BaseController {

	public function getIndex()
	{
		$query = Story::query();

		if(Input::has('keyword')) {
			$query->where('title', 'LIKE', '%'.Input::get('keyword').'%');
		}

		if(Input::has('user_id') && Input::get('user_id') != 0 ) {
			$query->where('user_id', '=', Input::get('user_id'));
		}

		if( User::find( Auth::user()->id)->is('administrator') ) {
		} else if(User::find( Auth::user()->id)->is('vendor')) {
			$query->where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC');
		} else {
			
		}

		$users = Toddish\Verify\Models\Role::where('name', '=', 'contributor')->first()->users;
		$contributors = ['0' => '-- Contributor --'];
		foreach ($users as $contributor) {
			$contributors[$contributor->id] = $contributor->firstname . ' ' . $contributor->lastName;
		}

		$stories = $query->paginate(20);

		return View::make('admin/stories/index', ['stories' => $stories, 'contributors' => $contributors]);
	}

	public function getCreate()
	{
		$festivals = Vendor::all()->lists('name','id');
		$categories = StoryCategory::all()->lists('name','id');

		$users = Toddish\Verify\Models\Role::where('name', '=', 'contributor')->first()->users;
		
		// convert to arrays
		$owners = [];
		foreach ($users as $owner) {
			$owners[$owner->id] = $owner->firstname . ' ' . $owner->lastName;
		}

		return View::make('admin/stories/create-edit', ['vendors' => $festivals, 'categories' => $categories,
			'owners' => $owners]);
	}

	public function getEdit($id)
	{
		$story = Story::find($id);
		$festivals = Vendor::all()->lists('name','id');
		$categories = StoryCategory::all()->lists('name','id');
		$users = Toddish\Verify\Models\Role::where('name', '=', 'contributor')->first()->users;
		
		// convert to arrays
		$owners = [];
		foreach ($users as $owner) {
			$owners[$owner->id] = $owner->firstname . ' ' . $owner->lastName;
		}

		return View::make('admin/stories/create-edit', ['story' => $story, 'vendors' => $festivals, 
			'categories' => $categories, 'owners' => $owners]);
	}

	public function postEdit($id = null)
	{
		$rules = [
			'title' => 'required'
		];

		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {
			$story = Story::firstOrCreate(['id' => $id]);

			if(empty($input['user_id'])){
				$input['user_id'] = Auth::user()->id;
			}

			// Checkbox unchecked value
			if( ! isset($input['featured'])) {
				$input['featured'] = 0;
			}

			// Save tags
			if(Input::has('tags')) {
				$story->tags()->detach();
				foreach(explode(',', $input['tags']) as $tagName)
				{
					$tagName = trim($tagName);
					$tag = Tag::firstOrNew(['name' => $tagName]);
					$tag->name = $tagName;
					$story->tags()->save($tag);
				}
			}

			$story->update($input);

			if($story->save()) {
				if($id) {
					return Redirect::back()
						->with('message', 'Story updated');
				} else {
					return Redirect::to('admin/stories')
							->with('message', 'The story has been created');
				}
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

	public function getDelete($id)
	{
		Story::destroy($id);

		return Redirect::back()
			->with('message', 'Story deleted');
	}
}