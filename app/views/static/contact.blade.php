@extends('layouts.default')

@section('content')
<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">We'll be happy to hear from you.</h1>
		<small></small>
	</div>

	<div class="row">
		<hr/>
	</div>
</section>


<div class="container">
	<div class="contact">
		<div class="row">
			<div class="col-md-12 contact-content item-content">

				{{--
				<div class="contact-secondary">
					<p>
						Pudding caramels bear claw. Gummi bears sweet gummi bears lollipop. Oat cake sweet roll wafer jujubes jelly-o croissant cotton candy. Cake pudding tootsie roll macaroon ice cream candy canes.
					</p>
				</div>
				--}}

				<div class="container">
	<div class="row">
        <div class="col-md-8">
        	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:350px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.trivoo.net" id="get-map-data">www.trivoo.net</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(2.9206528,101.66152320000003),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(2.9206528, 101.66152320000003)});infowindow = new google.maps.InfoWindow({content:"<b>iTech Tower</b><br/>Cyberjaya, Jalan Impact<br/> Cyberjaya" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
    	
    	</div>


      	<div class="col-md4">
    		<h4>Snail mail</h4>
    		<address>
    			<strong>9-03, iTech Tower</strong><br>
    			Jalan Impact<br>
    			Cyberjaya,<br>
    			63000, Selangor<br>
    			<abbr title="Phone">P:</abbr> 01234 567 890
    		</address>
    	</div>
    </div>
</div>

			</div>

		</div>
	</div>
</div>

@stop