<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Country extends \Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug'];
	protected $fillable = ['name', 'description', 'cover_img', 'traveltips'];

	public function festivals()
	{
		return $this->hasMany('Festival');
	}

	public function scopeActive($query)
    {
        return $query->whereIn('id', DB::table('vendors')->distinct()->lists('country_id'));
    }

    public function scopeFindSlug($query, $slug){
		return $query->where('slug', '=', $slug);
	}
}