<?php

use Gregwar\Image\Image;

class AdminMessagesController extends BaseController {

	public function getIndex()
	{	
		// admin sees everything
		if( User::find( Auth::user()->id)->is('administrator') )
			$messages = TextMessage::all();
		else
			$messages = TextMessage::where('vendor_id', '=', Auth::user()->id);

		return View::make('admin.messages.index', ['messages' => $messages]);
	}

	public function getUnread()
	{
		$roles = Role::all()->lists('name','id');
		return View::make('admin/users/create-edit', ['roles' => $roles]);
	}

	public function getCreate()
	{
		$roles = Role::all()->lists('name','id');
		return View::make('admin/users/create-edit', ['roles' => $roles]);
	}

	public function getEdit($id)
	{
		$user = User::find($id);
		if ($user) {
			$roles = Role::all()->lists('name','id');
			return View::make('admin/users/create-edit', ['user' => $user, 'roles' => $roles]);
		} else {
			return Redirect::to('admin')->with('message', 'User does not exist');
		}
	}

	public function postEdit($id = null)
	{
		$rules = array(
				'firstname' => 'required',
				'email' => 'required|unique:users,email',
				'role' => 'required',
				'new_password' => 'required|confirmed'
			);

		if($id) {
			$rules = array_merge($rules, 
				array(
					'email' => 'required|unique:users,email,' . $id,
					'new_password' => ''
				));
		}

		// dd(Input::all());

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$user = User::FirstOrCreate(['id' => $id]);
			$inputs = Input::all();
			$inputs['description'] = strip_tags($inputs['description']);
			// $inputs['link'] = strip_tags($inputs['link']);
			
			$user->update($inputs);

			if(Input::has('role')) {
				$user->roles()->sync($inputs['role']);
			}

			if(Input::has('new_password')) {
				$user->password = Input::get('new_password');
			}

			if( ! $id) {
				// Verified by default
				$user->verified = 1;
			}

			if($user->save()) {
				
				if($id) {
					return Redirect::back()
						->with('message', 'User updated');
				} else {
					return Redirect::to('admin/users')
						->with('message', 'The user has been created');
				}
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

	public function getAjaxToggle($userId, $field, $value = null)
	{
		$allowedFields = ['verified', 'disabled'];
		$user = User::find($userId);

		if(in_array($field, $allowedFields) && $user) {

			switch ($field) {
				case 'verified':
					if( ! is_null($value) && $value == 'sendmail') {
						// Log::info('Verification e-mail sent to user');
					}
					break;
			}

			$user->$field = ($user->$field == 1) ? 0 : 1;
			$user->save();

			$result = ['success' => 'true', 'status' => $user->$field];
		} else {
			$result = ['success' => 'false'];
		}

		return Response::json($result);
	}

	public function postAvatar($id){
		$user = User::find($id);
		$uploader = new FileUploader();
		$uploaderPath = '/assets/profile_photos'. '/';

		$result = $uploader->handleUpload(public_path() . $uploaderPath);
		// print_r($result); exit;
		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['path'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);

		// resize image
		Image::open(public_path() . $result['path'])
		    ->zoomCrop(350, 350)
		    ->save(public_path().''.$result['path']);

		// delete old photo
		if(!strstr($user->image, 'default')){
	    	@unlink( public_path(). $user->image);
	    }

	    $user->image = $result['path'];
	    $user->save();

		$result['success'] = 1;
		$result['src'] = asset($user->image);

		// for front-end
		return Response::json($result );
	}

}