@extends('layouts.default')

@section('content')

<div class="container">

<h4>After registering, how long will it take for my page to be published on your site?</h4>
<p>We want to post it as soon as you click the submit button, but every day, we're dealing with hundred of listing submitted. Do expect the latest is within 3-4 working days on Silver plan and Gold Plan </p>
 
<h4>Why is my logo incorrect?</h4></h4><p>
 
Sometimes there is some mistakes done by our staff. We apologise if ever it happens to you, note that we are only a small group of people, trying our very best to serve you, if your logo that we published is incorrect, please contact us immediately at admin[at]ikahwin.my and attached together your correct logo and we will upload it as soon as we receive your email. We take our customers seriously and we want to improve to help you in anyway that we could. Help us to make us better. 
 
</p><h4>Why should I pay to be listed at your site?</h4><p>
 
To drive traffic more to the website, we need to spent a lot on other sort of advertising as well. With the database increasing from day to day, the cost for hosting the server also rise.Thus, by accepting your subscription fee, we would be able to deliver a quality content to user and maintain the performance of the website. Besides that, you can list as many services that you have without any additional charges.*limited to GOLD Plan</p><p>
 
Lastly,our customer support is 24/7 around the clock to provide you with any help needed. We can help you to change, or to add any additional info to your listing, if you couldn't make it due to your busy schedule and entertaining your customers.</p><p>
 
We will taking care of your listing like our own business
 
</p><h4>I don't want my pricing to be shown at your site, can you remove it?</h4><p>
NO. It is part of our terms and conditions if you want to be listed with us. As we try our best to provide future brides and grooms with as much information's they need. </p><p>
 
We do understand some of the services are too subjective for you to list down your price, do atleast give us a price range for your packages, so at least potential clients have some idea when they pay a visit to your ads on our website.
 
</p><h4>Can I edit the listing myself?</h4><p>
 
YES.  We will give you your own login ID which you can login and edit at anytime anywhere.
 
</p><h4>I'm interested to sign up for your Silver / Gold plan. What's next?</h4><p>
 
Contact us on Facebook: ikahwin.my, email : info[at]ikahwin.my , or directly SMS / Call us at 017 - 311 0057. We will get back to you and set for an appointment so we can explain to you in details on how can we help you through online marketing by listing on iKahwin.my</p><p>
 
Worry not, we're not MLM people nor are we going to force you to subscribe with our plan at the end of the appointment. If you feel that iKahwin could not help you much. You can always opt to cancel at the end of our presentation.
 
</p><h4>What is the difference between Silver & Gold plan?</h4><p>
 
The major difference for Front Page plan is, you page will appear on our toplisting section. Which will have a higher chances for potential clients to click on, these pages by statistic have the most visits per month. None the less, the premium features that we're offering for both plan are the same.
 
</p><h4>Can I subscribe to you less than a year? (For Premium & Front Page plan)</h4><p>
 
Nope. As our subscription is per year basis. It is much easier for us to manage per year basis compared to month to month basis.
 
</p><h4>I'm not from KL, how do I pay if I'm interested?</h4><p>
 
If youre interested, do let us know by contacting us either through FB, E-mail or Whatsapp. We will guide you from there.
 
</p><h4>I don't have a shop, I'm base at home. But I don't want to list my house address due to privacy. How?</h4><p>
 
No problem, we also have few other vendor such as photographers and home made bakers who are based at home, and does not want their home to be reach by public due to privacy.</p><p>
 
What we suggest is, set a place that you're convenient to meet up with your customer. We will point the GPS Co-Ordinate for specific place chosen, so whenever you want to set for an appointment place, it will be easier for the brides and grooms to find out your meeting place.
 
</p><h4>How do I know if any of my customers are from iKahwin.my? How to know how effective is you website for my ads.</h4><p>
 
Well, we integrate our messaging app with our system, so whenever you received inquiry from the customer, it is always from admin[at]ikahwin.my thats how you will know that the customer is referral from us.</p><p>
 
How effective of the website is depends. Based on our experience people don't just used iKahwin to booked for their wedding caterer and so on, they also used our website to look for caterer to serve for a birthday party and functions at their office.</p>
 
 
<h4>Can I get a refund if I'm no longer interested to be listed? (Less than a year)</h4><p>
 
Unfortunately. NO. 
 
<h4>I don't have nice photos of my product. How?</h4><p>
 
If you're subscribing to our premium plan, we will set for an appointment with you and we will sent our proffessional photographer to take photos of your shop / products
 
We will discuss in person with you on how to make your listing look nice and interesting </p>
 
<h4>I'm listed for free. And I would like to update my packages/photos/logo/company info. Is that possible?</h4>
<p> 
No. Whatever we've keyed in at the first time, it will remain there, unless you want us to take it down for you.</p>


@stop