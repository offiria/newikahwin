@extends('layouts.default')

@section('content')

<div class="container">
	
		<div class="row">
			<div class="col-md-12">

				<div class="inner-content">
					<h1 class="main-title">About Us.</h1>
				</div>
			</div>
		</div>


		<div id="content" class="span12">
					<!-- Begin Content -->
					
					<div class="item-page">
								
	
		
								<div class="row" id="do-i-need-to-be-there-with-my-guests">
	<div class="col-md-6">
		<img src="http://www.ikahwin.my/ALL%20Background/234.png"></div>
	<div class="col-md-6 variable_tall ">
		<h3>
			How much should I spent for the wedding?</h3>
		<p>
			We do know, getting married is a complicated task to be done. Here at iKahwin.my we try to help as much as we can to ease the burden that you carried along the way.</p>
		<p>
			So, we collaborate with one of the local developer to provide you a wedding calculator so you can calculate how much money you needed to fit your budget plan in getting married. Its not the actual expenses, but its like a range of budget you should expect on how much money you should save. Bajetkahwin.my also provide two different calculator for groom and bride. We hope that with this calculator will ease your burden.</p>
		<a href="http://www.ikahwin.my/weddingbudget" target="_blank"> <img alt="" class="cke-resize" height="47" src="http://www.ikahwin.my/ALL%20Background/calculator.png" target="_blank" width="225"> </a></div>
</div>
<hr>
<div class="row" id="how-do-i-get-paid">
	<div class="col-md-6 tall4">
		<h3>
			Budget Guidelines</h3>
		<p>
			Your wedding budget is critical to your wedding planning. Without it spending can get out of hand. Because of its value, the budget should be one of the first decisions you make in your wedding planning process.</p>
		<p>
			Once you've established a budged, it is important that you keep track of what you actually spend compared to what you've budgeted. If you want to hire a vendor that costs more than you've budgeted, then you'll have to cut the cost on another item. Similarly, if you end of spending less than budgeted on an item, you then have a bit more to spend on something else. So here we attach together a guideline in both Malay and English version for most Malay wedding budget.</p>
		<a href="http://www.ikahwin.my/weddingplanner" target="_blank"> <img alt="" height="47" src="http://www.ikahwin.my/ALL%20Background/download.png" width="225"> </a></div>
	<div class="col-md-6">
		<img src="http://ikahwin.my/ALL%20Background/2.jpg"></div>
</div>
<hr>
<div class="row" id="do-i-need-to-be-there-with-my-guests">
	<div class="col-md-6">
		<img src="http://ikahwin.my/ALL%20Background/4.jpg"></div>
	<div class="col-md-6 variable_tall ">
		<h3>
			Who Pays for What</h3>
		<p>
			It's very important to address the issue of who is paying for what elements of the wedding up front. You don't want to assume that someone is footing the bill for a particular item to find out later that they aren't.</p>
		<p>
			Today many couples are paying for the own wedding. For others, the bride's family still covers the cost of most items. But sometimes the groom's family is willing to contribute to the expenses. Your best bet is to meet with everyone who is sharing the expenses early to find the solution that works best for you. Once you determine who can afford what you can then set your budget.</p>
		<a href="http://www.ikahwin.my/weddingplanner" target="_blank"> <img alt="" height="47" src="http://www.ikahwin.my/ALL%20Background/download.png" width="225"> </a></div>
</div>
<hr>
<div class="row" id="what-about-safety-security-and-privacy">
	<div class="col-md-6">
		<h3>
			We provide you complete checklist to ease your burden</h3>
		<p>
			Many of you sometimes lost because do not know where to start as so many things to be done. Worry not, we understand your stress and worried of stuff that might not completed before the day come. So, we made you a checklist.</p>
		<p>
			Planning a wedding can be very stressful as there are tons of details to take care of. The most important thing is stay organized. Our planning checklists help you understand what you need to do and when.<br>
			<br>
			<a href="http://www.ikahwin.my/weddingplanner" target="_blank"> <img alt="" height="47" src="http://www.ikahwin.my/ALL%20Background/download.png" width="225"> </a></p>
	</div>
	<div class="col-md-6">
		<img src="http://ikahwin.my/ALL%20Background/3.jpg">
		<div class="guarantee">
			&nbsp;</div>
	</div>
</div>
<div id="ckimgrsz" style="left: 544.328125px; top: 192px;">
	<div class="preview">
		&nbsp;</div>
</div>
 
	<!-- Disques -->
		<!-- End discuss -->

	
						 </div>

					
<form action="/wedding-checklist" method="post" id="login-form" class="">
<div class="modal hide fade" id="loginModal" style="width: 828px;margin: -250px 0 0 -420px;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <img src="http://www.ikahwin.my/images/signup_header.png" style="margin-left: 220px">
  </div>
  <div class="modal-body">
    <div class="row">
	<div class="span4" style="height: 217px;;">
<img src="http://www.ikahwin.my/images/signup_side.png" style="
    margin: -110px -0px -0px -20px;
    max-width: 270px;
	position:absolute;
">
	</div>
    	<div class="span4">
		<div class="userdata">
				<div class="control-group">
				    <label for="modlgn-username" class="">User Name</label>
				    <div class="controls">
				      <input id="modlgn-username" type="text" name="username" class="input-large" tabindex="0" size="18" placeholder="User Name">
				    </div>
				</div>

				<div class="control-group">
				    <label for="modlgn-passwd" class="">Password</label>
				    <div class="controls">
				      <input id="modlgn-passwd" type="password" name="password" class="input-large" tabindex="0" size="18" placeholder="Password">
				    </div>
				</div>

			

				<div id="form-login-remember" class="control-group checkbox">
			<label for="modlgn-remember" class="control-label">Remember Me</label> 
			<input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes" style="margin-top: -20px;">
		</div>
				<div id="form-login-submit" class="control-group">
			<div class="controls">
				
			</div>
		</div>
					<!--
			<ul class="unstyled">
				<li>
					<a href="/login?view=remind">
					  Forgot your username?</a>
				</li>
				<li>
					<a href="/login?view=reset">Forgot your password?</a>
				</li>
			</ul>
			-->
		<link rel="stylesheet" href="http://ikahwin.my/components/com_jfbconnect/assets/jfbconnect.css" type="text/css"><div class="jfbclogin"><span class="sourcecoast"><div class="social-login row"><div class="jfbcLogin pull-left"><a class="show" id="sc_fblogin" href="javascript:void(0)" onclick="jfbc.login.facebook();"><img src="http://ikahwin.my/media/sourcecoast/images/provider/button_facebook.png"></a></div></div></span></div> 
		<input type="hidden" name="option" value="com_users">
		<input type="hidden" name="task" value="user.login">
		<input type="hidden" name="return" value="aW5kZXgucGhwP0l0ZW1pZD00OTU=">
		<input type="hidden" name="53db6e963932e0fa44e259e87fd8ff74" value="1">	</div>
			</div>
		<div class="span4" style="border-left:1px; solid #DDD">
 <img width="275" src="http://www.ikahwin.my/images/signup_why.png">

		</div>
	</div>

  </div>
  <div class="modal-footer">
	<ul class="unstyled pull-left" style="margin: 0px;margin-left: 260px;">
				<li>
					<a href="/login?view=remind">
					  Forgot your username?</a>
				</li>
				<li>
					<a href="/login?view=reset">Forgot your password?</a>
				</li>
			</ul>
    <button href="#" class="btn btn-primary" type="submit" tabindex="0" name="Submit">Log in</button>
    <a href="/login?view=registration" class="btn btn-info">Create an account</a>
  </div>
</div>
</form>


					<!-- End Content -->
				</div>

</div>

@stop