@extends('layouts.default')

@section('content')

{{ Form::open(array('url' => 'festivals/submit', 'class' => '', 'method' => 'post')) }}

	{{ Form::token() }}

	<div class="form-group {{ Form::errorClass('name') }}">
		{{ Form::label('name', 'Name of Festival', array('class' => 'control-label')) }}
		{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
		{{ Form::errorMsg('name') }}
	</div>

	<div class="form-group {{ Form::errorClass('website') }}">
		{{ Form::label('website', 'Festival website', array('class' => 'control-label')) }}
		{{ Form::text('website', Input::old('website'), array('class' => 'form-control')) }}
		{{ Form::errorMsg('website') }}
	</div>

	<div class="form-group form-action">
		{{ Form::submit('Continue', array('class' => 'btn btn-success btn-lg')) }}
	</div>

{{ Form::close() }}

@stop