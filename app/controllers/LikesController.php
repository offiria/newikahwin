<?php

class LikesController extends BaseController {


	public function getIndex()
	{
		$user = User::find(Auth::user()->id);
		$vendors = $user->likes()->paginate(50);
		return View::make('likes.index', ['vendors' => $vendors, 'pageTitle' => 'Vendor-vendor pilihan anda', 'queryString' => '']);
	}

	public function postLike($type, $id)
	{
		$user = Auth::user();

		if(empty($user)){
			App::abort(403, 'Unauthorized action.');
		}
		Like::where('likeable_type', '=', $type)
			->where('likeable_id', '=', $id)
			->where('user_id', '=', $user->id)->delete();

		Like::create(['likeable_type' =>  $type,
			'likeable_id' => $id,
			'user_id' => $user->id ]);

		$data = ['ajax' => url('likes/unlike/Vendor/'.$id) ];
		return Response::json($data);
	}

	public function postUnlike($type, $id)
	{
		$user = Auth::user();
		Like::where('likeable_type', '=', $type)
			->where('likeable_id', '=', $id)
			->where('user_id', '=', $user->id)->delete();
		$data = ['ajax' => url('likes/like/Vendor/'.$id) ];
		return Response::json($data);
	}
}
