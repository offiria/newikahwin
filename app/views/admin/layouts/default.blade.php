<!DOCTYPE html>

<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="iKahwin Team">
	
	<link rel="shortcut icon" type="image/ico" href="{{{ asset('assets/img/favicon.png') }}}">
	<link rel="apple-touch-icon" href="#">
	<link rel="apple-touch-icon" sizes="144x144" href="#">
	<link rel="apple-touch-icon" sizes="114x114" href="#">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/bootstrap.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/font-awesome/css/font-awesome.min.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/js/datepicker/css/datepicker3.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/js/tagsinput/bootstrap-tagsinput.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/gotham-font/gt.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/style.css') }}}" media="screen">
	<link rel="stylesheet" href="{{asset('bootstrapvalidator/css/bootstrapValidator.css') }}"/>
	

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->

{{--	<style type="text/css">
		body {
			padding-top: 20px;
			padding-bottom: 20px;
			margin-bottom: 20px;
		}

		.content {
			padding: 1px 30px 10px 30px;
		}

		.navbar {
		  margin-bottom: 20px;
		}

		.navbar-brand {
			font-weight: bold;
		}

		h4 {
			line-height: 1.8em;
			color: black;
			font-size: 20px;
			margin-bottom: 20px;
		}

		h4:after {
			content:' ';
			display:block;
			border:2px solid #d0d0d0;
			border-radius:4px;
			-webkit-border-radius:4px;
			-moz-border-radius:4px;
			box-shadow:inset 0 1px 1px rgba(0, 0, 0, .05);
			-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, .05);
			-moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, .05);
		}
	</style>
--}}

	<title>iKahwin - Admin</title>
	<style type="text/css">
	.admin-album {
		padding: 16px;
		margin-top: 10px;
		border-radius: 6px;
		border: 1px solid #DDD;
	}

	.admin-album .qq-upload-button{
		width: 160px;
	}

	.admin-album .list-unstyled{
		margin-top: 20px;
	}

	.admin-album .list-unstyled li .photo {
		margin-bottom: 20px;
		border: 1px solid #DDD;
	}

	.admin-album .list-unstyled li .photo span {
		padding: 4px 8px;
	}
	</style>
</head>


<body class="admin">
	<div id="admin">

		<section id="header" class="">
		
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-inverse navbar-admin" role="navigation">

				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{ URL::to('admin') }}"> iKahwin <span class="hidden-xs">- Malaysia's No 1 wedding portal</span></a>
					</div>

					<div class="navbar-collapse collapse">
						
						{{-- 
						<ul class="nav navbar-nav">
							@if( Auth::user()->allowManageUser() )
							<li><a href="{{{ URL::to('admin') }}}">Users</a></li>
							<li><a href="{{{ URL::to('admin/countries') }}}">Countries</a></li>
							@endif
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Festivals <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
								<li><a href="{{{ URL::to('admin/vendors') }}}">All Festivals</a></li>
								<li><a href="{{{ URL::to('admin/vendors/create') }}}">Add New</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Stories <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
								<li><a href="{{{ URL::to('admin/stories') }}}">All Stories</a></li>
								<li><a href="{{{ URL::to('admin/stories/create') }}}">Add New</a></li>
								<li><a href="{{{ URL::to('admin/stories/categories') }}}">Categories</a></li>
								</ul>
							</li>
						</ul>
						--}}

						<ul class="nav navbar-nav navbar-right">
							<li><a href="{{ url('/') }}"><i class="fa fa-external-link"></i> View site</a>
							<li><a href="{{ url('/admin/users/'. Auth::user()->id . '/edit') }}">My Profile</a></li>

							@if( Auth::user()->allowManageFestival() )
								
								<li {{ (Request::is('admin/vendors') || Request::is('admin/vendors/*/edit') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/vendors') }}}">My Listing</a></li>
									
							@endif

							<li class="active"><a href="{{{ URL::to('logout') }}}">Log out</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</div>
		</section>

		<!-- Content -->
		<section class="content">
			<div class="container-fluid no-padding">

				<div class="row no-margin">
					<div class="col-md-2 sidebar-menu hidden-xs">
						<div class="inner-col">
							<ul class="nav">

								@if( Auth::user()->allowManageFestival() )
								
								<li class="menu-title">Messages</li>
								<li {{ (Request::is('admin/messages/all') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/messages/all') }}}">All messages</a></li>
									
								@endif


								@if( Auth::user()->allowManageUser() )
								<li class="menu-title">Admin-only stuff</li>
								<li {{ (Request::is('admin/countries') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/countries') }}}">Countries</a></li>

								<li {{ (Request::is('admin/orders') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/orders') }}}">Orders</a></li>
								<li><a href="{{{ URL::to('admin/orders') }}}?status=2"> - New Order</a></li>
								<li><a href="{{{ URL::to('admin/orders') }}}?status=3"> - Pending</a></li>
								<li><a href="{{{ URL::to('admin/orders') }}}?status=5"> - Design Approved</a></li>
								
								<li class="menu-title">Users</li>
								<li {{ (Request::is('admin/users') || Request::is('admin/users/*/edit') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin') }}}">All Users</a></li>
								<li {{ (Request::is('admin/users/create') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/users/create') }}}">Add New</a></li>			
								@endif

								@if( Auth::user()->allowManageFestival() )
								
								<li class="menu-title">Vendors</li>
								<li {{ (Request::is('admin/vendors') || Request::is('admin/vendors/*/edit') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/vendors') }}}">All Vendors</a></li>
								<li {{ (Request::is('admin/vendors/create') || Request::is('admin/vendors/create') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/vendors/create') }}}">Add New</a></li>
									
								@endif

								@if( Auth::user()->allowManageStory() )
								
								<li class="menu-title">Stories</li>
								<li {{ (Request::is('admin/stories')  || Request::is('admin/stories/*/edit') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/stories') }}}">All Stories</a></li>
								<li {{ (Request::is('admin/stories/create') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/stories/create') }}}">Add New</a></li>
								<li {{ (Request::is('admin/stories/categories') ? 'class="active"' : '') }}><a href="{{{ URL::to('admin/stories/categories') }}}">Categories</a></li>
								
								@endif

							</ul>
						</div>
					</div>
					<div class="col-md-10 main-content">
						<div class="inner-col">

							@if(Session::has('message'))
							<div class="alert alert-success alert-growl">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								{{ Session::get('message') }}
							</div>
							@endif
							@yield('content')
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ./ content -->

	</div> <!-- /container -->

	<!-- modal -->
	<div class="modal fade" id="photoSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-remote="@yield('photo-modal')">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/library/jquery/jquery.min.js">\x3C/script>')</script>
	<script src="{{{ asset('assets/js/bootstrap.min.js') }}}"></script>
	<script src="{{{ asset('assets/js/modernizr-latest.js') }}}"></script>

	{{-- @TODO: Yield --}}
	<script src="{{{ asset('assets/js/tagsinput/bootstrap-tagsinput.js') }}}"></script>
	<script src="{{{ asset('assets/js/bootstrap3-typeahead.js') }}}"></script>
	<script src="{{{ asset('assets/uploader/fileuploader.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('packages/wysihtml5/lib/js/wysihtml5-0.3.0.min.js')  }}}"></script>
	<script src="{{{ asset('packages/wysihtml5/src/bootstrap3-wysihtml5.js')  }}}"></script>
	<script src="//cdn.jsdelivr.net/isotope/1.5.25/jquery.isotope.min.js"></script>
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
	
	<script src="{{{ asset('assets/js/datepicker/bootstrap-datepicker.js') }}}"></script>

	<link rel="stylesheet" href="{{ asset('redactor/redactor.css') }}" />
	<script src="{{ asset('redactor/redactor.js') }}"></script>
	<script src="{{ asset('redactor/video.js') }}"></script>
	<script src="{{ asset('redactor/fontcolor.js') }}"></script>
	<script src="{{ asset('redactor/table.js') }}"></script>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-35746485-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<script>
	function photoSelectOpen(e){
		
		$('#photoSelectModal').modal({
			remote: "@yield('photo-modal')/" + $(e).closest('.form-group').find('textarea').attr('id')
		});
	}
	$(document).ready(function() {
		// $('textarea:not(.nowysiwyg)').summernote({

		// 	minHeight: null,
			/*
			toolbar: [
			    //[groupname, [button list]]
			    ['style', ['style']], 
			    ['style', ['bold', 'italic', 'underline', 'clear']],
			    ['font', ['strikethrough']],
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['table', ['table']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    // ['height', ['height']],
			  ],
			  */

			// onImageUpload: function(files, editor, welEditable) {
   //              sendFile(files[0], editor, welEditable);
   //          }

			// customTemplates: {
			//     image: function(locale) {
			//       return "<li>" +
			//         "<a class='btn  btn-sm btn-default' onclick='photoSelectOpen(this)' title='" + locale.image.insert + "'><i class='glyphicon glyphicon-picture'></i></a>" +
			//         "</li>";
			//     }
			//   }
		// });

		// make sure remote content are fetched
		$('body').on('hidden.bs.modal', '.modal', function () {
		  $(this).removeData('bs.modal');
		});


		// Toggle button
		$('a[href="#toggle"]').on('click', function() {
			var ajaxPath = $(this).data('ajax'),
				isActive = $(this).hasClass('active');

			if($(this).data('sendemail') && ! isActive) {
				if(confirm('Do you want to send a confirmation e-mail to this user?')) {
					ajaxPath = ajaxPath + '/sendemail';
				}
			}

			$(this).toggleClass('active', ! isActive);

			$.ajax(ajaxPath)
				.done(function(data) {
					if( ! data.success) {
						alert('Error');
					}
				});
		});

		// User filter
		$('#user-filter input, #user-filter select').on('change', function() {
			$(this).parents('form').submit();
		});

		$('.monthpicker').datepicker({
			minViewMode: 1,
			calendarWeeks: true,
			autoclose: true,
			format: 'MM yyyy'
		});

		$('.datepicker').datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd'
		});

		// cover image uploader
		if($('#cover-fine-uploader').length > 0 ){
		var coverupload = new qq.FileUploader({
				element: $('#cover-fine-uploader')[0],
				// 	request: {
				// 	endpoint: 'admin/coverUpload'
				// },
				autoUpload: true,
				text: {
					uploadButton: '<i class="icon-plus icon-white"></i> Select Files'
				},
				debug: true,
				action: $('#cover-fine-uploader').data('action'),
				allowedExtensions: ['jpg', 'png'],
				 template: '<div id="profile-pic-progress" class="progress progress-striped active hide"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">Uploading...</div></div>'+
				 	'<div class="qq-uploader">' + 
	                '<div class="qq-upload-drop-area" style="height:1px;"><span>Drop files here</span></div>' +
	                '<div class="qq-upload-button btn btn-default btn-block">Upload new cover image</div>' +
	                '<ul class="qq-upload-list list-unstyled hide"></ul>' + 
	             '</div>',

				// template for one item in file list
	        	// template for one item in file list
	        	fileTemplate: '<li class="clearfix">' +
	                '<span class="qq-upload-file pull-left"></span>' +
	                '<span class="qq-upload-spinner  pull-right"></span>' +
	                '<span class="qq-upload-size  pull-right"></span>' +
	                '<a class="qq-upload-cancel  pull-right" href="#">Cancel</a>' +
	                '<span class="qq-upload-failed-text  pull-right">Failed</span>' +
	            '</li>', 
	            showMessage: function(message){
		            $('#profile-pic-error').html(message).show();
		        }, 
		        onProgress: function(id, fileName, loaded, total){
		        	$('#profile-pic-progress .progress-bar').css('width', Math.round(loaded / total * 100) + '%');
		        },
	            onSubmit: function(id, fileName){
	            	$('#profile-pic-progress').removeClass('hide');
	            	$('#profile-fine-uploader .qq-uploader').addClass('hide');
	            	$('#profile-pic-error').hide();

	            },
				onComplete: function(id, fileName, response){
					$('#profile-pic-progress').addClass('hide');
					$('#profile-fine-uploader .qq-uploader').removeClass('hide');

					if(response.success){
						$('#cover-thumb').attr('src', response.src);
						$('#main-form').attr('action', response.action);
					} else {
						console.log(response);
						if(response.message){
							$('#profile-pic-error').html(response.message).show();	
						} else {
							$('#profile-pic-error').html('Upload error. The file could be too big. Upload a smaller file').show();
						}
					}
				},
			});
		}

		// generic photo image uploader
		if($('#photo-fine-uploader').length > 0 ){
		var coverupload = new qq.FileUploader({
				element: $('#photo-fine-uploader')[0],
				// 	request: {
				// 	endpoint: 'admin/coverUpload'
				// },
				autoUpload: true,
				text: {
					uploadButton: '<i class="icon-plus icon-white"></i> Select Files'
				},
				debug: true,
				action: $('#photo-fine-uploader').data('action'),
				allowedExtensions: ['jpg', 'png'],
				 template: '<div id="photo-pic-progress" class="progress progress-striped active hide"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">Uploading...</div></div>'+
				 	'<div class="qq-uploader">' + 
	                '<div class="qq-upload-drop-area" style="height:1px;"><span>Drop files here</span></div>' +
	                '<div class="qq-upload-button btn btn-default btn-block">Upload new cover image</div>' +
	                '<ul class="qq-upload-list list-unstyled hide"></ul>' + 
	             '</div>',

				// template for one item in file list
	        	// template for one item in file list
	        	fileTemplate: '<li class="clearfix">' +
	                '<span class="qq-upload-file pull-left"></span>' +
	                '<span class="qq-upload-spinner  pull-right"></span>' +
	                '<span class="qq-upload-size  pull-right"></span>' +
	                '<a class="qq-upload-cancel  pull-right" href="#">Cancel</a>' +
	                '<span class="qq-upload-failed-text  pull-right">Failed</span>' +
	            '</li>', 
	            showMessage: function(message){
		            $('#photo-pic-error').html(message).show();
		        }, 
		        onProgress: function(id, fileName, loaded, total){
		        	$('#photo-pic-progress .progress-bar').css('width', Math.round(loaded / total * 100) + '%');
		        },
	            onSubmit: function(id, fileName){
	            	$('#photo-pic-progress').removeClass('hide');
	            	$('#photo-fine-uploader .qq-uploader').addClass('hide');
	            	$('#photo-pic-error').hide();

	            },
				onComplete: function(id, fileName, response){
					$('#photo-pic-progress').addClass('hide');
					$('#photo-fine-uploader .qq-uploader').removeClass('hide');

					if(response.success){
						$('#photos-list').append(response.html);
					} else {
						console.log(response);
						if(response.message){
							$('#photo-pic-error').html(response.message).show();	
						} else {
							$('#photo-pic-error').html('Upload error. The file could be too big. Upload a smaller file').show();
						}
					}
				},
			});
		}


		// generic photo album image uploader
		if($('.album-uploader').length > 0 ){

		$('.album-uploader').each( function( index, element ){
		    // console.log( $( this ).text() );
		
		var  testi = new qq.FileUploader({
				element: $('.album-uploader')[index],
				// 	request: {
				// 	endpoint: 'admin/coverUpload'
				// },
				autoUpload: true,
				text: {
					uploadButton: '<i class="icon-plus icon-white"></i> Select Files'
				},
				debug: true,
				action: $(element).data('action'),
				allowedExtensions: ['jpg', 'png'],
				 template: '<div id="photo-pic-progress" class="progress progress-striped active hide"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">Uploading...</div></div>'+
				 	'<div class="qq-uploader">' + 
	                '<div class="qq-upload-drop-area" style="height:1px;"><span>Drop files here</span></div>' +
	                '<div class="qq-upload-button btn btn-success btn-sm btn-block">Add photo</div>' +
	                '<ul class="qq-upload-list list-unstyled hide"></ul>' + 
	             '</div>',

				// template for one item in file list
	        	// template for one item in file list
	        	fileTemplate: '<li class="clearfix">' +
	                '<span class="qq-upload-file pull-left"></span>' +
	                '<span class="qq-upload-spinner  pull-right"></span>' +
	                '<span class="qq-upload-size  pull-right"></span>' +
	                '<a class="qq-upload-cancel  pull-right" href="#">Cancel</a>' +
	                '<span class="qq-upload-failed-text  pull-right">Failed</span>' +
	            '</li>', 
	            showMessage: function(message){
		            $('#photo-pic-error').html(message).show();
		        }, 
		        onProgress: function(id, fileName, loaded, total){
		        	$('#photo-pic-progress .progress-bar').css('width', Math.round(loaded / total * 100) + '%');
		        },
	            onSubmit: function(id, fileName){
	            	$('#photo-pic-progress').removeClass('hide');
	            	$('#photo-fine-uploader .qq-uploader').addClass('hide');
	            	$('#photo-pic-error').hide();

	            },
				onComplete: function(id, fileName, response){
					$('#photo-pic-progress').addClass('hide');
					$('#photo-fine-uploader .qq-uploader').removeClass('hide');

					if(response.success){
						$('#photos-list-' + response.albumid ).prepend(response.html);
					} else {
						console.log(response);
						if(response.message){
							$('#photo-pic-error').html(response.message).show();	
						} else {
							$('#photo-pic-error').html('Upload error. The file could be too big. Upload a smaller file').show();
						}
					}
				},
			});

		});
		}
		

		// user profile photo image uploader
		if($('#avatar-fine-uploader').length > 0 ){
		var coverupload = new qq.FileUploader({
				element: $('#avatar-fine-uploader')[0],
				// 	request: {
				// 	endpoint: 'admin/coverUpload'
				// },
				autoUpload: true,
				text: {
					uploadButton: '<i class="icon-plus icon-white"></i> Select Files'
				},
				debug: true,
				action: $('#avatar-fine-uploader').data('action'),
				allowedExtensions: ['jpg', 'png'],
				 template: '<div id="photo-pic-progress" class="progress progress-striped active hide"><div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">Uploading...</div></div>'+
				 	'<div class="qq-uploader">' + 
	                '<div class="qq-upload-drop-area" style="height:1px;"><span>Drop files here</span></div>' +
	                '<div class="qq-upload-button btn btn-default btn-block">Upload new profile photo</div>' +
	                '<ul class="qq-upload-list list-unstyled hide"></ul>' + 
	             '</div>',

				// template for one item in file list
	        	// template for one item in file list
	        	fileTemplate: '<li class="clearfix">' +
	                '<span class="qq-upload-file pull-left"></span>' +
	                '<span class="qq-upload-spinner  pull-right"></span>' +
	                '<span class="qq-upload-size  pull-right"></span>' +
	                '<a class="qq-upload-cancel  pull-right" href="#">Cancel</a>' +
	                '<span class="qq-upload-failed-text  pull-right">Failed</span>' +
	            '</li>', 
	            showMessage: function(message){
		            $('#avatar-pic-error').html(message).show();
		        }, 
		        onProgress: function(id, fileName, loaded, total){
		        	$('#avatar-pic-progress .progress-bar').css('width', Math.round(loaded / total * 100) + '%');
		        },
	            onSubmit: function(id, fileName){
	            	$('#avatar-pic-progress').removeClass('hide');
	            	$('#avatar-fine-uploader .qq-uploader').addClass('hide');
	            	$('#avatar-pic-error').hide();

	            },
				onComplete: function(id, fileName, response){
					$('#avatar-pic-progress').addClass('hide');
					$('#avatar-fine-uploader .qq-uploader').removeClass('hide');

					if(response.success){
						$('#profile-pic').attr('src', response.src);
					} else {
						console.log(response);
						if(response.message){
							$('#avatar-pic-error').html(response.message).show();	
						} else {
							$('#avatar-pic-error').html('Upload error. The file could be too big. Upload a smaller file').show();
						}
					}
				},
			});
		}

		// delete a photo
		$('#photos-list, #photo-fine-uploader, .photo-list').on('click', 'a', function() {
			var ajaxPath = $(this).data('ajax');
			$.ajax(ajaxPath, {type:'POST'})
				.done(function(data) {
					if( ! data.success) {
						alert('Error');
					}

					$('#photo-'+ data.id).remove();
				});
		});

		$('.confirmation').on('click', function () {
		     return confirm('Are you sure?');
		 });

		// Dynamically load bootstrap modal
		$('[data-toggle="ajaxModal"]').on('click',
				function(e) {
					$('#ajaxModal').remove();
					e.preventDefault();
					var $this = $(this)
					  , $remote = $this.data('remote') || $this.attr('href')
					  , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
					$('body').append($modal);
					$modal.modal({backdrop: 'static', keyboard: false});
					$modal.load($remote);
					}
				);

		// Active tab from URL
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');

		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});

	});

	</script>

	@yield('scripts')

</body>
</html>