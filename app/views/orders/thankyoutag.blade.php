@extends('layouts.default')


@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="well well-sm">
			<img src="{{ asset('assets/img/thankyoutag.png')  }}" style="width:100%">
				</div>

			<div class="listing-header center-block" style="margin-bottom:30px">
				<h1 class="listing-title" style="text-align: center;">Borang Tempahan  Thank You Tag</h1>
				<small></small>
			</div>

			<p>Isikan maklumat tempahan anda disini dan kemudian semak dahulu sebelum membuat tempahan agar maklumat yang diberikan betul. Sebarang masalah, kemusykilan, ataupun soalan boleh hubungi kami di sales@ikahwin.my ataupun 013 - 972 1080</p>
		<hr/>
		</div>

		<div class="col-md-8 col-md-offset-1">
			{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal clearfix', 'name' => 'orderForm')) }}
				
				<!-- Required details -->
				<h4 class="form-title">Butiran Pembeli</h4>
				<div class="fieldset">
					{{ Form::bootstrapInput('name', 'Name Penuh', 'Perkataan apakah yang ingin ditulis di rubber stamp?') }}
					{{ Form::bootstrapInput('email', 'Email') }}
					{{ Form::bootstrapInput('phone', 'Nombor Telefon', 'Pastikan Kami boleh Whatsapp/SMS melalui nombor ini' ) }}
				</div>

				<h4 class="form-title">Order details</h4>
				<div class="fieldset">
					{{ Form::bootstrapDropdown('Quantity', 'Kuantiti', [
						'3x5cm Tag Kosong 500 Pieces - MYR 65.00',
						'3x5cm Tag Kosong 1000 Pieces - MYR 130.00',
						'  ',
						'4x7cm Tag Kosong 500 Pieces - MYR 90.00',
						'4x7cm Tag Kosong 1000 Pieces - MYR 160.00',
						'  ', 
						'3x5cm Printed 300 Pieces - MYR 55.00',
						'3x5cm Printed 500 Pieces - MYR 90.00',
						'3x5cm Printed 700 Pieces - MYR 125.00',
						'3x5cm Printed 1000 Pieces - MYR 160.00',
						'3x5cm Printed 1500 Pieces - MYR 230.00',
						'3x5cm Printed 2000 Pieces - MYR 270.00',
						'  ',
						'3x5cm Exclusive 300 Pieces - MYR 70.00',
						'3x5cm Exclusive 500 Pieces - MYR 115.00',
						'3x5cm Exclusive 700 Pieces - MYR 160.00',
						'3x5cm Exclusive 1000 Pieces - MYR 230.00',
						'3x5cm Exclusive 1500 Pieces - MYR 300.00',
						'3x5cm Exclusive 2000 Pieces - MYR 400.00',
						'  ',
						'3x5cm Premium 50 Pieces - MYR 50.00',
						'3x5cm Premium 100 Pieces - MYR 100.00',
						'3x5cm Premium 300 Pieces - MYR 270.00',
						'3x5cm Premium 500 Pieces - MYR 350.00',
						'3x5cm Premium 1000 Pieces - MYR 500.00'

						] ) }}

					{{ Form::bootstrapInput('Kod Design', 'Kod Design', '') }}

					{{ Form::bootstrapTextarea('Perkataan Untuk Ditulis', 'Perkataan Untuk Ditulis*', 'Perkataan apakah yang ingin ditulis di rubber stamp?') }}

					
					{{ Form::bootstrapRadio('source', 'Dari mana anda tahu mengenai kami?', [
						'Rakan-Rakan', 
						'Facebook Group',
						'iKahwin FB Page',
						'Instagram',
						'Blog iKahwin'] ) }}

				</div>

				<!-- Required details -->
				<h4 class="form-title">Delivery Details</h4>
				<div class="fieldset">

					{{ Form::bootstrapTextarea('address', 'Alamat Penuh', 'Pastikan alamat ditulis dengan betul. Jika terdapat kesalahan, anda mungkin terpaksa membayar kos penghantaran semula') }}

					{{ Form::bootstrapRadio('penghantaran', 'Penghantaran', [
						'Semenanjung (FREE) MYR 0.00', 
						'Sabah / Sarawak - MYR 5.00',
						'Brunei - MYR 45.00',
						'Singapore - MYR 50.00' ] ) }}

					{{ Form::bootstrapDropdown('payment_method', 'Kaedah Pembayaran', [
						Order::PAYMETHOD_ONLINETRANSFER, 
						Order::PAYMETHOD_CREDITCARD,
						Order::PAYMETHOD_OFFLINE ] ) }}
				</div>

				{{ Form::totalAmount( '0.00', ['names']) }}
				<div class="col-md-8 col-md-offset-4">
				{{ Form::submit( 'Order Now', array('class' => 'btn btn-primary btn-lg')) }}
				</div>

			{{ Form::close() }}
		</div>
</div>

@stop