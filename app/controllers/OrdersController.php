<?php

class OrdersController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showView($product)
	{
		return View::make('orders.'.$product );
	}


	public function postOrder($product)
	{
		// $rules = [
		// 	'name' => 'required',	
		// 	'email' => 'required',
		// 	'phone' => 'required',
		// 	'address' => 'required',
		// 	'penghantaran' => 'required'
		// ];

		$input = Input::all();
		// $validation = Validator::make($input, $rules);

		if (true /* $validation->passes() */ ) {
			$input['product'] = $product;
			$order = Order::create($input);

			// Build [data] section
			$data = [];
			$input = Input::all();
			unset($input['_token']);

			$fillable = $order->getFillable();
			foreach ($input as $key => $value) {
				if(!in_array($key, $fillable)) {
					if(is_array($value)) {
						$data[] = "[$key]\n" . json_encode($value);
					} else {
						$data[] = "[$key]\n" . $value;
					}
				} 
			}

			$dataStr = implode("\n\n", $data);
			$order->data = $dataStr;
			$order->slug = substr(str_shuffle(MD5(microtime())), 0, 12);
			$order->save();

			// all good, go to payment page
			switch ($order->payment_method) {
				case Order::PAYMETHOD_ONLINETRANSFER:
					return  Redirect::to('order/' . $product . '/pay/ipay88/'. $order->slug)->withInput();
					break;

				case Order::PAYMETHOD_CREDITCARD:
					return  Redirect::to('order/' . $product . '/pay/paypal/'. $order->slug)->withInput();
					break;

				case Order::PAYMETHOD_OFFLINE:
					return View::make('orders.pay-offline');
					break;
			}
			
		} else {
			return Redirect::back()
				->withInput();
				// ->withErrors($validation);
		}
		
	}

	public function showPaymentRedirect($product, $service, $slug)
	{
		$order = Order::findSlug($slug)->first();
		$productName = $product;
		$productId = 1;

		switch ($service) {
			case 'ipay88':
				$ipay88 = new IPay88(Config::get('app.ipay88_merchantcode'), Config::get('app.ipay88_merchantkey'));

				// $ipay88->setField('PaymentId', $paymentId);
				$ipay88->setField('RefNo', $order->id);
				$ipay88->setField('Amount', $order->total);
				$ipay88->setField('Currency', 'MYR');
				$ipay88->setField('ProdDesc', 'Order for: ' . $productName);
				$ipay88->setField('UserName', $order->name);
				$ipay88->setField('UserEmail', $order->email);
				$ipay88->setField('UserContact', $order->phone);
				$ipay88->setField('Remark', '');
				$ipay88->setField('Lang', 'utf-8');
				$ipay88->setField('ResponseURL', url('order/response/ipay88'));
				$ipay88->generateSignature();

				// redirect to ipay88
				return View::make('orders.pay-online', ['order' => $order, 'productName' => $productName, 'ipay88' => $ipay88]);
			case 'paypal':
				$checkout  = "?cmd=_cart";
				$checkout .= "&upload=1";
				$checkout .= "&business=" . urlencode(Config::get('app.paypal_email'));
				$checkout .= "&brandname=" . urlencode('iKahwin.my');
				// $checkout .= "&image_url=" . urlencode('');
				$checkout .= "&custom=" . urlencode($order->id);
				$checkout .= "&return=" . urlencode(url('order/success'));
				$checkout .= "&cancel_return=" . urlencode(url('/'));
				$checkout .= "&notify_url=" . urlencode(url('order/response/paypal'));
				$checkout .= "&currency_code=" . urlencode('MYR');
				$checkout .= "&item_number_" . 1 . "=" . urlencode($productId);
				$checkout .= "&item_name_" . 1 . "=" . urlencode($productName);
				$checkout .= "&amount_" . 1 . "=" . urlencode(number_format($order->total, 2, '.', ','));

				// error_log(date('Y/m/d H:i:s') . '#' . $checkout . '#' . PHP_EOL, 3, '/tmp/my_errors.log');

				return Response::make('', 302)->header('Location', 'https://www.paypal.com/cgi-bin/webscr' . $checkout);
		}
	}

	public function postPaymentResponse($service)
	{
		$response = null;

		switch ($service) {
			case 'ipay88':
				$ipay88 = new IPay88(Config::get('app.ipay88_merchantcode'), Config::get('app.ipay88_merchantkey'));
				$response = $ipay88->getResponse();

				if ($response['status']) {
					$order = Order::find((int)$response['data']['RefNo']);
					$order->status = Order::STATUS_SUCCESS;
					$order->save();
					return View::make('orders.payment-success');
				} else {
					return View::make('orders.payment-error');
				}
			case 'paypal':
				$paypal = new IpnListener();
				$paypal->use_sandbox = false;
				$paypal->force_ssl_v3 = false;

				try {
					$paypal->requirePostMethod();
					$response = $paypal->processIpn();
				} catch (Exception $e) {
					// error_log(date('Y/m/d H:i:s') . '# Exception: ' . $e->getMessage() . '#' . PHP_EOL, 3, '/tmp/my_errors.log');
				}

				if ($response) {
					$order = Order::find((int)Input::get('custom'));
					
					// @TODO: or just store the status
					switch (Input::get('payment_status')) {
						case 'Completed':
							$order->status = Order::STATUS_SUCCESS;
							break;
						case 'In-Progress':
						case 'Pending':
							$order->status = Order::STATUS_PENDING;
							break;
					}

					$order->save();
				}
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Debug
	|--------------------------------------------------------------------------
	*/

	public function showTestRequest()
	{
		$ipay88 = new IPay88('M06482', 'pFTJjguXOt');

		$refNo = time() . mt_rand();
		$ipay88->setField('RefNo', $refNo);

		// For standard online payment.
		// $ipay88->setField('PaymentId', '');
		$ipay88->setField('Amount', '1.00');
		$ipay88->setField('Currency', 'MYR');
		$ipay88->setField('ProdDesc', 'Thank You Tag');
		$ipay88->setField('UserName', 'UserName');
		$ipay88->setField('UserEmail', 'taufik@azrul.com');
		$ipay88->setField('UserContact', '81238238232');
		$ipay88->setField('Remark', '');
		$ipay88->setField('Lang', 'utf-8');
		$ipay88->setField('ResponseURL', 'http://new.ikahwin.my/order/response/ipay88');

		$ipay88->generateSignature();

		$ipay88Fields = $ipay88->getFields();

		if (!empty($ipay88Fields)) {
			$thankYouMessage = '<div style="text-align: center"><h2>Redirecting to ipay88...</h2></div>';
			$thankYouMessage .= '<form action="' . $ipay88->getTransactionUrl() . '" method="post" id="ipform">
				<table style="display: none;">';
					foreach ($ipay88Fields as $key => $val) {
						$thankYouMessage .= '<tr>
							<td><label>' . $key . '</label></td>
							<td><input type="text" name="' . $key . '" value="' . $val . '" /></td>
						</tr>';
					}
					$thankYouMessage .= '<tr>
					<td colspan="2"><input type="submit" value="Submit" name="Pay with IPay88" /></td>
					</tr>
				</table>
			</form>
			<script type="text/javascript">
				document.getElementById(\'ipform\').submit();
			</script>
			';
		}

		echo $thankYouMessage;
	}
}
