<?php

class BaseModel extends Eloquent {
 
	public function formatDate($column, $format = null)
	{
		if(empty($this->$column)) {
			return '';
		} else {
			return \Carbon\Carbon::createFromTimeStamp(strtotime($this->$column))->toFormattedDateString();
		}
	} 
}