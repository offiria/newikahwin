<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTraveltipsFieldToFestivalsCountries extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendors', function($table)
		{
			$table->text('traveltips')->after('excerpt')->nullable();
		});

		Schema::table('countries', function($table)
		{
			$table->text('traveltips')->after('description')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendors', function($table)
		{
			$table->dropColumn('traveltips');
		});

		Schema::table('countries', function($table)
		{
			$table->dropColumn('traveltips');
		});
	}

}
