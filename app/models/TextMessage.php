<?php

class TextMessage extends BaseModel {
	protected $fillable = ['vendor_id', 'name', 'mobile', 'message'];


	public function vendor()
	{
		return $this->hasOne('Vendor', 'id', 'vendor_id');    
	}
}