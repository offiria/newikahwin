<?php

return array(
	'company-name' => 'Name syarikat',
	'excerpt' => 'Short description',
	'phone-primary' => 'No telefon utama (Mobile)',
	'email' => 'Email',
	'coverage' => 'Liputan',
	'work-hours' => 'Waktu bekerja',
	'contact-info' => 'Nombor lain untuk dihubungi',

	'street' => 'No bangunan & Jalan',
	'city' => 'Daerah/Bandar',

	'album-gallery' => 'Album/Gallery',
	'packages' => 'Pakej Kami',
	'deals' => 'Promosi Terkini'
	);