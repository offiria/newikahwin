<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
		    $table->dropColumn('username');
		});

		Schema::table('users', function(Blueprint $table)
		{
			$table->string('username', 30)->nullable()->index();
			$table->string('firstname', 40)->nullable();
			$table->string('lastname', 40)->nullable();
			$table->string('verification_token', 64)->nullable();
			$table->text('description', 255)->nullable();
			$table->string('image', 255)->nullable();
			$table->string('status', 64)->nullable();
			$table->string('slug')->nullable();
			$table->text('params')->nullable();
		});

		Schema::create('password_reminders', function(Blueprint $table)
		{
			$table->string('email')->nullable()->index();
			$table->string('token')->nullable()->index();
			$table->timestamp('created_at');
		});

		Schema::create('stories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('old_id')->nullable();
			$table->integer('artist_id')->nullable();
			$table->integer('festival_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('hits')->nullable();
			$table->string('title')->nullable();
			$table->text('story')->nullable();
			$table->text('excerpt')->nullable();

			$table->string('cover_img')->default('assets/cover/default_story.jpg');
			$table->string('cover_thumb')->default('assets/cover/default_story_thumb.jpg');

			$table->smallInteger('featured')->default(0);
			$table->string('slug')->nullable();
			$table->timestamps();	
		});

		Schema::create('story_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('slug')->nullable();
			$table->timestamps();	
		});

		Schema::create('artists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('website')->nullable();
			$table->string('country')->nullable();
			$table->string('label')->nullable();
			$table->timestamps();	
		});

		Schema::create('albums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->integer('vendor_id')->nullable();
			$table->timestamps();	
		});

		Schema::create('artist_festival', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('artist_id')->nullable();
			$table->integer('festival_id')->nullable();
		});

		Schema::create('artist_space', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('artist_id')->nullable();
			$table->integer('space_id')->nullable();
		});

		Schema::create('pricings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->timestamps();
		});


		Schema::create('vendor_pricing', function(Blueprint $table)
		{
			$table->integer('vendor_id');
			$table->integer('pricing_id');
		});

		

		Schema::create('vendors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->text('description');
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->integer('user_id');
			$table->integer('old_id')->nullable();
			$table->integer('views')->default(0);
			$table->integer('hits')->default(0);
			
			$table->string('address')->nullable();
			$table->string('street')->nullable();
			$table->string('postcode')->nullable();
			$table->string('location')->nullable();
			$table->string('district')->nullable();
			$table->string('state')->nullable();
			$table->integer('country_id')->nullable();
			$table->string('cover_img')->default('assets/cover/default_vendor.jpg');
			$table->string('cover_thumb')->default('assets/cover/default_vendor_thumb.jpg');
			
			$table->integer('subscription')->default(0); // 0=none  10 = standard 20 = premium
			$table->date('subscription_expiry')->nullable();
			$table->string('status')->nullable();
			$table->string('activities')->nullable();
			$table->string('coverage')->nullable();
			$table->string('work_hours')->nullable();

			

			$table->date('announcement_date')->nullable();
			
			$table->string('website')->nullable();
			$table->string('facebook')->nullable();
			$table->string('instagram')->nullable();
			$table->string('twitter')->nullable();
			$table->string('googleplus')->nullable();
			$table->text('excerpt')->nullable();
			$table->text('additional_charges')->nullable();
			$table->text('contact_info')->nullable();

			
			$table->smallInteger('active')->default(1);
			$table->smallInteger('featured')->default(0);
			$table->string('slug')->nullable();
			$table->timestamps();
		});

		Schema::create('vendor_genre', function(Blueprint $table)
		{
			$table->integer('genre_id')->nullable();
			$table->integer('vendor_id')->nullable();
		});

		Schema::create('genres', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('cover_img')->nullable();
			$table->string('slug')->nullable();
			$table->timestamps();	
		});

		Schema::create('headliner_space', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('headliner_id')->nullable();
			$table->integer('space_id')->nullable();
		});

		Schema::create('hotels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('street')->nullable();
			$table->string('district')->nullable();
			$table->string('state')->nullable();
			$table->string('country')->nullable();
			$table->string('website')->nullable();
			$table->decimal('latitude')->nullable();
			$table->decimal('longitude')->nullable();
			$table->timestamps();	
		});

		Schema::create('maillists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->string('number')->nullable();
			$table->string('city')->nullable();
			$table->timestamps();	
		});

		Schema::create('spaces', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('concept')->nullable();
			$table->string('spacetype')->nullable();
			$table->integer('festival_id')->nullable();
			$table->timestamps();	
		});

		Schema::create('vouchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('vouchernumber')->nullable();
			$table->datetime('expired_at');
			$table->integer('user_id')->nullable();
			$table->timestamps();	
		});


		// create folder
		if(!file_exists(public_path() . '/photos'))
			mkdir( public_path() . '/photos');

		if(!file_exists(public_path() . '/covers'))
			mkdir( public_path() . '/covers');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('password_reminders');
		Schema::drop('stories');
		Schema::drop('albums');
		Schema::drop('story_categories');
		Schema::drop('artists');
		Schema::drop('artist_festival');
		Schema::drop('artist_space');
		Schema::drop('pricings');
		Schema::drop('vendor_pricing');
		Schema::drop('vendors');
		Schema::drop('vendor_genre');
		Schema::drop('genres');
		Schema::drop('headliner_space');
		Schema::drop('hotels');
		Schema::drop('maillists');
		Schema::drop('spaces');
		Schema::drop('vouchers');

		if(file_exists(public_path() . '/photos'))
			$this->delete( public_path() . '/photos' );

		if(file_exists(public_path() . '/covers'))
			$this->delete( public_path() . '/covers' );
	}


	// delete photo storage
	function delete($path)
	{
	    if (is_dir($path) === true)
	    {
	        $files = array_diff(scandir($path), array('.', '..'));

	        foreach ($files as $file)
	        {
	            $this->delete(realpath($path) . '/' . $file);
	        }

	        return rmdir($path);
	    }

	    else if (is_file($path) === true)
	    {
	        return unlink($path);
	    }

	    return false;
	}

}
