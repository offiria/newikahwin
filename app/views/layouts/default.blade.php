<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="@yield( 'meta-description', 'iKahwin.my is a trusted online wedding planning website which allows user to discover local wedding service provider online')">
	<meta name="keywords" content="@yield( 'meta-keywords', 'kahwin, plan kahwin , wedding directory malaysia , wedding directory , wedding checklist , checklist kahwin melayu')">

	<meta name="author" content="iKahwin Team">
	
	<link rel="shortcut icon" type="image/ico" href="img/favicon.png">
	<link rel="apple-touch-icon" href="http://ikahwin.my/">
	<link rel="apple-touch-icon" sizes="144x144" href="#">
	<link rel="apple-touch-icon" sizes="114x114" href="#">

	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/bootstrap.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('flat-ui/css/flat-ui.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/js/datepicker/css/datepicker3.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/font-awesome/css/font-awesome.min.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/weather-icons/css/weather-icons.min.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/gotham-font/gt.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/style.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('magnific-popup/magnific-popup.css') }}} "> 
	<link rel="stylesheet" href="{{{ asset('assets/js/bootstrapvalidator/css/bootstrapValidator.min.css') }}}"/>

	<!-- Utilities -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  	<script>window.jQuery || document.write('<script src="js/library/jquery/jquery.min.js">\x3C/script>')</script>
	<script src="{{{ asset('assets/js/bootstrap.min.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('assets/js/modernizr-latest.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('assets/js/datepicker/bootstrap-datepicker.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('magnific-popup/jquery.magnific-popup.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('nailthumb/jquery.nailthumb.1.1.min.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('assets/js/script.js') }}}" type="text/javascript"></script>
	<script type="text/javascript" src="{{{ asset('assets/js/bootstrapvalidator/js/bootstrapValidator.min.js') }}}"></script>

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js" type="text/javascript"></script>
	<![endif]-->
	<title>@yield('title', "Malaysia's Wedding Directory") - iKahwin</title>
	<link rel="image_src" href="@yield('title', asset('assets/logo.png') ) - iKahwin" />
	@yield('meta', "")
	
	<script type="text/javascript">
		function tagline_fade() {
		      var curr = $("#rotate-title a.active");
		      curr.removeClass("active");
		      var nextTag = curr.next('a');
		      if (!nextTag.length) {
		        nextTag = $("#rotate-title a").first();
		      } 
		      nextTag.addClass("active");
		    }

		$(document).ready(function() {
		    $('#main-carousel').carousel({interval: 7000});
		 });

		$(document).ready(function() {
		    $('.carousel').carousel({interval: 7000});
		 });


		$(document).ready(function() {
		    setInterval('tagline_fade()', 2500);
		});
	</script>
	<style type="text/css">
		#rotate-title a {
			display: none;

		}

		#rotate-title a.active{
			display: block;

		}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1547213035507514&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<section id="header" class="">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-inverse navbar-default" role="navigation">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{{ asset('assets/img/ikahwin-logo.png') }}}" alt="iKahwin - Weddings Simplified" /></a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="collapse">
							
							<ul class="nav navbar-nav navbar-right">
					
								<li class="link-vendor {{ (Request::is('top') ? ' active ' : '') }}"><a href="{{ URL::to('top') }}">Top Vendors</a></li>

								<li class="link-vendor {{ (Request::is('vendors') || Request::is('vendors/*') ? ' active ' : '') }}" id="rotate-title">
									<a href="{{ URL::to('vendors') }}" class="active">Cari Katering</a>
									<a href="{{ URL::to('vendors') }}">Cari Juruvideo</a>
									<a href="{{ URL::to('vendors') }}">Cari Mak Andam</a>
									<a href="{{ URL::to('vendors') }}">Cari Pelamin</a>
									<a href="{{ URL::to('vendors') }}">Cari Kek Kahwin</a>
									<a href="{{ URL::to('vendors') }}">Cari Perancang Perkahwinan</a>
									<a href="{{ URL::to('vendors') }}">Cari Kad Kahwin</a>
								</li>
								<li class="link-stories {{ (Request::is('blog') || Request::is('blog/*') ? ' active ' : '') }}"><a href="{{ URL::to('blog') }}">Blog</a></li>
								{{-- <li class="link-genres"><a href="{{ URL::to('what-to-do/genres') }}">Genres</a></li> --}}

								@if(Auth::check())
								@if( User::find( Auth::user()->id)->is('vendor') )
								<li><a href="{{ URL::to('admin/vendors') }}">My Listing</a></li>
								@endif

								@if( User::find( Auth::user()->id)->is('contributor') )
								<li><a href="{{ URL::to('admin/stories') }}">My Blog</a></li>
								@endif
								@endif

								{{--
								<li class="link-submit">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Submit <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu dropdown-menu-left" role="menu">
										<li class="link-submit link-story"><a href="{{ URL::to('submit-story') }}">Submit a Story</a></li>
										<li class="link-submit link-story"><a href="{{ URL::to('admin/vendors/create') }}">Submit a Festival</a></li>
									</ul>
								</li>
								--}}

								@if(Auth::check())
								<li class="link-register"><a href="{{ url('likes') }}"><i class="fa fa-heart"></i></a></li>
								<li class="link-login"><a href="{{ URL::to('logout') }}">Logout</a></li>
								@else
								<li class="link-login"><a href="{{ URL::to('login') }}">Login</a></li>
								<li class="link-register"><a href="{{ URL::to('register') }}" >Sign Up</a></li>
								@endif
								
							</ul>

							<ul class="nav navbar-nav navbar-right social-media hide">
								<li><a href="https://www.facebook.com/ikahwin.my" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/ikahwin.my" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="http://www.pinterest.com/ikahwin.my/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
								<li><a href="http://instagram.com/ikahwin.my" target="_blank"><i class="fa fa-instagram"></i></a></li>
							</ul>

						</div><!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>
		</div>
	</section>

	@if( !isset($isHomepage)) 
	<section id="site-search">
		<div class="container">
			<div class="inner">
				<div class="row">
						<div class="col-md-4 col-first">
							{{ Form::open( array('url' => 'vendors', 'method' => 'get')) }}	
							<label for="">Search Wedding Vendor</label>
							<div class="input-group">
								<input type="text" class="form-control" name="name" value="{{{ Input::get('name') }}}" placeholder="Vendor name or specific item">
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div><!-- /input-group -->
							{{Form::close()}}
						</div>
						<div class="col-md-8 col-sm-12 hidden-xs">
							<div class="row">
								<div class="col-md-12">
									<label for="">Or Browse</label>
								</div>
							</div>

							<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
								{{ Form::open( array('url' => 'vendors', 'method' => 'get')) }}	
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<?php $genre = Input::get('category'); ?>
									
									<div class="input-group" data-toggle="dropdown">
										<input type="text" class="form-control" id="" placeholder="Vendor" name="category" value="{{{ isset( $genre ) ? Genre::currentName() : '' }}}">
										<span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
									</div>
									<ul class="dropdown-menu" role="menu" aria-labelledby="genreDropdown">
										<?php
										$genres = Genre::all();
										$genres->sort( function($a, $b){ 
											return strcasecmp( trans('vendor.category-'.$a->slug), trans('vendor.category-'.$b->slug)); 
										}); 
										?>
										@foreach( $genres as $g)
										<?php
										$httpQuery = ['category' => $g->slug];
										if(Input::get('state')){
											$httpQuery['state'] = Input::get('state');
										}
										if(Input::get('name')){
											$httpQuery['name'] = Input::get('name');
										}
										?>
									    <li><a role="menuitem" tabindex="-1" href="{{ url('vendors?') . http_build_query( $httpQuery ) }}">{{ trans('vendor.category-'.$g->slug) }}</a></li>
									    @endforeach
									  </ul>
								</div>
								{{Form::close()}}
								</div>
								<div class="col-md-2 col-sm-2 text-separator">
									<span>{{ trans('vendor.in-around') }}</span>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-12">
								{{ Form::open( array('url' => 'vendors', 'method' => 'get')) }}	
								<div class="form-group">
									<div class="input-group" data-toggle="dropdown">
										<input type="text"    class="form-control" id="" placeholder="State" name="state" value="{{{ Input::get('state') }}}">
										<span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
									</div>
									<ul class="dropdown-menu" role="menu" aria-labelledby="contryDropdown">
										<?php
										$states = ['Johor', 'Kedah', 'Kelantan', 'Kuala Lumpur', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Perak',  'Perlis', 'Pulau Pinang', 'Putrajaya', 'Sabah', 'Sarawak', 'Selangor', 'Terengganu', 'Wilayah Persekutuan - Labuan'];
										?>
										@foreach( $states as $c)
										<?php
										$httpQuery = [];
										if(Input::get('category')){
											$httpQuery['category'] = Input::get('category');
										}
										$httpQuery['state'] = $c;

										if(Input::get('name')){
											$httpQuery['name'] = Input::get('name');
										}
										
										?>
									    <li><a role="menuitem" tabindex="-1" href="{{ url('vendors?') . http_build_query( $httpQuery ) }}">{{ $c }}</a></li>
									    @endforeach
									  </ul>
								</div>
								{{Form::close()}}
								</div>

								<!-- <div class="col-md-4 col-sm-4 col-xs-12">
								{{ Form::open( array('url' => 'vendors', 'method' => 'get',  'name' =>"dateSearch")) }}	
								<div class="form-group input-group date">
									<label for="" class="sr-only"></label>
									<input type="text" readonly class="form-control  uneditable-input" style="cursor:pointer; background-color: #fff;" id="" placeholder="Dates" name="date" value="{{{ Input::get('date') }}}"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
								{{Form::close()}}
								</div> -->

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	@endif

	@if(Session::has('message'))
	<div class="alert alert-success alert-growl">
		<div class="container">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{{ Session::get('message') }}
		</div>
	</div>
	@endif
	
	<!-- Content -->
	<section id="content" class="main-content">
		@yield('content')
	</section>
	<!-- ./ content -->
	
	<section id="footer">
		{{-- 
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="footer-title text-center">Suss the scene out before you go.</h2>
				</div>
			</div>
		</div>
		--}}
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        @include('layouts.footer.col1')
      </div>

      <div class="col-md-4">
        @include('layouts.footer.col2')
      </div>


      <div class="col-md-4 hidden-xs">
        @include('layouts.footer.col4')
      </div>
    </div>
  </div>
</div>

		
	</section>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-35746485-1', 'auto');
	  ga('send', 'pageview');

	</script>

<!-- Signup Modal -->
	<div class="modal fade" id="signupModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel">Sign Up For Fundplaces</h4>
				</div>
				
				{{ Form::open(array('id' => 'regForm', 'url' => 'register', 'class' => '', 'method' => 'post')) }}

				<div class="modal-body">
					<div class="form-group">
						{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'placeholder' => 'First Name')) }}
					</div>

					<div class="form-group {{ Form::errorClass('lastname') }}">
						{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control', 'placeholder' => 'Last Name')) }}
					</div>

					<div class="form-group {{ Form::errorClass('email') }}">
						{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>"Email")) }}
					</div>

					<div class="form-group">
						{{ Form::password('password', array('class' => 'form-control', 'placeholder'=> 'Password')) }}
					</div>

					<div class="form-group">
						{{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder'=> 'Confirm Password')) }}
					</div>

					<div class="form-group row">
						<label for="inputEmail3" class="col-sm-4 control-label">I am signing up to</label>
						<div class="col-sm-8">
							{{ Form::select('usertype', array('investor' => 'invest', 'owner' => 'raise fund'), null, ['class' => 'form-control']) }}
						</div>
					</div>

					<div class="checkbox clearfix">
						<label>
							<input type="checkbox"> I agree with the Terms & Conditions and Privacy Policy
						</label>
					</div>
				</div>
				
				<div class="modal-footer">
					<input type="submit" class="btn btn-orange" value="Submit" />
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>


</body>
</html>