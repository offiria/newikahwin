@extends('admin.layouts.default')

@section('content')

@if(Auth::user()->is('administrator'))
<div class="admin-search">
	<form role="form" id="user-filter" action="{{ url('admin/vendors') }}">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>
	</form>
</div>
@endif


<h2 class="admin-title">{{ isset($vendor) ? 'Butiran Perniagaan' : 'Butiran Perniagaan' }} @if(isset($vendor)) <span class="small"> <a href="{{ url('vendors/' . $vendor->genres->first()->slug . '/' . $vendor->slug) }}" target="_blank"><i class="fa fa-external-link"></i> </a></span> @endif</h2>


@if(isset($vendor))
		{{ Form::model($vendor, array('url' => 'admin/vendors/' . $vendor->id . '/edit', 'method' => 'post', 'class' => 'form-lm clearfix', 'id' => 'main-form')) }}
		@else
		{{ Form::open(array('url' => 'admin/vendors/create', 'method' => 'post', 'class' => 'form-lm clearfix', 'name' => 'entryForm', 'id' => 'main-form')) }}
@endif

<div class="row">
	<div class="col-md-9">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#tab-info" role="tab" data-toggle="tab">Basic info</a></li>
			<li><a href="#tab-desc" role="tab" data-toggle="tab">Detailed description </a></li>
			<li><a href="#tab-albums" role="tab" data-toggle="tab">Gallery/Albums</a></li>
			{{--<li><a href="#tab-traveltips" role="tab" data-toggle="tab">Travel Tips</a></li>--}}
			@if(isset($vendor))
			@if(Auth::user()->is('administrator'))
			<li><a href="#tab-revisions" role="tab" data-toggle="tab">Revisions</a></li>
			@endif
			@endif
		</ul>

		

		<div class="tab-content">
			<div class="tab-pane active" id="tab-info">
		  
				<h4 class="form-title">Butiran Asas</h4>

				<div class="fieldset">

					<div class="form-group {{ Form::errorClass('name') }}">
					{{ Form::label('name', trans('admin.company-name')) }}
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control input-lg')) }}
					{{ Form::errorMsg('name') }}
					</div>

					@if(Auth::user()->is('administrator'))
					<div class="form-group {{ Form::errorClass('excerpt') }}">
					{{ Form::label('excerpt', trans('admin.excerpt')) }}
					{{ Form::textarea('excerpt', Input::old('excerpt'), array('class' => 'form-control nowysiwyg', 'maxlength' =>'158', 'rows' => '3')) }}
					{{ Form::errorMsg('excerpt') }}
					</div>
					@endif

					<div class="row">
						<?php
						$email = Input::old('email');
						if(!isset($vendor) &&  empty($email)){
							$email = Auth::user()->email;
						}
						?>
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('email') }}">
							{{ Form::label('email', trans('admin.email')) }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								{{ Form::text('email', $email, array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('email') }}
							</div>
						</div>


						<?php
						$phone = Input::old('phone');
						if(!isset($vendor) &&  empty($phone)){
							$phone = Auth::user()->phone;
						}
						?>
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('phone') }}">
							{{ Form::label('phone', trans('admin.phone-primary')) }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-phone"></i></span>
								{{ Form::text('phone', $phone, array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('phone') }}
							</div>
						</div>
					</div>


					@if(Auth::user()->is('administrator'))
					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('user_id') }}">						
						{{ Form::label('user_id', 'Owner', ['class' => "text-danger"]) }}
						{{ Form::select('user_id', $owners , Input::old('user_id'), array('class' => 'form-control')) }}
						{{ Form::errorMsg('user_id') }}
							</div>
						</div>

						<div class="col-md-6">
						<div class="form-group {{ Form::errorClass('status') }}">
							{{ Form::label('status', 'status', ['class' => "text-danger"]) }}
							{{ Form::select('status', ['0' => 'Draft', '1' => 'Published'], Input::old('status'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('status') }}
						</div>
						</div>
					</div>

					
					<div class="row">

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('subscription') }}">						
						{{ Form::label('subscription', 'Subscription', ['class' => "text-danger"]) }}
						{{ Form::select('subscription', [ '2'=> 'Premium',  '1' => 'Verified', '0'=> 'None'] , Input::old('subscription'), array('class' => 'form-control')) }}
						{{ Form::errorMsg('subscription') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('subscription_expiry') }}">						
						{{ Form::label('subscription_expiry', 'Subscription Expiry', ['class' => "text-danger"]) }}
						{{ Form::text('subscription_expiry', Input::old('subscription_expiry'), array('class' => 'form-control')) }}
						{{ Form::errorMsg('subscription_expiry') }}
							</div>
						</div>

					</div>
					

					@endif

				</div>


				<h4 class="form-title">Details</h4>

				<div class="fieldset">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('coverage') }}">
							{{ Form::label('coverage', 'Coverage Area') }}
							{{ Form::textarea('coverage', Input::old('coverage'), array('class' => 'form-control nowysiwyg','rows' => '3', 'placeholder' => 'eg: Klang Valley')) }}
							{{ Form::errorMsg('coverage') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('work_hours') }}">
							{{ Form::label('work_hours', trans('admin.work-hours')) }}
							{{ Form::textarea('work_hours', Input::old('work_hours'), array('class' => 'form-control nowysiwyg', 'rows' => '3', 'placeholder' => "eg:Mon - Friday: 9am - 6pm")) }}
							{{ Form::errorMsg('work_hours') }}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('contact_info') }}">
							{{ Form::label('contact_info', trans('admin.contact-info')) }}
							{{ Form::textarea('contact_info', Input::old('contact_info'), array('class' => 'form-control nowysiwyg','rows' => '3')) }}
							{{ Form::errorMsg('contact_info') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('additional_charges') }}">
							{{ Form::label('additional_charges', 'Additional Charges') }}
							{{ Form::textarea('additional_charges', Input::old('additional_charges'), array('class' => 'form-control nowysiwyg', 'rows' => '3')) }}
							{{ Form::errorMsg('additional_charges') }}
							</div>
						</div>
					</div>

				</div>



				<h4 class="form-title">Lokasi</h4>

				<div class="fieldset">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('street') }}">
							{{ Form::label('street', trans('admin.street')) }}
							{{ Form::text('street', Input::old('street'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('street') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('district') }}">
							{{ Form::label('district', trans('admin.city')) }}
							{{ Form::text('district', Input::old('district'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('district') }}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('postcode') }}">
							{{ Form::label('postcode', 'Postcode') }}
							{{ Form::text('postcode', Input::old('postcode'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('postcode') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('state') }}">
							{{ Form::label('state', 'State') }}
							{{ Form::select('state', ['Johor' =>'Johor', 'Kedah' => 'Kedah', 'Kelantan' =>'Kelantan', 'Kuala Lumpur' => 'Kuala Lumpur', 'Melaka' => 'Melaka', 'Negeri Sembilan' => 'Negeri Sembilan', 'Pahang'=> 'Pahang', 'Perak' => 'Perak', 'Perlis' => 'Perlis', 'Pulau Pinang' => 'Pulau Pinang', 'Putrajaya' => 'Putrajaya', 'Sabah' => 'Sabah', 'Sarawak' => 'Sarawak', 'Selangor' => 'Selangor', 'Terengganu' => 'Terengganu', 'Wilayah Persekutuan - Labuan' => 'Wilayah Persekutuan - Labuan'], Input::old('state'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('state') }}
							</div>
						</div>
						<input name="country_id" value="107" type="hidden"/>
						{{--
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('country') }}">
							{{ Form::label('country_id', 'Country') }}
							{{ Form::select('country_id', $countries, Input::old('country_id'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('country_id') }}
							</div>
						</div>
						--}}
					</div>
				</div>

				<h4 class="form-title">Links</h4>

				<div class="fieldset">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('website') }}">
							{{ Form::label('website', 'Website Link') }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-link"></i></span>
								{{ Form::text('website', Input::old('website'), array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('website') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('instagram') }}">
							{{ Form::label('instagram', 'Instagram Link') }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-instagram"></i></span>
								{{ Form::text('instagram', Input::old('instagram'), array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('instagram') }}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('facebook') }}">
							{{ Form::label('facebook', 'Facebook Link') }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-facebook"></i></span>
								{{ Form::text('facebook', Input::old('facebook'), array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('facebook') }}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('twitter') }}">
							{{ Form::label('twitter', 'Twitter Link') }}
							<div class="input-group">
  								<span class="input-group-addon"><i class="fa fa-twitter"></i></span>
								{{ Form::text('twitter', Input::old('twitter'), array('class' => 'form-control')) }}
							</div>
							{{ Form::errorMsg('twitter') }}
							</div>
						</div>

						<!-- <div class="col-md-6">
							<div class="form-group {{ Form::errorClass('googleplus') }}">
							{{ Form::label('googleplus', 'Googleplus Link') }}
							{{ Form::text('googleplus', Input::old('googleplus'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('googleplus') }}
							</div>
						</div> -->
					</div>
					{{--
					<div class="row">
						<div class="col-md-6">
							<div class="form-group {{ Form::errorClass('twitter') }}">
							{{ Form::label('twitter', 'Twitter Link') }}
							{{ Form::text('twitter', Input::old('twitter'), array('class' => 'form-control')) }}
							{{ Form::errorMsg('twitter') }}
							</div>
						</div>
					</div>
					--}}
				</div>
			</div><!-- end tab -->


			<div class="tab-pane" id="tab-desc">

				<h4 class="form-title form-title-extend">Describe your Products and Services</h4>

				<div class="">
					<div class="form-group {{ Form::errorClass('description') }}">
					
					{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control wysiwyg',  'rows' => '20')) }}
					{{ Form::errorMsg('description') }}
					</div>
				</div>

			</div><!-- end tab -->

			
		  	<div class="tab-pane" id="tab-albums">
		  		<h4 class="form-title form-title-extend">Gallery / Albums</h4>

		  		<div class="alert alert-info hidden-xs">Pastikan saiz gambar berukuran kurang dari 2400 pixel (lebar) x 1800 (height) pixel. Proses muat-naik mungkin gagal jika gambar terlalu besar</div>
		  		@if(isset($vendor))
		  		<div>
		  		<a data-toggle="ajaxModal" href="{{ url('admin/vendors/'.$vendor->id . '/albums/create') }}" class="btn btn-info">New Album</a>
		  		</div>

		  		@foreach ($vendor->albums as $album)
		  			<div class="admin-album">
		  				<H4>{{ $album->name }}


          				</H4>
          				<div class="btn-group pull-right">
		  				 <a class="btn btn-default btn-fb btn-edit btn-sm" title="Edit" alt="Edit" data-toggle="ajaxModal" href="{{ url('admin/vendors/' . $vendor->id . '/albums/' . $album->id . '/edit') }}"><i class="fa fa-edit"></i> Edit</a>

          <a class="btn btn-default btn-warning btn-remove confirmation btn-sm" title="Delete" alt="Delete" href="{{ url('admin/vendors/' . $vendor->id . '/albums/' . $album->id . '/delete') }}"><i class="fa fa-times"></i> Delete</a>
          				</div>
		  				<div class="album-uploader" data-action="{{ url('/admin/Vendor/albumUpload') }}/{{$vendor->id}}/{{ $album->id }}"></div>

		  				<ul class="list-unstyled photo-list" id="photos-list-{{ $album->id }}">
		  					
							@foreach($album->photos as $photo)
								@include('admin.common.albumphoto', ['photo' => $photo])
							@endforeach
						</ul>
						<div class="clearfix"></div>
		  			</div>
		  		@endforeach

		  		@else
				<div class="alert alert-warning" role="alert">You can add/edit the photo album after you save this entry</div>
				@endif
		  	</div>

		  	{{--
			<div class="tab-pane" id="tab-traveltips">
				<h4 class="form-title form-title-extend">Travel Tips</h4>
				<div class="">
					<div class="form-group {{ Form::errorClass('traveltips') }}">
					{{ Form::textarea('traveltips', Input::old('traveltips'), array('class' => 'form-control')) }}
					{{ Form::errorMsg('traveltips') }}
					</div>
				</div>
			</div>
			--}}<!-- end tab -->

			@if(isset($vendor))
			
			<div class="tab-pane" id="tab-revisions">
				<h4 class="form-title form-title-extend">Revisions</h4>
				{{--
				<table class="table">
				  <thead>
					<tr>
					  <th>By</th>
					  <th width="15%">Date/Time</th>
					  <th>Field</th>
					  <th>Before</th>
					  <th>After</th>
					</tr>
				  </thead>
				  <tbody>
					@foreach($vendor->revisionHistory as $history)

					<tr>
					  <td>{{ User::find($history->user_id)->getFullname() }}</td>
					  <th>{{ $history->created_at }}</th>
					  <td><i>{{ ucfirst($history->key) }}</i></td>
					  <td>{{ $history->old_value }}</td>
					  <td>{{ $history->new_value }}</td>
					</tr>
					@endforeach
				  </tbody>
				</table>
				--}}
			</div><!-- end tab -->
			
			@endif

		</div>

		
	</div>

	<div class="col-md-3">
		@if( User::find( Auth::user()->id)->is('administrator') )

		<div class="btn-group" data-toggle="buttons">
			<label class="btn btn-default @if(isset($vendor) && $vendor->featured == 1) active @endif">
				{{ Form::checkbox('featured', 1, Input::old('featured')) }} <span class="glyphicon glyphicon-star"></span> Featured
			</label>
		</div>

		@endif

		<h4>Tags</h4>
		<input type="text" @if(isset($vendor) && $vendor->tags) value="{{ implode(',', $vendor->tags->lists('name')) }}" @endif class="tagsinput " name="tags" autocomplete="off" />

		<h4>Category</h4>
		<div class="form-group {{ Form::errorClass('genres') }}">

			<select class="form-control" name="genres[]">
			@foreach($genres as $genre)
			
			<option value="{{ $genre->id }}"
				@if(isset($vendor) && $vendor->genres->contains($genre->id)) selected="selected"
				@elseif(Input::old('genres') && in_array($genre->id, Input::old('genres')))  selected="selected" @endif
				>{{ $genre->name }}</option>
			
			@endforeach
			</select>
			
			{{ Form::errorMsg('genres') }}
		</div>

		<h4>Pricing Range</h4>
		<p class="text-muted">Do you serve budget, premium or luxury customer?</p>
		<?php
		$pricings = Pricing::all();
		$myPricing = [];
		if(!empty($vendor))
			$myPricing = DB::table('vendor_pricing')->where('vendor_id', '=', $vendor->id)->lists('pricing_id');

		?>
		@foreach($pricings as $p)
			<div class="checkbox">
			<label>
			<input type="checkbox" value="{{ $p->id }}" name="pricing[]"
				@if(isset($vendor) && in_array((int)$p->id, $myPricing))  checked="checked"
				@elseif(Input::old('pricing') && in_array($p->id, Input::old('pricing'))) checked="checked" @endif
				>
			{{ $p->name }}
			</label>
			</div>
		@endforeach

		

		<h4>Cover Image</h4>
		<div class="thumbnail">
			<img style="width:100%" src="{{ isset($vendor) ? asset($vendor->cover_thumb) : '' }}" alt="" id="cover-thumb">
		</div>
		<div id="cover-fine-uploader" class="attachment" data-action="{{ url('/admin/Vendor/coverUpload') }}/{{ isset($vendor) ? $vendor->id: 0 }}"></div>
		

		{{--
		<h4>Other images</h4>
						@if(isset($vendor))
						<ul class="list-unstyled" id="photos-list">
							@foreach($vendor->photos as $photo)
								@include('admin.common.photolist', ['photo' => $photo])
							@endforeach
						</ul>
						
						<div id="photo-fine-uploader" class="attachment" data-action="{{ url('/admin/Vendor/photoUpload') }}/{{$vendor->id}}"></div>
						@else
						<div class="alert alert-warning" role="alert">You can add/edit the addtional image after you save this entry</div>
						@endif --}}

	</div>

	<div class="col-md-12">
		<hr/>
		{{ Form::submit(((isset($vendor)) ? 'Update' : 'Add New Entry'), array('class' => 'btn btn-primary btn-lg')) }}
	</div>
</div>
{{ Form::close() }}

@stop

@if(isset($vendor))
@section('photo-modal'){{ url("/admin/selectPhoto/festival/" . $vendor->id) }}@stop
@endif

@section('scripts')
<style>
.redactor-dropdown .redactor-formatting-h3-package-title{
	font-size: 18px;
	font-weight: bold;
	color: #ff6699 !important;
}
</style>
	<script>
		$(document).ready(function() {

			// enable wysiwyg editor
			$(function()
			{
			    $('textarea:not(.nowysiwyg)').redactor({
	                focus: true,
	                plugins: ['fontcolor', 'table','video'],
	                formatting: ['p', 'blockquote', 'pre', 'h2', 'h3'],
	                formattingAdd: [
					    {
					        tag: 'h3',
					        title: 'Package Name',
					        class: 'package-title'
					    }]
	            });
			});

			// Content tagging
			$('.tagsinput').tagsinput({
				typeahead: {
					source: function(query) {
							return $.getJSON('/tags');
						},
					freeInput: false
				}});
			
			$('form[name="entryForm"]').validate({
				rules: {
					name: "required",
					email: "required",
					phone: "required",
				},
					messages: {
					name: "Name cannot be empty",
					email: "Please add your email",
					phone: "Please add your phone number",
				}
			});
		});
	</script>
@stop