<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImportUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$importUsers = ImportUser::where('block', '!=', 1)->get();
		foreach( $importUsers as $data )
		{
			
			// make sure no username clash
			if( User::where('email', '=', $data->email)->count() == 0 ){

					$user = new Toddish\Verify\Models\User;
					$user->username = $data->username;
					$user->email = $data->email;

					$password = explode(':', $data->password);

					$user->password = $password[0];
					$user->salt = isset($password[1]) ? $password[1]: '';
					$user->verified = 1;
					$user->firstname = $data->name;
					$user->lastname = '';
					$user->save();

					$user->roles()->sync(array( 4 ));

			}
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
