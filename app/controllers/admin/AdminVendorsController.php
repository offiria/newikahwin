<?php

class AdminVendorsController extends BaseController {

	public function getIndex()
	{
		$query = Vendor::query();

		if(Input::has('keyword')) {
			$query->where('name', 'LIKE', '%'.Input::get('keyword').'%');
		}

		if(Input::has('user_id') && Input::get('user_id') != 0 ) {
			$query->where('user_id', '=', Input::get('user_id'));
		}

		if(Input::has('status')) {
			$query->where('status', '=', Input::get('status'));
		}

		if(Input::has('month')) {
			$query->searchInMonth(Input::get('month'));
		}

		// make sure not empty
		$query->where('name', '<>', '');

		if( User::find( Auth::user()->id)->is('administrator') ) {
			$query->orderBy('updated_at', 'DESC');
		} else if(User::find( Auth::user()->id)->is('vendor')) {
			$query->where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC');
		} else {
			// error
		}


		$users = Toddish\Verify\Models\Role::where('name', '=', 'vendor')->first()->users;
		$owners = ['0' => '-- Owner --'];
		foreach ($users as $owner) {
			$owners[$owner->id] = $owner->firstname . ' ' . $owner->lastName;
		}

		$festivals = $query->paginate(20);


		return View::make('admin.vendors.index', ['vendors' => $festivals, 'owners' => $owners]);
	}

	public function getCreate()
	{
		$user = Auth::user();
		$genres = Genre::where('parent_id', '=', '0')->get();
		$countries = DB::table('countries')->lists('name', 'id');
		$lastFestivals = Vendor::where('user_id', '=', $user->id)->lists('name', 'id');
		$users = Toddish\Verify\Models\Role::where('name', '=', 'vendor')->first()->users;
		
		// convert to arrays
		$owners = [];
		foreach ($users as $owner) {
			$owners[$owner->id] = $owner->firstname . ' ' . $owner->lastName;
		}

		return View::make('admin.vendors.create-edit', ['genres' => $genres, 'countries' => $countries, 
			'lastFestivals' => $lastFestivals,
			'owners' => $owners]);
	}

	public function getEdit($id)
	{
		$user = Auth::user();
		$vendor = Vendor::find($id);
		$genres = Genre::where('parent_id', '=', '0')->get();
		$countries = DB::table('countries')->lists('name', 'id');
		$lastFestivals = Vendor::where('user_id', '=', $user->id)->lists('name', 'id');
		$users = Toddish\Verify\Models\Role::where('name', '=', 'vendor')->first()->users;


		// check for permission
		if( !User::find( $user->id)->is('administrator') ){	
			if($user->id != $vendor->user_id){
				return Redirect::to('admin')->with('message','Permission denied');
			}
		}

		// convert to arrays
		$owners = [];
		foreach ($users as $owner) {
			$owners[$owner->id] = $owner->firstname . ' ' . $owner->lastName;
		}

		asort($owners, SORT_STRING);

		return View::make('admin.vendors.create-edit', ['vendor' => $vendor, 
			'genres' => $genres, 
			'countries' => $countries, 
			'lastFestivals' => $lastFestivals, 
			'packages' => $vendor->packages,
			'owners' => $owners ]);
	
	}

	public function postEdit($id = null)
	{
		$user = Auth::user();
		$vendor = Vendor::FindOrNew($id);

		// check for permission
		if( !User::find( $user->id)->is('administrator') ){	
			if($id != null && $user->id != $vendor->user_id){
				return Redirect::to('admin')->with('message','Permission denied');
			}
		}


	
		$rules = [
			'name' => 'required',	
			'genres' => 'required',
			'email' => 'required',
			'phone' => 'required',
			'street' => 'required',
			'district' => 'required',
			'coverage' => 'required'
		];

		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {
			// clean input
			//$input['description'] 			= Purifier::clean($input['description']);

			$vendor = Vendor::FindOrNew($id);

			// set to current user if not specified
			if(empty($input['user_id'])){
				$input['user_id'] = Auth::user()->id;
			}

			// Checkbox unchecked value
			if( ! isset($input['featured'])) {
				$input['featured'] = 0;
			}

			if( empty($input['subscription_expiry'])){
				$input['subscription_expiry'] = '2014-09-24 18:19:11';
			}

			// preventing ->update() to avoid revisions from being added 
			foreach($input as $k => $v) {
				if($k == 'genres' || $k == 'tags' || $k == '_token' || $k == 'pricing' || $k == 'photocaption') {
					continue;
				}

				$vendor->$k = $v;
			}
			



			if( $vendor->save()) {

				// Save genres
				if(Input::has('genres')) {
					$vendor->genres()->detach();
					foreach($input['genres'] as $genreId)
					{
						$vendor->genres()->attach($genreId);
					}
				}

				// save pricing
				if(Input::has('pricing')) {
					$vendor->pricings()->detach();
					foreach($input['pricing'] as $pricingId)
					{
						$vendor->pricings()->attach($pricingId);
					}
				}

				// save photo caption
				if(Input::has('photocaption')) {
					foreach($input['photocaption'] as $key => $caption)
					{
						$photo = Photo::find($key);
						if(!empty($photo)){
							if($photo->imageable_id == $vendor->id){
								$photo->caption = $caption;
								$photo->save();
							}
						}
					}
				}

				// Save tags
				if(Input::has('tags')) {
					$vendor->tags()->detach();
					foreach(explode(',', $input['tags']) as $tagName)
					{
						$tagName = trim($tagName);
						$tag = Tag::FirstOrNew(['name' => $tagName]);
						$tag->name = $tagName;
						$vendor->tags()->save($tag);
					}
				}

				if($id) {

					if( ! Auth::user()->allowManageUser()) {
						// Notify admin of the changes
						/*
						Mail::send(['html' => 'emails.festivals.changes'], ['festival' => $vendor], function($message) use ($vendor)
						{
						    $my = Auth::user();
						    $message->from('noreply@ikahwin.my', 'ikahwin.my');
						    $message->to('festivals@ikahwin.my', 'Admin')->subject('Changes to festival [' . $vendor->name . '] ');
						});
						*/
					}

					return Redirect::to('admin/vendors/'. $vendor->id . '/edit')
						->with('message', 'Listing updated');
				} else {
					return Redirect::to('admin/vendors')
							->with('message', 'The listing has been created');
				}
			}

		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}

	}

	public function getDelete($id)
	{
		$user = Auth::user();
		$vendor = Vendor::find($id);

		// check for permission
		if( !User::find( $user->id)->is('administrator') ){	
			if($user->id != $vendor->user_id){
				return Redirect::to('admin')->with('message','Permission denied');
			}
		}

		$vendor->packages()->delete();
		$vendor->delete();

		return Redirect::back()
			->with('message', 'Listing deleted');
	}
}