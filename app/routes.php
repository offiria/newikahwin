<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return View::make('hello');
// });


// old redirect
Route::get('188-kisah-benar-cinta-selepas-kahwin', function() { return Redirect::to('blog/kisah-benar-cinta-selepas-kahwin'); });
Route::get('blog/234-tips-perkahwinan-5-cara-menghilangkan-nafas-dan-mulut-berbau', function() { return Redirect::to('blog/tips-perkahwinan-5-cara-menghilangkan-nafas-dan-mulut-berbau'); });
Route::get('blog/245-kisah-benar-akhirnya-aku-jumpa-kebahagiaan-hidup', function() { return Redirect::to('blog/kisah-benar-akhirnya-aku-jumpa-kebahagiaan-hidup'); });
Route::get('/top/beauty-centre-spa', function() { return Redirect::to('top/health-beauty'); });
Route::get('/weddingstamp', function() { return Redirect::to('order/weddingstamp'); });
Route::get('/weddingcard', function() { return Redirect::to('order/weddingcard'); });
Route::get('/thankyoutag', function() { return Redirect::to('order/thankyoutag'); });


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => 'admin', 'before' => 'managers'), function() {
	Route::get('/', function() {
		if( User::find( Auth::user()->id)->is('administrator') )
			return Redirect::to( 'admin/users');

		if( User::find( Auth::user()->id)->is('vendor') )
			return Redirect::to( 'admin/vendors');

		if( User::find( Auth::user()->id)->is('contributor') )
			return Redirect::to( 'admin/stories');

		return Redirect::to( '/');
	});

	Route::get('users', 'AdminUsersController@getIndex');
	Route::get('users/create', 'AdminUsersController@getCreate');
	Route::post('users/create', array('before'=>'csrf', 'uses'=>'AdminUsersController@postEdit'));
	Route::get('users/{id}/edit', 'AdminUsersController@getEdit');
	Route::post('users/{id}/edit', 'AdminUsersController@postEdit');
	Route::post('users/{id}/upload', 'AdminUsersController@postAvatar');
	Route::get('users/{id}/toggle/{field}/{param?}', 'AdminUsersController@getAjaxToggle');

	Route::get('messages/unread', 'AdminMessagesController@getUnread');
	Route::get('messages/all', 'AdminMessagesController@getIndex');

	Route::get('vendors', 'AdminVendorsController@getIndex');
	Route::get('vendors/create', 'AdminVendorsController@getCreate');
	Route::post('vendors/create', array('before'=>'csrf', 'uses'=>'AdminVendorsController@postEdit'));
	Route::get('vendors/{id}/edit', 'AdminVendorsController@getEdit');
	Route::post('vendors/{id}/edit', array('before'=>'csrf', 'uses'=>'AdminVendorsController@postEdit'));
	Route::get('vendors/{id}/delete', 'AdminVendorsController@getDelete');
	
	Route::get('vendors/{id}/packages/create', 'AdminPackagesController@getCreateModal');
	Route::get('vendors/{id}/packages/{packageid}/edit', 'AdminPackagesController@getEditModal');
	Route::get('vendors/{id}/packages/{packageid}/delete', 'AdminPackagesController@getDelete');
	Route::post('vendors/{id}/packages/store', 'AdminPackagesController@postStore');

	Route::get('vendors/{id}/albums/create', 'AdminAlbumController@getCreateModal');
	Route::get('vendors/{id}/albums/{packageid}/edit', 'AdminAlbumController@getEditModal');
	Route::get('vendors/{id}/albums/{packageid}/delete', 'AdminAlbumController@getDelete');
	Route::post('vendors/{id}/albums/store', 'AdminAlbumController@postStore');

	Route::get('orders', 'AdminOrdersController@getIndex');
	Route::get('orders/{id}/edit', 'AdminOrdersController@getEdit');
	Route::post('orders/{id}/edit', 'AdminOrdersController@postEdit');
	Route::get('orders/{id}/delete', 'AdminOrdersController@getDelete');

	Route::get('stories', 'AdminStoriesController@getIndex');
	Route::get('stories/create', 'AdminStoriesController@getCreate');
	Route::post('stories/create', array('before'=>'csrf', 'uses'=>'AdminStoriesController@postEdit'));
	Route::get('stories/{id}/edit', 'AdminStoriesController@getEdit');
	Route::post('stories/{id}/edit', array('before'=>'csrf', 'uses'=>'AdminStoriesController@postEdit'));
	Route::get('stories/{id}/delete', 'AdminStoriesController@getDelete');

	Route::get('stories/categories', 'AdminStoryCategoryController@getIndex');
	Route::get('stories/categories/create', 'AdminStoryCategoryController@getCreate');
	Route::post('stories/categories/create', array('before'=>'csrf', 'uses'=>'AdminStoryCategoryController@postEdit'));
	Route::get('stories/categories/{id}/edit', 'AdminStoryCategoryController@getEdit');
	Route::post('stories/categories/{id}/edit', array('before'=>'csrf', 'uses'=>'AdminStoryCategoryController@postEdit'));

	Route::post('stories/images/upload', 'AdminDashboardController@postBlogImage');

	Route::get('countries', 'AdminCountriesController@getIndex');
	Route::post('countries', 'AdminCountriesController@postEdit');

	Route::post('{classname}/coverUpload/{id}', 'AdminDashboardController@postCover');
	Route::post('{classname}/photoUpload/{id}', 'AdminDashboardController@postPhoto');
	Route::post('{classname}/albumUpload/{id}/{album_id}', 'AdminDashboardController@postAlbumPhoto');
	Route::post('stories/images/upload', 'AdminDashboardController@postBlogPhoto');

	Route::post('photoDelete/{id}', 'AdminDashboardController@postDelete');
	Route::get('selectPhoto/{type}/{id}/{element}', 'AdminDashboardController@selectPhoto');
});

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/

Route::get('/', array('as'=>'home', 'uses'=>'HomeController@showWelcome'));

// User registration and authentication
Route::get('register', array('uses'=>'UsersController@getRegister'));
Route::post('register', array('before'=>'csrf', 'uses'=>'UsersController@postRegister'));

// vendor registration
Route::get('register/vendor', array('uses'=>'UsersController@getVendorRegister'));
Route::post('register/vendor', array('before'=>'csrf', 'uses'=>'UsersController@postVendorRegister'));
Route::get('register/packages', array('uses'=>'UsersController@getSelectPackages'));

Route::get('register/validate/{email}/{token}', array('uses'=>'UsersController@getValidate'));
Route::get('login/facebook', array('uses'=>'UsersController@getLoginFB'));
Route::get('login/facebook/callback', array('uses'=>'UsersController@getLoginFBCallback'));
Route::get('login/{redirect?}', array('as'=>'login', 'uses'=>'UsersController@getLogin'));
Route::post('login', array('before'=>'csrf', 'uses'=>'UsersController@postLogin'));
Route::get('logout', array('as'=>'logout', 'uses'=>'UsersController@getLogout'));
Route::get('login/reset', array('uses' => 'UsersController@getRemind', 'as' => 'password.remind'));
Route::post('login/reset', array('uses' => 'UsersController@postRequest', 'as' => 'password.request'));
Route::get('login/reset/{token}', array('uses' => 'UsersController@getReset', 'as' => 'password.reset'));
Route::post('login/reset/{token}', array('uses' => 'UsersController@postUpdate', 'as' => 'password.update'));

// User profile
// Route::get('profile/account', 'ProfileController@getAccount');
// Route::post('profile/account', array('before'=>'csrf', 'uses' => 'ProfileController@postAccount'));
// Route::post('profile/profile_picture', array('before'=>'auth', 'uses' => 'ProfileController@uploadProfile'));
// Route::get('profile/plans', array('before'=>'lawyer', 'uses' => 'ProfileController@plans'));
// Route::get('profile/password', 'ProfileController@getChangePassword');
// Route::post('profile/password', array('before'=>'csrf', 'uses' => 'ProfileController@postChangePassword'));

// Stories
Route::get('blog/submit', array('uses'=>'StoriesController@getCreate'));
Route::post('blog/submit', array('uses'=>'StoriesController@postCreate'));
Route::get('blog', array('uses'=>'StoriesController@getIndex'));
Route::get('blog/category/{slug}', array('uses'=>'StoriesController@getIndex'));
Route::get('blog/{slug}', array('uses'=>'StoriesController@getView'));

// vendors
Route::get('vendors/submit', array('uses'=>'VendorsController@getCreate'));
Route::post('vendors/submit', array('uses'=>'VendorsController@postCreate'));
Route::get('vendors', array('uses'=>'VendorsController@getIndex', 'as' => 'vendors'));
Route::get('vendors/tag/{tag_slug}', array('uses'=>'VendorsController@getIndexTag'));
Route::get('vendors/places', array('uses'=>'VendorsController@getCountriesIndex'));
Route::get('vendors/genres', array('uses'=>'VendorsController@getGenresIndex'));
Route::get('vendors/genres/{slug}', array('uses'=>'GenresController@getIndex'));
Route::get('vendors/smsall', array('uses'=>'VendorsController@smsAllVendor'));
Route::get('vendors/{id}', array('uses'=>'VendorsController@getRedirect'))->where('id', '[0-9]+');
Route::get('vendors/{category}', array('uses'=>'VendorsController@getIndex'));
Route::get('vendors/{category}/{slug}_i{old_id}', array('uses'=>'VendorsController@getView'))->where('old_id', '[0-9]+');;
Route::get('vendors/{category}/{slug}', array('uses'=>'VendorsController@getView'));
Route::post('vendors/{category}/{slug}/sms', array('uses'=>'VendorsController@postSms'));
Route::post('vendors/{category}/{slug}/report', array('uses'=>'VendorsController@postReport'));

// Top vendors
Route::get('top/{category}', array('uses'=>'VendorsController@getTopIndex'));

// Route::get('what-to-do/genres/{slug}', array('uses'=>'GenresController@getView'));

Route::get('where-to-go', array('uses'=>'CountriesController@getIndex'));
// Route::get('where-to-go/{country}', array('uses'=>'CountriesController'));

// Route::get('what-to-see', array('uses'=>'ActivitiesController'));
// Route::get('what-to-see/{fest}', array('uses'=>'ActivitiesController'));

Route::get('tags', array('uses'=>'TagsController@getTags'));

// Likes
Route::get('likes', 'LikesController@getIndex');
Route::post('likes/like/{type}/{id}', array('uses'=>'LikesController@postLike'));
Route::post('likes/unlike/{type}/{id}', array('uses'=>'LikesController@postUnlike'));

// tracking
Route::get('tracking', 'TrackingController@getIndex');
Route::get('tracking/{trackingId}', 'TrackingController@getIndex');

// newsletter test
Route::get('newsletter/{slug}', function($slug){ 
	if( file_exists( base_path() . '/app/views/newsletter/'. $slug. '.blade.php')){
		$content = View::make('newsletter/'. $slug); 
		$content = str_replace('<p>', '<p style="font-family:Helvetica,sans-serif;font-size:16px;line-height:160%;margin-top:13px;margin-bottom:15px;">', $content);
		$content = str_replace('<h4>', '<h4 style="font-family:Helvetica,sans-serif;color:#384047;display:block;font-size:16px;font-weight:bold;line-height:130%;letter-spacing:normal;margin-right:0;margin-left:0;margin-top:15px;margin-bottom:15px;text-align:left;">', $content);
		return $content;
	} else
		App::abort(404);
});

// Static pages
Route::get('home', function(){ return View::make('static/home'); });
Route::get('packages-single', function(){ return View::make('static/packages-single'); });
Route::get('submit-story', function(){ return View::make('static/submit-story'); });
Route::get('submit-vendor-single', function(){ return View::make('static/submit-vendor-single'); });
Route::get('about', function(){ return View::make('static/about'); });
Route::get('contact', function(){ return View::make('static/contact'); });
Route::get('privacy-policy', function(){ return View::make('static/privacy-policy'); });
Route::get('faq', function(){ return View::make('static/faq'); });

Route::get('vendormail', function(){ return View::make('emails.auth.vendor'); });

// Order Payment
Route::get('order/{product}/pay/{service}/{slug}', array('uses'=>'OrdersController@showPaymentRedirect'));
Route::post('order/response/{service}', array('uses'=>'OrdersController@postPaymentResponse'));
Route::get('order/success', function() { return View::make('orders/payment-success'); });

// Order pages
Route::get('order/{product}', array('uses'=>'OrdersController@showView'));
Route::post('order/{product}', array('uses'=>'OrdersController@postOrder'));




// catch all other static pages
Route::get('{slug}', function($slug){ 
	// echo base_path() . '/app/view/static/'. $slug. '.blade.php'; exit;
	if( file_exists( base_path() . '/app/views/static/'. $slug. '.blade.php'))
		return View::make('static/'. $slug); 
	else
		App::abort(404);
});

