@extends('admin.layouts.default')

@section('content')

<style>
	/* @TODO: LESS */
	.orders {
		overflow: auto;
		/*overflow-y: hidden;*/
	}
</style>

<h2 class="admin-title">Orders</h2>

<div class="admin-search">
	<form role="form" id="user-filter">

		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>
		
		<div class="admin-filter">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						{{ Form::select('product', array('' => '-- Product --', 'weddingstamp' => 'Wedding Stamp', 'weddingcard' => 'Wedding Card', 'thankyoutag' => 'Thank You Tags'), Input::get('product', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('paymethod', array('' => '-- Payment Method --', Order::PAYMETHOD_ONLINETRANSFER => Order::PAYMETHOD_ONLINETRANSFER, Order::PAYMETHOD_CREDITCARD => Order::PAYMETHOD_CREDITCARD, Order::PAYMETHOD_OFFLINE => Order::PAYMETHOD_OFFLINE), Input::get('paymethod', ''), array('class' => 'form-control')) }}
					</div>


					<div class="col-md-1">
						{{ Form::select('status', array('' => '-- Status --', '0' => 'Pending', 
							Order::STATUS_FAIL => 'Failed', 
							Order::STATUS_SUCCESS => 'Completed',
							Order::STATUS_APPROVAL => 'Pending Approved',
							Order::STATUS_APPROVED => 'Design Approved',
							Order::STATUS_CONSIGNMENT => 'Consignment',
							Order::STATUS_DELIVERED => 'Delivered'), Input::get('status', ''), array('class' => 'form-control')) }}
					</div>

					<div class="col-md-1">
						{{ Form::select('month', array('' => '-- Month --', 
							'2014-10-1' => 'Oct 2014', 
							'2014-11-1' => 'Nov 2014', 
							'2014-12-1' => 'Dec 2014', 
							'2015-01-1' => 'Jan 2015'), 
							Input::get('month', ''), array('class' => 'form-control')) }}
					</div>

					<div class="col-md-1 col-md-offset-5">
						<div class="btn btn-default">Total: RM {{ $total }} </div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="orders">
	<table class="table table-hover table-bordered admin-table">
		<thead>
		<tr>
			<th>#</th>
			<th>Date</th>
			<th>Lapse</th>
			<th>Name</th>
			<th>E-Mail</th>
			<th>Phone</th>
			<th>Product</th>
			<!-- <th>Penghantaran</th> -->
			<th>Total</th>
			<th>Payment Method</th>
			<!-- <th>Status</th> -->
			<th width="5%" colspan="2">Action</th>
		</tr>
		</thead>
		<tbody>
		@foreach($orders as $i => $order)
		<tr class="status-{{ $order->status }}">
			<td>{{{ $order->id }}}</td>
			<td>{{{ $order->created_at->toDateString() }}}</td>
			<td>{{ $order->created_at->diffForHumans() }}</td>
			<td>{{{ $order->name }}}</td>
			<td>{{{ $order->email }}}</td>
			<td>{{{ $order->phone }}}</td>
			<td>{{{ $order->product }}}</td>
			<!-- <td>{{{ $order->penghantaran }}}</td> -->
			<td>{{{ $order->total }}}</td>
			<td>{{{ $order->payment_method }}}</td>
			<!-- <td>{{{ $order->getStatus() }}}</td> -->
			<td>
			<a class="btn btn-default btn-fb btn-block btn-edit" href="<?php echo url('admin/orders/' . $order->id) . '/edit'; ?>"><i class="fa fa-edit"></i></a>
		</td>
			<td>
				@if($order->status < Order::STATUS_SUCCESS )
			<a class="btn btn-default btn-fb btn-block btn-edit" href="<?php echo url('admin/orders/' . $order->id) . '/delete'; ?>"><i class="fa fa-trash-o"></i>
				@endif
			</a>
		</td>
		</tr>
		@endforeach
		</tbody>
	</table>
</div>

{{ $orders->links() }}

@stop