@extends('admin.layouts.default')

@section('content')

<a class="btn btn-yellow pull-right" href="<?php echo url('admin/users/create'); ?>"><i class="fa fa-plus"></i> Add User</a>
<h2 class="admin-title">Users</h2>

<div class="admin-search">
	<form role="form" id="user-filter">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>
		<div class="admin-filter">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						{{ Form::select('role', array('' => '-- Role --') + $roles, Input::get('role', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('verified', array('' => '-- Verified --', 1 => 'Yes', 0 => 'No'), Input::get('verified', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('disabled', array('' => '-- Disabled --', 1 => 'Yes', 0 => 'No'), Input::get('disabled', ''), array('class' => 'form-control')) }}
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<table class="table table-hover table-bordered admin-table">
	<thead>
	<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>E-Mail</th>
		<th class="text-center">Verified</th>
		<th class="text-center">Disabled</th>
		<th>Roles</th>
		<th>Date Joined</th>
		<th>ID</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	@foreach($users as $i => $user)
	<tr>
		<td>{{{ $user->firstname }}}</td>
		<td>{{{ $user->lastname }}}</td>
		<td>@if( $user->uid ) <i class="fa fa-facebook-square"></i> @endif {{{ $user->email }}} </td>
		<td class="text-center"><a href="#toggle" @if($user->rolename == 'admin') data-sendemail="1" @endif data-ajax="{{ url('admin/users/'. $user->id . '/toggle/verified') }}" class="btn btn-default btn-verified @if($user->verified) active @endif"><i class="fa fa-check"></i></a></td>
		<td class="text-center"><a href="#toggle" data-ajax="{{ url('admin/users/'. $user->id . '/toggle/disabled') }}" class="btn btn-default btn-disabled @if($user->disabled) active @endif"><i class="fa fa-ban"></i></a></td>
		<td>
			<?php
			$prefix = '';
			$roleList = '';
			foreach ($user->roles as $role) {
				$roleList .= $prefix . $role['name'];
				$prefix = ', ';
			}
			?>
			{{ $roleList }}
		</td>
		<td>{{{ $user->created_at->toFormattedDateString() }}}</td>
		<td>{{{ $user->id }}}</td>
		<td><a class="btn btn-default btn-fb btn-block btn-edit" href="<?php echo url('admin/users/' . $user->id . '/edit'); ?>"><i class="fa fa-edit"></i></a></td>
	</tr>
	@endforeach
	</tbody>
</table>

{{ $users->links() }}

@stop