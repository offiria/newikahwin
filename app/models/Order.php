<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Order extends \Eloquent {

	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

	const STATUS_PENDING = 0;
	const STATUS_FAIL = 1;
	const STATUS_SUCCESS = 2;
	const STATUS_APPROVAL = 3;
	const STATUS_APPROVED = 5;
	const STATUS_CONSIGNMENT = 7;
	const STATUS_DELIVERED = 10;

	const PAYMETHOD_ONLINETRANSFER = 'Online Transfer';
	const PAYMETHOD_CREDITCARD = 'Credit Card';
	const PAYMETHOD_OFFLINE = 'Offline / Bank-in';

	protected $fillable = ['name', 'phone', 'product', 'email', 'address', 'penghantaran', 'total', 'payment_method', 'data', 'status', 'source'];

	public function getFillable()
    {
        return $this->fillable;
    }

    public function scopeFindSlug($query, $slug){
		return $query->where('slug', '=', $slug);
	}

	public function getStatus()
	{
		switch ($this->status) {
			case Order::STATUS_PENDING:
				return 'Unpaid';
			case Order::STATUS_FAIL:
				return 'Fail';
			case Order::STATUS_SUCCESS:
				return 'New order';
			case Order::STATUS_APPROVAL:
				return 'Pending approval';
			case Order::STATUS_APPROVED:
				return 'Design approved';
			case Order::STATUS_CONSIGNMENT:
				return 'Consignment';
			case Order::STATUS_DELIVERED:
				return 'Delivered';
		}
	}
}