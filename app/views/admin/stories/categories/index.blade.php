@extends('admin.layouts.default')

@section('content')

<a class="btn btn-yellow pull-right" href="<?php echo url('admin/stories/categories/create'); ?>"><i class="fa fa-plus"></i> Add Category</a>

<h2 class="admin-title">Categories</h2>
<br>
<table class="table table-hover table-bordered admin-table">
	<thead>
	<tr>
		<th>Category Name</th>
		<th width="10%"></th>
	</tr>
	</thead>
	<tbody>
	@foreach($categories as $i => $category)
	<tr>
		<td>{{{ $category->name }}}</td>
		<td><a class="btn btn-default btn-fb btn-block btn-edit" href="<?php echo url('admin/stories/categories/' . $category->id . '/edit'); ?>"><i class="fa fa-edit"></i></a></td>
	</tr>
	@endforeach
	</tbody>
</table>

{{ $categories->links() }}

@stop