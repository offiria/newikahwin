<?php

class Like extends \Eloquent {
	protected $fillable = ['likeable_id', 'likeable_type', 'user_id'];

	public function likeable()
    {
        return $this->morphTo();
    }
}