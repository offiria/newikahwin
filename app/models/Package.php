<?php

class Package extends BaseModel {
	protected $fillable = [];

	public function festival() {
		return $this->belongsTo('Festival', 'festival_id', 'id');
	}
}