<?php
$ipay88Fields = $ipay88->getFields();
?>

@extends('layouts.default')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-1">
			<script>
				$(document).ready(function(){
					ePayment.submit();
				});
			</script>

			<h2>Redirecting to ipay88...</h2>

			@if( ! empty($ipay88Fields))
				<form action=" {{ $ipay88->getTransactionUrl() }}" method="post" id="ePayment">
					<table style="display: none;"> <!-- 'block' to debug -->
						@foreach ($ipay88Fields as $key => $val)
							<tr>
								<td><label>{{ $key }}</label></td>
								<td><input type="text" name="{{ $key }}" value="{{ $val }}" /></td>
							</tr>
						@endforeach
						<tr>
							<td colspan="2"><input type="submit" value="Submit" name="Pay with IPay88" /></td>
						</tr>
					</table>
				</form>
			@endif

		</div>
	</div>
</div>

@stop