<?php

class Taggable extends \Eloquent {
	protected $fillable = ['tag_id', 'taggable_id', 'taggable_type'];
}