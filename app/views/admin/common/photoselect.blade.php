<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">

        <ul>
	@foreach($photos as $photo)
	<li>
		<img width="64" src="{{ asset($photo->path) }}" />
	<a href="#" onclick="insertPhoto('{{ asset($photo->path) }}')">Insert</a>
	</li>
	@endforeach
</ul>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>


<script type="text/javascript">
function insertPhoto(src){
	var editor = $('{{$element}}').data("wysihtml5").editor;
	editor.composer.commands.exec("insertHTML", '<img src="'+src+'" />');
}
</script>