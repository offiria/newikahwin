@extends('layouts.default')

@section('content')

<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">Genres</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="places-content">
				<div class="row place-items">
					@foreach($genres as $genre)	
					@if( $genre->festivals()->published()->count() > 0 )
					<div class="col-md-3 place-item">
						<div class="thumbnail">
							<img src="{{ asset($genre->festivals()->published()->random()->first()->cover_img) }}">
							<div class="box-content">
								<h3><a href="{{ URL::to('festivals?genre=' . $genre->slug) }}">{{ $genre->name }}</a></h3>
							</div>
						</div>
					</div>
					@endif
					@endforeach
				</div>				
			</div>
		</div>
		<div class="text-center">
			{{ $genres->links() }}
		</div>
	</div>
</div>

@stop