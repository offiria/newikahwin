@extends('layouts.default')
@section('title', $pageTitle)

@section('content')


<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">{{ $pageTitle }}</h1>
		<span>{{ trans('vendor.subtitle_vendor') }}</span>
	</div>
	<div class="row">
		<hr/>
	</div>

	{{--
	@if($vendors)
	<div class="row">
		<div class="col-md-5">
			<div class="vendor-content">
				@foreach($vendors as $vendor)
				<div class="thumbnail">
					<img src="{{ asset( $vendor->cover_thumb ) }}" style="width:100%">
				</div>
				@endforeach
			</div>
		</div>
	</div>
	@endif
	--}}

	<!-- <img src="{{ asset('assets/img/top-vendor.png') }}" style="width:100%"/> -->

	@if($featured && $showFeatured)
		<div class="row vendor-featured">
			@foreach($featured as $vendor)
				@include('vendors.itemindex', array('vendor'=> $vendor, 'col_size' => 2, 'showtag' => true ) )
			@endforeach
		<div class="clearfix"></div>
		@if( $featured->count() > 0 )
		<hr/>
		@endif
	</div>
	@endif

	@if($vendors)
	<div class="row">
		<div class="col-md-12">
			<div class="vendor-content">
			<?php $i=0; ?>
				@foreach($vendors as $vendor)

					@if($i % Config::get('app.vendors_col_size')  == 0)
					<div class="row vendor-items">
					@endif
					<?php $i++; ?>

					@include('vendors.itemindex', array('vendor'=> $vendor, 'col_size' => Config::get('app.vendors_col_size') ))

					@if( ($i) % Config::get('app.vendors_col_size')  == 0 || $i == count($vendors))
					</div>
					@endif

				@endforeach
			</div>
		</div>
	</div>

	@if(isset($vendors))
	<div class="text-center">
		{{ $vendors->links() }}
	</div>
	@endif

	@else
		No vendors
	@endif
</div>
@stop