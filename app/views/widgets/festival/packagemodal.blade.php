<!-- Modal -->
<div class="modal modal-packages fade" id="packageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">

         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="myModalLabel">Hassle-free festive travels.</h4>
            <h3 class="modal-sub-title text-center">
               Same price, just save time on research <br>and save money on mistakes.
            </h3>
         </div>
      
         <div class="modal-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-dotted" role="tablist">
               <li class="active"><a href="#customised-package" role="tab" data-toggle="tab">Customised Luxury Package</a></li>
               <li><a href="#package" role="tab" data-toggle="tab">Standard Package</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
               <div class="tab-pane active" id="customised-package">
                  <h3 class="title-note text-center">
                     Our agent will get in touch within 24 hours (weekdays only) to offer end-to-end options for a truly pampered festive travel experience.
                  </h3>
                  <form role="form">
                     <h3 class="form-title text-center">Festival Details</h3>
                     <label for="">Festival</label>
                     <div class="input-group">
                        <input type="text" class="form-control" name="name" value="{{{ Input::get('name') }}}">
                        <span class="input-group-btn">
                           <button class="btn btn-default" type="submit">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div><!-- /input-group -->

                      <div class="input-group">
                        <input type="text" class="form-control" name="name" value="{{{ Input::get('name') }}}">
                        <span class="input-group-btn">
                           <button class="btn btn-default" type="submit">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div><!-- /input-group -->

                     <label for="">Dates</label>
                     <div class="form-group input-group date">
                        <label for="" class="sr-only"></label>
                        <input type="text" readonly class="form-control  uneditable-input" style="cursor:pointer; background-color: #fff;" id="" placeholder="Dates" name="date" value="{{{ Input::get('date') }}}"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     </div>

                     <div class="form-group input-group date">
                        <label for="" class="sr-only"></label>
                        <input type="text" readonly class="form-control  uneditable-input" style="cursor:pointer; background-color: #fff;" id="" placeholder="Dates" name="date" value="{{{ Input::get('date') }}}"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     </div>

                     <label for="">Pax</label>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Number of Adults">
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Number of Children">
                     </div>

                  </form>

                  <div class="form-separator"></div>

                  <form>
                     <h3 class="form-title text-center">Your Details</h3>

                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Full Name">
                     </div>
                     <div class="form-group">
                        <input type="tel" class="form-control" id="" placeholder="Phone Number">
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control" id="" placeholder="Email Address">
                     </div>
                  </form>
               </div>

               <div class="tab-pane" id="package">
                  <ul class="list-unstyled package-details text-center">
                     <li>
                        <h5>Hotel</h5>
                        • # Star Hotel/ BnB is the closest medium range 
                        hotel (there’s a shuttle to the festival from the 
                        hotel/ The festival is within walking distance of 
                        the festival) x Y nights for 2 persons
                     </li>
                     <li>
                        <h5>Breakfast</h5>
                        • Including breakfast/Without Breakfast
                     </li>
                     <li>
                        <h5>Festival Ticket</h5>
                        • Festival ticket (what type)
                     </li>
                     <li>
                        <h5>Package</h5>
                        2 persons x 4 nights
                     </li>
                     <li class="total-price">
                        <h5>Total Price</h5>
                        MYR 560
                     </li>
                  </ul>
                  <form role="form">
                     <h3 class="form-title text-center">Festival Details</h3>
                     <label for="">Festival</label>
                     <div class="input-group">
                        <input type="text" class="form-control" name="name" value="{{{ Input::get('name') }}}">
                        <span class="input-group-btn">
                           <button class="btn btn-default" type="submit">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div><!-- /input-group -->

                      <div class="input-group">
                        <input type="text" class="form-control" name="name" value="{{{ Input::get('name') }}}">
                        <span class="input-group-btn">
                           <button class="btn btn-default" type="submit">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div><!-- /input-group -->

                     <label for="">Dates</label>
                     <div class="form-group input-group date">
                        <label for="" class="sr-only"></label>
                        <input type="text" readonly class="form-control  uneditable-input" style="cursor:pointer; background-color: #fff;" id="" placeholder="Dates" name="date" value="{{{ Input::get('date') }}}"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     </div>

                     <div class="form-group input-group date">
                        <label for="" class="sr-only"></label>
                        <input type="text" readonly class="form-control  uneditable-input" style="cursor:pointer; background-color: #fff;" id="" placeholder="Dates" name="date" value="{{{ Input::get('date') }}}"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                     </div>

                     <label for="">Pax</label>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Number of Adults">
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Number of Children">
                     </div>

                  </form>

                  <div class="form-separator"></div>

                  <form>
                     <h3 class="form-title text-center">Your Details</h3>

                     <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Full Name">
                     </div>
                     <div class="form-group">
                        <input type="tel" class="form-control" id="" placeholder="Phone Number">
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control" id="" placeholder="Email Address">
                     </div>
                  </form>
               
               </div>
            </div>
         </div>
      
         <div class="modal-footer">
            <button type="button" class="btn btn-green">Book Now</button>
         </div>

      </div>
   </div>
</div>