@extends('admin.layouts.default')

@section('content')

<a class="btn btn-success pull-right" href="<?php echo url('admin/vendors/create'); ?>"><i class="fa fa-plus"></i> Add New Listing</a>

<h2 class="admin-title">Vendors</h2>

<div class="admin-search">
	<form role="form" id="user-filter">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>

		<div class="admin-filter">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						{{ Form::select('user_id', $owners, Input::get('user_id', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('status', array('' => '-- Status --', '1' => 'Published', '0' => 'Draft'), Input::get('status', ''), array('class' => 'form-control')) }}
					</div>

					{{--
					<div class="col-md-2">
						{{ Form::select('disabled', array('' => '-- Date --', 1 => 'Yes', 0 => 'No'), Input::get('', ''), array('class' => 'form-control')) }}
					</div>
					
					
					<div class="col-md-2">
						<div class="input-group date monthpicker">
							{{ Form::text('month', Input::get('month', ''), array('class' => 'form-control')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
					--}}

					<div class="col-md-2">
						<a href="{{ url('admin/vendors') }}" class="btn btn-default">reset</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<table class="table table-hover table-bordered admin-table">
	<thead>
	<tr>
		<th width="25%" colspan="2">Vendor Name</th>
		<th width="10%" class=" hidden-xs">Owner</th>
		<th width="10%" class=" hidden-xs">Status</th>
		<!-- <th width="5%">Featured</th> -->
		<th width="10%" class=" hidden-xs">State</th> 
		<th width="10%" class=" hidden-xs">Last updated</th> 
		<th width="8%" class="text-center hidden-xs">Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($vendors as $i => $vendor)
	<tr>
		<td width="5%"><img src="{{ asset($vendor->cover_thumb) }}" height="60" />
		<td><a href="{{ url('admin/vendors/' . $vendor->id . '/edit') }}">{{{ $vendor->name }}}</a> <br/> - {{$vendor->genres->first()->name}}</td>
		<td class=" hidden-xs">{{ $vendor->user->firstname }} </td>
		<td class=" hidden-xs">
			@if( $vendor->status)
			<i class="fa fa-check"></i> Published
			@else 
			<i class="fa fa-exclamation"></i> Pending
			@endif
		</td>
		<!-- <td>@if($vendor->featured) <i class="fa fa-star"></i> @else <i class="fa fa-star-o"></i> @endif </td> -->
		<td class=" hidden-xs"> {{$vendor->state}} </td>
		<td class=" hidden-xs"> {{$vendor->updated_at}} </td>
		<td class="text-center hidden-xs">
			<a class="btn btn-default btn-fb btn-edit" title="Edit" alt="Edit" href="{{ url('admin/vendors/' . $vendor->id . '/edit') }}"><i class="fa fa-edit"></i></a>

			@if( User::find(Auth::user()->id)->is('administrator') )
			<a class="btn btn-default btn-warning btn-remove confirmation" title="Delete" alt="Delete" href="{{ url('admin/vendors/' . $vendor->id . '/delete') }}"><i class="fa fa-times"></i></a>
			@endif

		</td>
	</tr>
	@endforeach
	</tbody>
</table>

<?php /*
<div class="row">
	<div class="col-md-12">
		<div class="vendor-content">
			<div id="posts" class="row vendor-items">
				
				@foreach($vendors as $i => $vendor)
				<div class="col-md-4 vendor-item item">
					<div class="thumbnail">
						<img src="<?php echo e(asset('assets/img/43H.jpg')); ?>">
						<div class="box-content">
							<h3><a href="#">{{{ $vendor->name }}}</a></h3>
							<div class="date">$vendor->formatDate('start_date')</div>
							
							<div class="tags-listing">
								<a href="#" class="btn btn-hollo">$tag->name</a>
								<a href="#" class="btn btn-hollo">$tag->name</a>
								<a href="#" class="btn btn-hollo">$tag->name</a>
								<a href="#" class="btn btn-hollo">$tag->name</a>
							</div>

						</div>

						<div class="item-action row">
							<div class="col-md-6">
								<a class="btn btn-default btn-fb btn-block" href="<?php echo url('admin/vendors/' . $vendor->id . '/edit'); ?>"><i class="fa fa-edit"></i> Edit</a>
							</div>
							<div class="col-md-6">
								<a class="btn btn-default btn-warning btn-block confirmation" href="<?php echo url('admin/vendors/' . $vendor->id . '/delete'); ?>"><i class="fa fa-times"></i> Delete</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
</div>
*/?>

{{ $vendors->links() }}

@stop

@section('scripts')

<?php /*
<script>
$(document).ready(function(){
	/* activate jquery isotope 
	$('#posts').imagesLoaded( function(){
    $('#posts').isotope({
      itemSelector : '.item'
    });
  });	
});
</script>
*/?>

@stop