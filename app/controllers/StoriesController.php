<?php

class StoriesController extends BaseController {

	public function getIndex($categorySlug = null)
	{
		$searchTag = Input::get('tag');
		$searchSortBy = Input::get('sortby');
		$showFeatured = true; // should we show the main 3 articles?

		$query = Story::query();
		$query->published();

		if( ! is_null($categorySlug)) {
			$storyCategory = StoryCategory::where('slug', '=', $categorySlug)->first();
			$query->where('category_id', '=', $storyCategory->id);
			
		}

		if( ! empty($searchTag)) {
			$query->searchWithTag($searchTag);
			$showFeatured = false;
		}

		
		$query->orderBy('stories.id', 'desc');
		

		if($showFeatured){
			$featuredQuery =  clone $query;
			$featured = $featuredQuery->take(3)->get();
			$featuredId = $featured->lists('id');


			$stories = $query->whereNotIn('id', $featuredId )->paginate(15);
			
		} else{
			$stories = $query->paginate(15);
			$featured = null;
		}
		

		$categories = StoryCategory::all();

		// hide featured section on pagination
		if( Input::get('page') && Input::get('page') != 1){
			$showFeatured = false;
		}

		return View::make('stories.index', ['featured' => $featured,  'stories' => $stories, 'categories' => $categories, 
			'sortBy' => $searchSortBy, 'showFeatured' => $showFeatured]);
	}

	public function getView($slug)
	{
		$story = Story::where('slug', '=', $slug)->first();

		if(empty( $story ))
			App::abort(404);
		
		$story->hits ++;
		$story->save();
		
		if(Story::recommended( $story )->get()->count() >= 5){
			$recommended = Story::recommended( $story )->get()->random(5);
		} else {
			$recommended = Story::recommended( $story )->get();
		}
		$popular = Story::popular()->get()->random(5);
		$categories = StoryCategory::all();

		return View::make('stories.view', ['categories' => $categories,  'story' => $story, 'festival' => null, 
			'recommended' => $recommended, 'popular' => $popular]);
	}
	
	public function getCreate()
	{
		$festivals = Vendor::all()->lists('name','id');
		$categories = StoryCategory::all()->lists('name','id');

		return View::make('stories.new', ['vendors' => $festivals, 'categories' => $categories]);
	}

	public function postCreate()
	{
		$rules = array(
				'title' => 'required|min:5|max:70',
				'story' => 'required|min:5|max:600',
				'category' => 'required',
				'festival' => 'required'
			);

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$story = new Story();
			$story->title = Input::get('title');
			$story->story = Input::get('story');
			$story->festival_id = Input::get('festival');
			$story->category_id = Input::get('category');
			$story->save();

			return Redirect::to('stories/' . $story->id)
					->with('message', 'Your story has been submitted!');
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

}
