<div class="modal-dialog">
	@if(isset($package))
	{{ Form::model($package, array('url' => 'admin/vendors/' . $festivalId . '/albums/store', 'method' => 'post', 'class' => 'form-lm clearfix')) }}
	@else
	{{ Form::open(array('url' => 'admin/vendors/' . $festivalId . '/albums/store', 'method' => 'post')) }}
	@endif
	
	<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="admin-title">{{ isset($package) ? 'Edit' : 'New' }} Album</h4>
	</div>
	<div class="modal-body">

		@if(isset($package))
		{{ Form::hidden('packageid', $package->id) }}
		@endif

		<div class="form-group {{ Form::errorClass('name') }}">
			{{ Form::label('name', 'Album Name') }}
			{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('name') }}
		</div>

		<div class="form-group {{ Form::errorClass('description') }}">
			{{ Form::label('description', 'Description') }}
			{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control wysiwyg')) }}
			{{ Form::errorMsg('description') }}
		</div>
			
	</div>
	<div class="modal-footer">
		<button class="btn btn-default btn-2" data-dismiss="modal" name="button" type="button">Cancel</button>
		<button class="btn btn-default btn-1" id="save_button" name="button" type="submit">{{ ((isset($package)) ? 'Update' : 'Create') }}</button>
	</div>
	</div><!-- /.modal-content -->
	{{ Form::close() }}
</div><!-- /.modal-dialog -->

<script type="text/javascript">
$(document).ready(function() {
	//$('.modal-dialog .wysiwyg').summernote();
});
</script>