@extends('layouts.default')

@section('content')

<div class="page-title no-title"></div>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="inner-col login-content">

					<h2 class="text-center">{{ $title }}</h2>
					 
					{{ Form::open(array('route' => 'password.request')) }}
					 
					 	<div class="form-group {{ Form::errorClass('email') }}">
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>'E-mail address'))  }}
							{{ Form::errorMsg('email') }}
					 	</div>

					 	<div class="form-group">
						 	{{ Form::submit('Submit', array('class' => 'btn btn-lg btn-success btn-block')) }}
						 </div>

					{{ Form::close() }}

				</div>
			</div>
		</div>
	</div>
</section>

@stop