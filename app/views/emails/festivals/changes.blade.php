<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi Admin,<br /><br />
			Changes has been made to this festival. <a href="{{ URL::to('admin/vendors/' . $festival->id . '/edit#tab-revisions') }}">click here</a> to view them.
		</div>
	</body>
</html>