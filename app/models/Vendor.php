<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Venturecraft\Revisionable\RevisionableTrait;
use Carbon\Carbon;

class Vendor extends \BaseModel implements SluggableInterface {

	use SluggableTrait, RevisionableTrait;
	
	protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug'];
	protected $fillable = ['name', 'description', 'latitude', 'longitude', 'user_id', 'street', 
		'location', 'district', 'state', 'country_id', 'announcement_date', 'directornote', 'status', 
		'ticketprice_fga', 'ticketprice_cust', 'ticketprice_retail', 'ticketsale_date', 'othernote', 'curatornote', 
		'website', 'facebook', 'instagram', 'coverage', 'work_hours', 'twitter', 'googleplus', 'excerpt', 'contact_info', 'work_hours', 
		'active', 'featured', 'allow_ticketsale', 'traveltips', 'contact_info', 'postcode', 'email', 'phone','address', 'subscription', 'subscription_expiry'];

	public function genres()
	{
		return $this->belongsToMany('Genre', 'vendor_genre', 'vendor_id', 'genre_id');    
	}

    public function pricings()
    {
        return $this->belongsToMany('Pricing', 'vendor_pricing', 'vendor_id', 'pricing_id');    
    }

    public function country()
    {
        return $this->hasOne('Country', 'id', 'country_id');    
    }

    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id');    
    }

    public function photos()
    {
        return $this->morphMany('Photo', 'imageable');
    }

    public function packages()
    {
    	return $this->hasMany('Package', 'festival_id');
    }

    public function albums()
    {
        return $this->hasMany('Album', 'vendor_id')->orderBy('updated_at', 'desc');
    }

    /**
     * Search by name
     */
	public function scopeSearchName($query, $name)
    {
        return $query->where('name', 'LIKE', '%'.$name.'%')->where('active', '=', '1');
    }

    /**
     * Search by name
     */
    public function scopeSearchNameAndTag($query, $name)
    {


        // toSql()
        $tagObj = Tag::where('name', '=', $name)->first();
        if(empty($tagObj))
            return $this->scopeSearchName($query, $name);

        // DB::table('users')
        //              ->select(DB::raw('count(*) as user_count, status'))
        //              ->where('status', '<>', 1)
        //              ->groupBy('status')

        return $query
            ->join('taggables', 'taggables.taggable_id', '=', 'vendors.id')
            ->where('taggables.taggable_type', '=', 'Vendor')
            ->where('taggables.tag_id', '=', $tagObj->id)
            ->orWhere('vendors.name', 'LIKE', '%'.$name.'%')
            ->where('vendors.active', '=', '1')
            ->groupBy('vendors.id')
            ->orderBy(DB::raw('count(*)'), 'desc');
        // return $query->where('name', 'LIKE', '%'.$name.'%')->where('active', '=', '1');
    }

    /**
     * Search by location
     */
    public function scopeSearchLocation($query, $name)
    {
        return $query->where('location', 'LIKE', '%'.$name.'%')->where('active', '=', '1');
    }

    public function scopePublished($query){
        return $query->where('status', '=', 1);
    }

    public function scopeRandom($query)
    {
        return $query->orderBy(DB::raw('RAND()'));
    }

    public function scopeDraft($query){
        return $query->where('status', '=', 0);
    }

    public function scopeOtherThan($query, $id){
        return $query->where('vendors.id', '!=', $id);
    }

    public function scopeDistinct($query){
        return $query->groupby('country_id');
    }

    /**
     * Search by country
     */
    public function scopeFindCountry($query, $id)
    {
        
        $country = Country::find($id);
        return $query->where('country_id', '=', $country->id)->where('active', '=', '1');
    }

    /**
     * Search by country
     */
    public function scopeSearchCountry($query, $slug)
    {
        
        $country = Country::where('slug', '=', $slug)->first();
        if($country)
            return $query->where('country_id', '=', $country->id)->where('active', '=', '1');
        else
            return $query->where('country_id', '=', '-1')->where('active', '=', '1');
    }

    /**
     * Search by country
     */
    public function scopeSearchState($query, $state)
    {

        return $query->where('state', '=', $state)->where('active', '=', '1');
    }


    public function scopeFindGenre($query, $id)
    {
        $genre = Genre::where('id', '=', $id)->first();
        if(empty($genre)){
            return $query->where('id', '=', '-1');// dummy to not return any result
        }
        return $query
            ->join('vendor_genre', 'vendor_genre.vendor_id', '=', 'vendors.id')
            ->where('vendor_genre.genre_id', '=', $genre->id) // We could use OR for multiple genre search
            ->groupBy('vendors.id');
    }

    /**
     * Return festival with the genre
     */
    public function scopeSearchGenre($query, $slug)
    {
    	$genre = Genre::where('slug', '=', $slug)->first();
        if(empty($genre)){
            return $query->where('id', '=', '-1');// dummy to not return any result
        }
    	return $query
    	    ->join('vendor_genre', 'vendor_genre.vendor_id', '=', 'vendors.id')
    	    ->where('vendor_genre.genre_id', '=', $genre->id) // We could use OR for multiple genre search
    	    ->groupBy('vendors.id');
    }

    /**
     * Return festivals only with the given tag
     */
    public function scopeSearchWithTag($query, $tag)
    {
        $tagObj = Tag::where('name', '=', $tag)->first();
        if($tagObj){
        return $query
            ->join('taggables', 'taggables.taggable_id', '=', 'vendors.id')
            ->where('taggables.taggable_type', '=', 'Vendor')
            ->where('taggables.tag_id', '=', $tagObj->id)
            ->groupBy('vendors.id')
            ->orderBy(DB::raw('count(*)'), 'desc');
        } else {
            return $query->where('name', '=', 'YOU SHOULD NOT FOUND THIS AT ALL');
        }
    }

    /**
     * Featured
     */
    public function scopeFeatured($query)
    {
        return $query->where('featured', '=', '1')->where('active', '=', '1');
    }

    public function scopePremium($query)
    {
        return $query->where('subscription', '=', '2');
    }

    public function scopeVerified($query)
    {
        return $query->where('subscription', '=', '1');
    }

    public function tags()
    {
        return $this->morphToMany('Tag', 'taggable');
    }

	public function revisionHistory()
	{
		return $this->morphMany('\Venturecraft\Revisionable\Revision', 'revisionable');
	}


    public function getFbOgImage(){
        $path = $this->cover_img;
        $path = str_replace( '.', '_fb.', $path);
        if( file_exists( public_path() . $path)) {
            return $path;
        } else {
            return $this->cover_img;
        }
    }
}