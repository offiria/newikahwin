@extends('admin.layouts.default')

@section('content')

<!-- <a class="btn btn-yellow pull-right" href="<?php echo url('admin/messages/create'); ?>"><i class="fa fa-plus"></i> Add message</a>
 --><h2 class="admin-title">Messages</h2>

<!-- <div class="admin-search">
	<form role="form" id="user-filter">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>

	</form>
</div> -->

<table class="table table-hover table-bordered admin-table">
	<thead>
	<tr>

		<th>Date</th>
		<th>Name</th>
		<th>Vendor</th>
		<th>Message</th>
		<th>Mobile</th>
	</tr>
	</thead>
	<tbody>
	@foreach($messages as $i => $message)
	<tr>
		<td>{{ $message->formatDate('created_at') }}</td>
		<td>{{{ $message->name }}}</td>
		<td><a target="_blank" href="{{ url('admin/vendors/' . $message->vendor->id. '/edit') }}">{{{ $message->vendor->name }}}</a></td>
		<td>{{{ $message->message  }}}</td>
		<td>{{{ $message->mobile }}}</td>
	</tr>
	@endforeach
	</tbody>
</table>


@stop