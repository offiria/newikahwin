@extends('layouts.default')

@section('content')

<div class="page-title no-title"></div>


<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="inner-col submit-content">
			<h2 class="text-center">
				Submit Story
			</h2>

				<p>
					We delight in great stories. Just email your festival experience to <b>festivals@festivalguideasia.com</b>.
					A few things to keep in mind:
				</p>
				<ul>
					<li>We will only publish stories related to a festival in Asia. If it is in Asia and isn’t listed, we will do our best to enlist them.</li>
					<li>Entertain us! Make sure you have a captivating start, add a little humour and make sure you have pictures or visuals that you have rights to use.</li>
					<li>You may submit an article, a video, a photo essay, a comic… Whatever helps you tell your story.</li>
					<li>We’ll be editing your story for grammar, style and length.</li>
					<li>
						We’ve created the following editorial themes to give you some inspiration and structure. Here they are: 
						<ol>
							<li><b>Blow My Mind:</b> Tell us about something or a few things that blew your mind, maybe even changed your world view.</li> 
							<li><b>Follow The White Rabbit:</b> Document a surprising journey. You never know what orwho is on the road ahead. But once you’re home from your trip, you realize it was one of those adventures you’ll think of fondly for years to come.</li>
							<li><b>Fashion Police:</b> Festivals and new cities/countries are full of great and funny fashion.Be the fashion police, tell us what you love and what not to wear.</li>
							<li><b>The Recipe:</b> Festivals are a careful mix of several ingredients. Festival curators bring together specific types of entertainment, shopping, food and drinks at a specific location to create the festival experience.</li>
						</ol>
					</li>
				</ul>

			<a href="mailto:festivals@festivalguideasia.com" class="btn btn-lg btn-block btn-hollo">Submit to: festivals@festivalguideasia.com</a>
		
		</div>
	</div>
</div>


@stop