<?php

/*
|--------------------------------------------------------------------------
| Macros
|--------------------------------------------------------------------------
*/

Form::macro('errorMsg', function($field) { //yay! we don't have to pass $errors anymore
    $errors = Session::get('errors');

    if($errors && $errors->has($field)) { //make sure $errors is not null
        $msg = $errors->first($field);
        return '<p class="help-block text-danger">'.$msg.'</p>';
    }
    return '';
});

Form::macro('errorClass', function($field) { //yay! we don't have to pass $errors anymore
    $errors = Session::get('errors');

    if($errors && $errors->has($field)) { //make sure $errors is not null
        $msg = $errors->first($field);
        return 'has-error';
    }
    return ;
});


Form::macro('bootstrapInput', function($name, $label, $help ='', $attr = ['class' => 'form-control input-lg'] ) {
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
        <?php echo  Form::text( $name, Input::old( $name ), $attr)  ?>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});

Form::macro('bootstrapInput4', function($name, $label, $help ='', $attr = ['class' => 'form-control input-lg'] ) {
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( $name, Input::old( $name ), $attr)  ?>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});


Form::macro('bootstrapTextarea', function($name, $label, $help ='') {
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
        <?php echo  Form::textarea( $name, Input::old( $name ), array('class' => 'form-control input-lg', 'rows' => '4'))  ?>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});

Form::macro('bootstrapRadio', function($name, $label, $options, $help ='') {
    // echo Input::get( $name ). $name .'pppp'; exit;
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
        <?php foreach ($options as $value) {
            ?>
            
            <div class="radio">
              <label>
                <input type="radio" name="<?php echo $name; ?>" id="" value="<?php echo $value; ?>" class="selectChange" <?php echo (Input::get( $name ) == $value) ? 'checked' : '' ; ?>>
                <?php echo $value; ?>
              </label>
            </div>

            <?php
        }
        ?>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});

Form::macro('bootstrapCheckbox', function($name, $label, $options, $help ='') {
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
        <?php foreach ($options as $value) {
            ?>
            
            <div class="checkbox">
              <label>
                <input type="checkbox" name="<?php echo $name; ?>[]" value="<?php echo $value; ?>" class="selectChange"  <?php echo (Input::get( $name ) == $value)? 'checked': '' ; ?>/ >
                <?php echo $value; ?>
              </label>
            </div>

            <?php
        }
        ?>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});


Form::macro('bootstrapDropdown', function($name, $label, $options, $help ='') {
    ob_start();
    ?>
    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
            <select class="form-control selectChange" name="<?php echo $name; ?>">
        <?php foreach ($options as $value) {
            ?>
            
                <option class="" value="<?php echo $value; ?>"  <?php echo (Input::get( $name ) == $value)? 'selected="selected"': '' ; ?>  ><?php echo $value; ?></option>

            <?php
        }
        ?>
         </select>
         <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});


Form::macro('bootstrapQuantity', function($name, $label, $min=1, $max =0, $steps = 10, $help ='') {
    ob_start();
    ?>
    

    <div class="form-group <?php echo Form::errorClass( $name ) ?>">
        <?php echo Form::label($name, $label, ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
            <div class="input-group number-spinner">
            <span class="input-group-btn">
                <button class="btn btn-default input-lg" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
            </span>
            <?php echo  Form::text( $name, Input::old( $name ), array('class' => 'form-control input-lg text-center'))  ?>
            <span class="input-group-btn">
                <button class="btn btn-default input-lg" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
            </span>
        </div>
        <p class="help-block">Minimum order: <?php echo $min; ?></p>
        <p class="help-block"><?php echo $help; ?></p>
        <?php echo  Form::errorMsg( $name )  ?>
        </div>
    </div>

    <script>
    $(document).on('click', '.number-spinner button', function (event) {    
        var btn = $(this),
            oldValue = btn.closest('.number-spinner').find('input').val().trim(),
            newVal = 0;
        
        if (btn.attr('data-dir') == 'up') {
            newVal = parseInt(oldValue) + <?php echo $steps; ?>;
        } else {
            if (oldValue > 1) {
                newVal = parseInt(oldValue) - <?php echo $steps; ?>;
            } else {
                newVal = <?php echo $min; ?>;
            }
        }
        btn.closest('.number-spinner').find('input').val(newVal);
        event.preventDefault();
    });
    </script>
    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});


Form::macro('totalAmount', function($base, $items ) {
    ob_start(); 
    ?>
    <div class="form-group ">
        <?php echo Form::label('Total', "Total", ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-8">
            <span class="total" id="total-price">RM 0.00</span>
        </div>
        <input type="hidden" name="total"  />
    </div>

    <script>
        // some global vars
        var basePrice = parseFloat('<?php echo $base; ?>');
        var totalPrice = parseFloat('<?php echo $base; ?>');

        Number.prototype.formatMoney = function(c, d, t){
        var n = this, 
            c = isNaN(c = Math.abs(c)) ? 2 : c, 
            d = d == undefined ? "." : d, 
            t = t == undefined ? "," : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
           return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
         };

        function recalculatePrice(){
            var price = parseFloat(basePrice);
               

                $( ".selectChange" ).each(function( index ) {
                    if($(this).prop('tagName') == 'INPUT'){
                        if( $(this).attr('type') == 'radio' || $(this).attr('type') == 'checkbox')
                        {
                            if($(this).is(':checked')){
                                var regexp = /MYR ([\d,]+(?:\.\d{1,2})?)/g;
                                var optionPrice = $(this).val();
                                var match = regexp.exec(optionPrice);
                                if(match){
                                    match[1] = match[1].replace(',', '');
                                    price = price + parseFloat(match[1]);
                                }
                            }
                        }
                    }

                    if($(this).prop('tagName') == 'SELECT'){
                        var regexp = /MYR ([\d,]+(?:\.\d{1,2})?)/g;
                        var optionPrice = $(this).find(":selected").val();
                        var match = regexp.exec(optionPrice);
                        if(match){
                            match[1] = match[1].replace(',', '');
                            price = price + parseFloat(match[1]);
                        }
                    }
                });

                $('#total-price').html('RM ' + parseFloat(price).formatMoney(2));
                $('input[name="total"]').val( parseFloat(price).toFixed(2) );
        }
        $(document).ready(function(){
            recalculatePrice();
            $(".selectChange").change(function(){
                // recalculate EVERYTHING
                recalculatePrice();
            });
        });
  </script>

    <?php
    $out = ob_get_contents();
    ob_end_clean();

    return $out;
});