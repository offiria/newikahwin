<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('package_id')->nullable();
			$table->date('departure_date')->nullable();
			$table->date('return_date')->nullable();
			$table->integer('adults')->nullable();
			$table->integer('children')->nullable();
			$table->string('fullname')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('package_orders');
	}

}
