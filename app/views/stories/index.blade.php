@extends('layouts.default')
@section('title', 'Blog')

@section('content')

<div class="listing-header blog-categories">
	<div class="container">
		<h1 class="listing-title" style="display: none;">Stories</h1>

		<div class="category-bar clearfix">
			<ul class="list-inline center-block">
				{{--
				<li>Categories</li>
				--}}
				@foreach($categories as $category)
					<li><a href="{{ URL::to('blog/category/' . $category->slug) }}">{{ trans('vendor.blog-'.$category->slug) }}</a></li>
				@endforeach
			</ul>
			<!-- <div class="listing-sorter pull-right">
				<div class="dropdown listing-sortby">
					Sort by
					<button class="btn btn-sort dropdown-toggle" type="button" data-toggle="dropdown">
						{{ ( ! empty($sortBy) ? ucfirst($sortBy) : 'Date') }}
						<i class="fa fa-chevron-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<li><a href="" data-sortby="date">Date</a></li>
						<li><a href="" data-sortby="festivals">Festivals</a></li>
					</ul>
				</div>
			</div> -->
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<div class="stories-content">

			{{-- Featured Styling--}}
			@if( $featured && $showFeatured)
			<div class="row stories-items featured">
				<?php $i = 0; ?>
				@foreach ($featured as $story)

				@if($i == 0)

					<div class="col-md-12 stories-item">
						<div class="thumbnail">
							
							<img src="{{ asset( $story->cover_img ) }}">
							<div class="box-content">
								<h3><a href="{{ URL::to('blog/' . $story->slug) }}">{{{ $story->title }}}</a></h3>
								<div class="places">{{ trans('vendor.blog-by')}} Owner </div>
							</div>
							<div style="padding:8px 0px">
								{{ $story->excerpt }}
							</div>
							<a href="{{ URL::to('blog/' . $story->slug) }}"></a>

						</div>
					</div>

				@else				

					<div class="col-md-6 stories-item">
						<div class="thumbnail">
							
							<img src="{{ asset( $story->cover_thumb ) }}">
							<div class="box-content">
								<h3><a href="{{ URL::to('blog/' . $story->slug) }}">{{{ $story->title }}}</a></h3>
								<div class="places">{{ trans('vendor.blog-by')}} {{{ $story->author->firstname }}} {{{ $story->author->lastname }}} </div>
							</div>
							<div style="padding:8px 0px">
								{{ $story->excerpt }}
							</div>
							<a href="{{ URL::to('blog/' . $story->slug) }}"></a>

						</div>
					</div>
				@endif
				<?php  $i++; ?>
				@endforeach
				<hr/>
			</div>
			@endif
		</div><!-- end col -->
	</div><!-- end row -->
</div><!-- end container-->

<!-- Normal listing -->
<div class="container">
	<hr/>
	<div class="row">
		<div class="col-md-8">
			<?php $i=0; ?>
				@foreach($stories as $story)

					@if($i % Config::get('app.stories_col_size') == 0)
					<div class="row stories-items" style="border-bottom:1px solid #DDD; padding-bottom:20px">
					@endif
					<?php $i++; ?>
						
						<div class="col-md-6 stories-item">
							<div class="thumbnail">
								
								<img src="{{ asset( $story->cover_thumb ) }}" >
								

							</div><!-- end thumbnail -->
						</div>

						<div class="col-md-6 stories-item">
							<div class="thumbnail">
							<div class="box-content">
								<h3><a href="{{ URL::to('blog/' . $story->slug) }}">{{ $story->title }}</a></h3>
								<div class="places">{{ trans('vendor.blog-by')}} {{{ $story->author->firstname }}} {{{ $story->author->lastname }}}</div>
									{{-- <div class="date">{{ $story->formatDate('created_at') }}</div> --}}
									

									{{--
									<div class="tags-listing">
									@if($story->tags)
										@foreach($story->tags as $tag)
											<a href="{{ URL::to('stories?tag='. $tag->name) }}" class="btn btn-hollo">{{{ $tag->name }}}</a>
										@endforeach
									@endif
									</div>
									--}}
								</div>
								<div style="padding:8px 0px; display: none;">
									{{ Str::limit($story->excerpt, 158) }}
								</div>
								<a href="{{ URL::to('blog/' . $story->slug) }}">
								</a>
								<p class="" style="font-size:16px;line-height:18px;">
									{{ Str::limit($story->excerpt, 128) }}
								</p>
								
								
							</div><!-- end thumbnail -->
						</div>

					@if( ($i) % Config::get('app.stories_col_size') == 0 || $i == count($stories))
					</div>
					@endif

				@endforeach
			</div><!-- end col-->

			<div class="col-md-4" style="border-left: 1px solid #eee;">
				@include('stories.sidebar')
			</div>
		</div>
	</div>
	<div class="text-center">
	{{ $stories->links() }}
	</div>
</div>
@stop