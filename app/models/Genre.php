<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Genre extends \Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug'];
	protected $fillable = [];
	protected $attributes = [ 'name' => '' ];

	public function vendors()
	{
		return $this->belongsToMany('Vendor', 'vendor_genre');
	}

	public function scopeFindSlug($query, $slug){
		return $query->where('slug', '=', $slug);
	}

	public function scopeHasFestival($query){
		$genres_ids = DB::table('vendor_genre')->distinct('genre_id')->lists('genre_id');
		return $query->whereIn('id', $genres_ids);
	}

	public function childs(){
		return $this->hasMany('Genre', 'parent_id', 'id');
	}
	
	public static function currentName(){
		$slug = Input::get('category');
		$genre = Genre::findSlug($slug)->first();
		
		if(empty($genre)){
			return '';
		} else {
			return $genre->name;
		}
	}
}