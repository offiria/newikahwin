<?php

class PackageOrder extends \Eloquent {
	public function package() {
		return $this->belongsTo('Package', 'package_id', 'id');
	}

	public function festival() {
		return $this->belongsTo('Festival', 'festival_id', 'id');
	}
}