@extends('admin.layouts.default')

@section('content')

<h2 class="admin-title">Edit Country</h2>

<div class="admin-search">
	<form role="form" id="user-filter">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>
	</form>
</div>

<ul class="admin-row row list-unstyled country-list no-margin">
@foreach($countries as $country)
	<li class="col-md-3"><a href="#editCountry" data-id="{{ $country->id }}" data-title="{{$country->name}}" data-description="{{{ $country->description }}}" data-cover-img="{{ $country->cover_img }}">{{$country->name}}</a></li>
@endforeach
</ul>

<div class="modal fade" id="edit-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title">Modal title</h3>
			</div>
			<div class="modal-body">
				<form action="" method="POST" role="form" name="editCountryForm">
				
					<div class="form-group {{ Form::errorClass('state') }}">
					{{ Form::label('cover_img', 'Cover image') }}
					{{ Form::text('cover_img', Input::old('cover_img'), array('class' => 'form-control')) }}
					{{ Form::errorMsg('cover_img') }}
					</div>

					<div class="form-group {{ Form::errorClass('description') }}">
					{{ Form::label('description', 'Description') }}
					{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control nowysiwyg')) }}
					{{ Form::errorMsg('description') }}
					</div>

					<div class="form-group {{ Form::errorClass('traveltips') }}">
					{{ Form::label('traveltips', 'Travel Tips') }}
					{{ Form::textarea('traveltips', Input::old('traveltips'), array('class' => 'form-control nowysiwyg')) }}
					{{ Form::errorMsg('traveltips') }}
					</div>

				<input type="hidden" name="id" value="0"/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a href="#saveCountry" class="btn btn-primary">Save changes</a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')

<script>
$(document).ready(function(){
	$('a[href="#editCountry"]').click(function(){
		$('#edit-modal').modal();

		$('#edit-modal .modal-title').html( $(this).data('title') );
		$('#edit-modal [name="id"]').val( $(this).data('id') );
		$('#edit-modal [name="cover_img"]').val( $(this).data('cover-img') );
		$('#edit-modal [name="description"]').val( $(this).data('description') );
	});

	$('a[href="#saveCountry"]').click(function(){
		$.ajax({ 
	      url: '{{ URL::to("admin/countries") }}', 
	      data: $('form[name="editCountryForm"]').serialize() ,
	      type: 'POST'
	    })
	    .done(function(data) {
	      $('a[data-id="'+ data.id +'"]').data('cover-img', data.cover_img );
	      $('a[data-id="'+ data.id +'"]').data('description', data.description );
	      $('#edit-modal').modal('hide')
	    });
	});
});
</script>

@stop