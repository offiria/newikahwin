@extends('layouts.default')
@section('title', $vendor->name)
@section('meta')
<meta property="og:title" content="{{{ $vendor->name }}}" />
<meta property="og:type" content="website" />
<meta property="og:image" content="{{ asset( $vendor->getFbOgImage() ) }}" />
<meta property="og:url" content="{{ Request::fullUrl() }}" />
<meta property="og:site_name" content="iKahwin" />
<!-- <meta property="og:description" content="{{{ $vendor->excerpt }}}" /> -->
<meta property="fb:app_id" content="1547213035507514" />
@stop
@section('content')

<div class="container">
	<div class="vendor">
		<div class="row">
			<div class="col-md-12 item-content">

				<div class="vendor-cover">
				
					<h1 class="main-title">{{{ $vendor->name }}}</h1>

					<div id="carousel" class="festival-carousel carousel slide" data-ride="carousel">

						<!-- Wrapper for slides -->
						<div class="">
							<div class="item active">
								<img src="{{ asset( $vendor->cover_img ) }}" />
								@if($vendor->subscription == 2)
									<img src="{{ asset( 'assets/img/premium.png') }}" class="vendor-tag" style=""/>
								@elseif($vendor->subscription == 1)
									<img src="{{ asset( 'assets/img/verified.png') }}" class="vendor-tag" style=""/>
								@else
									<img src="{{ asset( 'assets/img/unverified.png') }}" class="vendor-tag" style=""/>
								@endif
								
							</div>
							
							
						</div>
					</div>
				</div>

				<div class="vendor-profile">
					<div class="row">
						<div class="col-md-2 text-center">
							<img class="vendor-avatar" src="{{ isset($vendor->user->image) ? asset($vendor->user->image) : asset('assets/img/default_avatar.jpg') }}"/>
						</div>
						<div class="col-md-8">
							<h3 class="vendor-name">{{ $vendor->user->firstname }} {{ $vendor->user->lastname }}</h3>
							<ul class="list-inline vendor-meta">
								<li><i class="fa fa-map-marker"></i> {{{ $vendor->state }}}, {{{ $vendor->country->name }}}</li>
								<li><i class="fa fa-truck"></i> {{ $vendor->genres->first()->name }}</li><br/>
								@if( !empty($vendor->website) )
								<li><a href="{{ HttpHelper::url($vendor->website) }}"><i class="fa fa-globe"></i> {{ HttpHelper::url($vendor->website) }}</a></li>
								@endif
								@if( !empty($vendor->facebook) )
								<li><a href="{{ HttpHelper::url($vendor->facebook) }}"><i class="fa fa-facebook-square"></i> {{$vendor->facebook}}</a></li>@endif
								@if( !empty($vendor->instagram) )
								<li><a href="{{ HttpHelper::url($vendor->instagram) }}"><i class="fa fa-instagram"></i> {{$vendor->instagram}}</a></li>@endif
							</ul>
						</div>
						<div class="col-md-4 ">
							
							<ul class="list-inline pull-right hide">
								<li>
									<div class="banner-badge">
										<i class="fa fa-heart"></i>
									</div>
								</li>
								<li>
									<div class="banner-badge">
										<i class="fa fa-star"></i>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="widget-vendor-info">
					<div class="row">
					<div class="col-md-3">
						<span>Alamat</span>
						{{{ $vendor->street }}}, {{{ $vendor->district }}} <br>
						{{{ $vendor->state }}}, {{ $vendor->postcode }}
						{{{ $vendor->country->name }}}
					</div>
					<div class="col-md-3">
						<span>{{ trans('admin.coverage') }}</span>
						
						{{{ $vendor->coverage }}}
					</div>

					<div class="col-md-3">
						<span>{{ trans('admin.work-hours') }}</span>
						{{{ $vendor->formatDate('start_date') }}}
						@if( $vendor->formatDate('start_date') != $vendor->formatDate('end_date'))
							- {{{ $vendor->formatDate('end_date') }}}
						@endif
						{{{ $vendor->work_hours }}}
					</div>

					<div class="col-md-3">
						<span>{{ trans('admin.contact-info') }}</span>
						
						{{{ $vendor->contact_info }}}
					</div>
					</div>
				</div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-tabs-line" id="vendor-nav" role="tablist">
				 	<li class="active"><a href="#about" role="tab" data-toggle="tab">{{ trans('admin.album-gallery') }}</a></li>
				 	<li><a href="#packages" role="tab" data-toggle="tab">{{ trans('admin.packages') }}</a></li>	
				 	<li><a href="#traveltips" role="tab" data-toggle="tab">{{ trans('admin.deals') }}</a></li>	

				 	<li class="pull-right">
				 		<button class="btn btn-success" data-toggle="modal" data-target="#contactModal"><i class="fa fa-envelope"></i> {{ trans('vendor.contact_vendor') }}</button>
				 		<button class="btn btn-default @if($liked) btn-like @else btn-unlike  @endif" id="like" data-ajax="{{ $liked ? url('likes/unlike/Vendor/'.$vendor->id) : url('likes/like/Vendor/'.$vendor->id) }}" style="margin-left: 10px;"><i class="fa fa-heart"></i></button>
				 	</li>

				</ul>

				<!-- Tab panes -->
				<div class="col-md-8">
					<div class="tab-content">
						<div class="tab-pane active" id="about">
						
							<div class="row">
								<div class="col-md-12">
									<div class="inner-content inner-tab-content">
										<div class="row">
											<div class="col-md-12">
												
												

												@foreach ($vendor->albums as $album)
												<div class="album-container">
													<h4>{{ $album->name }}</h4>
													<ul class="list-unstyled gallery row">
													@foreach ($album->photos as $photo)
														<li class="col-sm-4 col-xs-6 ">
															<a title="{{{ $photo->caption }}}" href="{{ asset($photo->path) }}"><div class="thumb-container"><img class="thumb-image" src="{{ asset($photo->path) }}" style="width:100%" title="{{{ $photo->caption }}}" /></div></a><span class="small">{{{ $photo->caption }}}</span></li>
													@endforeach	
													</ul>
												</div>
												@endforeach
											</div>
										</div>
									</div>

								</div>
							</div>
						</div><!-- end tab pane -->

						<div class="tab-pane" id="packages">
							
							<div class="row">
								<div class="col-md-12">
									{{ $vendor->description }}
								</div>
							</div>

						</div><!-- end tab pane -->

						<div class="tab-pane" id="traveltips">
							
							@if( ! empty($vendor->traveltips))
							<div class="row">
								<div class="col-md-12">
									{{ $vendor->traveltips }}
								</div>
							</div>
							@endif

							@if( ! empty($vendor->country->traveltips))
							<div class="row">
								<div class="col-md-12">
									<h3>Country travel tips</h3>
									{{ $vendor->country->traveltips }}
								</div>
							</div>
							@endif

						</div><!-- end tab pane -->

					</div><!-- end tab content -->
				</div>

				<div class="col-md-4">
					<div class="side-tab-content">


						<div class="inner-left-excerpt">
							<!-- {{{ $vendor->excerpt }}} -->
							
						</div>

						<div class="inner-tags-content">
							<h4><i class="fa fa-tags"></i> Tags</h4>
							<div class="inner-tags-listing clearfix">
								@if($vendor->tags)
									@foreach($vendor->tags as $tag)
										<a href="{{ URL::to('vendors/tag/'. trim(urlencode($tag->name))) }}" class="btn btn-default">{{ $tag->name }}</a>
									@endforeach								
								@endif
							</div>
						</div>

						<div class="inner-tags-listing clearfix">
							<div class="fb-like" data-href="{{ Request::fullUrl() }}" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>

						</div>
				

						<div class="row">
							<div class="col-xs-12">
								<a class="btn btn-default btn-block"  data-toggle="modal" data-target="#reportModal"><i class="fa fa-exclamation-triangle"></i> Report this vendor</a>
							</div>
						</div>
						
						
					</div>
				</div>

				

			</div>

		</div>
	</div>


	{{--
	<div class="other-festival">
		<div class="row">
			<div class="col-md-12">
				Other related festivals
				<h3>Other Festivals You Might Like</h3>
				<ul>
				@foreach ($otherVendors as $fest ) 
					<li>{{ $fest->name }}</li>
				@endforeach
				</ul>
			</div>
		</div>
	</div>
	--}}

	@if( !empty($otherVendors))
	<div class="other-vendors no-border">
		<div class="row">
			<div class="col-md-12">
				<h2 class="slider-title text-center">{{ trans('vendor.other-vendors') }}</h2>
					<div class="item active">
							@foreach ($otherVendors as $vnd ) 
								@include('vendors.itemindex', array('vendor'=> $vnd, 'col_size' => Config::get('app.vendors_col_size') ) )
							@endforeach
					</div>
			</div>
		</div>
	</div>
	@endif

</div>

<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Contact this vendor</h4>
      </div>
      <form role="form" method="POST" action="{{ Request::url() }}/sms">
      <div class="modal-body">
        
        	<div class="row">
        		<div class="col-md-6">
				  <div class="form-group">
				    <label for="name">Your name</label>
				    <input type="text" class="form-control" id="name" name="name" placeholder="Your name">
				  </div>
				</div>

				<div class="col-md-6">
						  <div class="form-group">
						    <label for="mobile">Mobile number</label>
						    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="+6016-1111111">
						    <p class="help-block">Specify your full mobile number so the vendor can get back to you</p>
						  </div>
						</div>
			</div>
						  <div class="form-group">
						    <label for="message">Message</label>
						    <textarea class="form-control" name="message"></textarea>
						    <p class="help-block">What do you want to ask the vendor</p>
						  </div>

						  {{ Form::captcha() }}
						  <input type="hidden" name="sms" value="send"/>
						  
						
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default btn-primary">Submit </button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Report Modal -->
<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Report this vendor</h4>
      </div>
      <form role="form" method="POST" action="{{ Request::url() }}/report">
      <div class="modal-body">
        
        	<div class="form-group">
        		<label for="reason">Reason for reporting this vendor</label>
				<select class="form-control"  name="reason">
				<option value="Vendor did not deliver what they promise">Vendor did not deliver what they promise</option>
				<option value="Vendor is out of business">Vendor is out of business</option>
				<option value="Vendor does not response to my enquiry">Vendor does not response to my enquiry</option>
				<option value="Other reason">Other reason...</option>
				</select>
			</div>

			<div class="form-group">
			<label for="message">Message</label>
			<textarea class="form-control" name="message"></textarea>
			<p class="help-block">Let us know why you're reporting this vendor.</p>
			</div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default btn-primary">Submit </button>
      </div>
      </form>
    </div>
  </div>
</div>

<style>
.affix-top{
	position: relative;
}

.affix {
	top: 0px;
	z-index: 100;
	background: #FFF;
}

</style>
<script>
function affixWidth() {
    // ensure the affix element maintains it width
    var affix = $('#vendor-nav');
    var width = affix.width();
    affix.width(width);
}

$(document).ready(function() {

	affixWidth();

	$('#vendor-nav').affix({
	    offset: {
	      top: $('#vendor-nav').offset().top
	    }
	  });
});
</script>
@stop