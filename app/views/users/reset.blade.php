@extends('layouts.default')

@section('content')

<div class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h1 class="title">
					{{ $title }}
				</h1>
			</div>
		</div>
	</div>
</div>

<section id="content">
	<div class="container">
		<div class="inner-col">

		@if (Session::has('error'))
		  {{ trans(Session::get('reason')) }}
		@endif
		 
		{{ Form::open(array('route' => array('password.update', $token))) }}
		 
			<p>{{ Form::label('email', 'Email') }}
			{{ Form::text('email') }}</p>

			<p>{{ Form::label('password', 'Password') }}
			{{ Form::text('password') }}</p>

			<p>{{ Form::label('password_confirmation', 'Password confirm') }}
			{{ Form::text('password_confirmation') }}</p>

			{{ Form::hidden('token', $token) }}

			<p>{{ Form::submit('Submit') }}</p>
		 
		{{ Form::close() }}

		</div>
	</div>
</section>

@stop