<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi Admin,<br /><br />
			A new order has been made. <a href="{{ URL::to('admin/orders/' . $packageOrder->id) }}">click here</a> to view it.
		</div>
	</body>
</html>