@extends('layouts.default')

@section('content')

<div class="container">






					
			
			
		
								



<h2>Sumber Kewangan</h2>  
{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal clearfix', 'id' => 'orderForm')) }}

  <div class="form-group <?php echo Form::errorClass( 'in1' ) ?>">
        {{ Form::label('in1', 'Mas Kahwin', ['class' => 'col-md-4 control-label']); }}
        <div class="col-md-4">
        {{ Form::text( 'in1', Input::old( 'in1' ), array('class' => ' calc form-control input-lg', 
          'placeholder'=>"RM 80.00", 'title'=>"Mas Kahwin Ikut Negeri"))  }}
        </div>
  </div>


  <div class="form-group <?php echo Form::errorClass( 'in2' ) ?>">
        <?php echo Form::label('in2', 'Wang Hantaran', ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( 'in2', Input::old( 'in2' ), array('class' => ' calc form-control input-lg', 
          'placeholder'=>"RM 80.00", 'title'=>"Wang yang diberikan oleh pihak lelaki"))  ?>
        </div>
  </div>


  <div class="form-group <?php echo Form::errorClass( 'in3' ) ?>">
        <?php echo Form::label('in3', 'Sumber Lain - Lain', ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( 'in3', Input::old( 'in3' ), array('class' => ' calc form-control input-lg', 
          'placeholder'=>"RM 80.00", 'title'=>"Sumbangan ikhlas dari ahli keluarga dan lain-lain."))  ?>
        </div>
  </div>


<div class="row" style="border-top:1px solid #DDD;border-bottom:1px solid #DDD; padding:20px 0px 0px 10px">
   <div class="col-md-8">
    <h4 style="padding-top: 5px;">Jumlah Kewangan</h4>
   </div>
   <div class="col-md-4">
    <p id="total" class="bg-success" style="padding:10px">RM0.00</p> 
   </div> 
</div>


<div class="clearfix"></div>
<br/><br/>

 		<h2>Kos Nikah</h2>   


  <div class="form-group <?php echo Form::errorClass( 'calc4' ) ?>">
        <?php echo Form::label('ex01', 'Jurunikah / Kadi', ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( 'ex01', Input::old( 'calc4' ), array('class' => 'morecalc form-control input-lg', 
          'placeholder'=>"RM 200.00", 'title'=>"Upah Jurunikah"))  ?>
          <p class="help-block">Upah Jurunikah</p>
        <span id="error_calc4"></span>
        </div>
  </div>

  <div class="form-group <?php echo Form::errorClass( 'calc5' ) ?>">
        <?php echo Form::label('ex02', 'Baju Pengantin', ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( 'ex02', Input::old( 'calc5' ), array('class' => 'morecalc form-control input-lg', 
          'placeholder'=>"RM 2000.00", 'title'=>"Kos Baju Perkahwinan Anda &amp; Pasangan"))  ?>
        <span id="error_calc5"></span>
        </div>
  </div>


  <div class="form-group <?php echo Form::errorClass( 'calc5' ) ?>">
        <?php echo Form::label('ex03', 'Mak Andam', ['class' => 'col-md-4 control-label']); ?>
        <div class="col-md-4">
        <?php echo  Form::text( 'ex03', Input::old( 'calc5' ), array('class' => 'morecalc form-control input-lg', 
          'placeholder'=>"RM 2000.00", 'title'=>"Upah Make Up Mak Andam"))  ?>
        <span id="error_calc5"></span>
        </div>
  </div>

  
{{ Form::bootstrapInput4('ex04', 'Katering', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 12000.00", 'title'=>"Kos Keseluruhan Katering Akad Nikah"]) }}

{{ Form::bootstrapInput4('ex05', 'Sewa Khemah / Masjid', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 300.00", 'title'=>"Sewa Canopy / Masjid"]) }}
  
{{ Form::bootstrapInput4('ex06', 'Kos Mini Pelamin / Hiasan ', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 1500.00", 'title'=>"Kos Mini Pelamin dan Gubahan"]) }}
  
{{ Form::bootstrapInput4('ex07', 'Cincin Kahwin', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 500.00", 'title'=>"Harga Cincin Perkahwinan Anda"]) }}
  
{{ Form::bootstrapInput4('ex08', 'Dulang Hantaran', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 80.00", 'title'=>"Kos Sewa / Gubahan Dulang Hantaran"]) }}
  
{{ Form::bootstrapInput4('ex09', 'Door Gift ', '', ['class' => 'morecalc  form-control input-lg', 'placeholder'=>"RM 500.00", 'title'=>"Kos Door Gift"]) }}
  
  
<div class="row" style="border-top:1px solid #DDD;border-bottom:1px solid #DDD; padding:20px 0px 0px 10px">
   <div class="col-md-8">
    <h4 style="padding-top: 5px;">Kos Majlis Akad Nikah</h4>
   </div>
   <div class="col-md-4">
    <p id="anothertotal" class="bg-danger" style="padding:10px">RM0.00</p> 
   </div> 
</div>
  

 <h2>Kos Majlis Bersanding</h2>   


{{ Form::bootstrapInput4('ex10', 'Katering ', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 15,000.00", 'title'=>"Kos Keseluruhan Katering Majlis Bersanding"]) }}

{{ Form::bootstrapInput4('ex11', 'Sewa Khemah / Dewan ', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 1500.00", 'title'=>"Sewa Dewan / Sewa Kanopi"]) }}

{{ Form::bootstrapInput4('ex12', 'Baju Pengantin', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 2500.00", 'title'=>"Kos Baju Pengantin Anda &amp; Pasangan"]) }}

{{ Form::bootstrapInput4('ex13', 'Mak Andam', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 300.00", 'title'=>"Kos Make Up dan Andaman"]) }}

{{ Form::bootstrapInput4('ex14', 'Pelamin / Dekorasi', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 5000.00", 'title'=>"Kos Keseluruhan Pelamin"]) }}

{{ Form::bootstrapInput4('ex15', 'Photographer / Videographer ', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 3500.00", 'title'=>"Upah Jurugambar &amp; Videographer"]) }}
  
{{ Form::bootstrapInput4('ex16', 'Wedding Gift', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 1000.00", 'title'=>"Kos Wedding Gift &amp; VVIP Gift"]) }}
  
{{ Form::bootstrapInput4('ex17', 'Persembahan', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 1000.00", 'title'=>"Upah Persembahan Kompang / Silat"]) }}

  
{{ Form::bootstrapInput4('ex17', 'Persembahan', '', ['class' => 'extracalc  form-control input-lg', 'placeholder'=>"RM 1000.00", 'title'=>"Upah Persembahan Kompang / Silat"]) }}

<div class="row" style="border-top:1px solid #DDD;border-bottom:1px solid #DDD; padding:20px 0px 0px 10px">
   <div class="col-md-8">
    <h4 style="padding-top: 5px;">Kos Majlis Bersanding</h4>
   </div>
   <div class="col-md-4">
    <p id="plustotal" class="bg-danger" style="padding:10px">RM0.00</p> 
   </div> 
</div>
   
 
<br>  
<div class="col-md-7">  
  <center><h2>
   Bajet Kos Perkahwinan
  </h2></center></div>
<div class="row">
  <div class="col-md-7">
    <button type="button" value="Calculate" onclick="budget(budcalc);return false;" style="width:100%;" class="btn btn-block btn-lg btn-success">Calculate</button>  
  </div>
  </div>
  <br>
  <div class="row">
  <div class="col-md-7">
    <input type="button" name="res" value="RM" readonly="readonly" style="margin-left:0px; width:100%; font-weight:bold;" size="25" class="btn btn-block btn-lg btn-inverse">
  </div>
  
</div>  

  <table class="budcalc">
  </table>




<script>

// This Calculator generated by the
// Budget Calculator Generator Javascript
// at http://javascript.about.com/library/blbudget.htm
function stripBlanks(fld) {
	var result = "";
	var c = 0;
	for (i=0; i < fld.length; i++) {
		if ( fld.charAt(i) != " " || c > 0){  
			result += fld.charAt(i); 
			if (fld.charAt(i) != " ") 
				c = result.length;
		}
	}
	result = result.replace(',', ''); 
	return result.substr(0,c);
}
function budget(thisform) {
var result = 0;
var in1 = stripBlanks(thisform.in1.value);
if (in1 != '' && in1 != parseFloat(in1)) {alert("Wang Yang diberikan oleh Pihak lelaki must be numeric");thisform.in1.focus();return false;}
var in2 = stripBlanks(thisform.in2.value);
if (in2 != '' && in2 != parseFloat(in2)) {alert("Wang Yang diberikan oleh Pihak lelaki must be numeric");thisform.in2.focus();return false;}
var in3 = stripBlanks(thisform.in3.value);
if (in3 != '' && in3 != parseFloat(in3)) {alert("Wang Yang diberikan oleh Pihak lelaki must be numeric");thisform.in1.focus();return false;}  
result += Number(in1);
result += Number(in2);
result += Number(in3);
var ex01 = stripBlanks(thisform.ex01.value);
if (ex01 != '' && ex01 != parseFloat(ex01)) {alert("Jurunikah / Kadi must be numeric");thisform.ex01.focus();return false;}
result -= (ex01);
var ex02 = stripBlanks(thisform.ex02.value);
if (ex02 != '' && ex02 != parseFloat(ex02)) {alert("Baju Pengantin must be numeric");thisform.ex02.focus();return false;}
result -= (ex02);
var ex03 = stripBlanks(thisform.ex03.value);
if (ex03 != '' && ex03 != parseFloat(ex03)) {alert("Mak Andam must be numeric");thisform.ex03.focus();return false;}
result -= (ex03);
var ex04 = stripBlanks(thisform.ex04.value);
if (ex04 != '' && ex04 != parseFloat(ex04)) {alert("Katering must be numeric");thisform.ex04.focus();return false;}
result -= (ex04);
var ex05 = stripBlanks(thisform.ex05.value);
if (ex05 != '' && ex05 != parseFloat(ex05)) {alert("Katering must be numeric");thisform.ex05.focus();return false;}
result -= (ex05);
var ex06 = stripBlanks(thisform.ex06.value);
if (ex06 != '' && ex06 != parseFloat(ex06)) {alert("Katering must be numeric");thisform.ex06.focus();return false;}
result -= (ex06); 
var ex07 = stripBlanks(thisform.ex07.value);
if (ex07 != '' && ex07 != parseFloat(ex07)) {alert("Katering must be numeric");thisform.ex07.focus();return false;}
result -= (ex07); 
var ex08 = stripBlanks(thisform.ex08.value);
if (ex08 != '' && ex08 != parseFloat(ex08)) {alert("Katering must be numeric");thisform.ex08.focus();return false;}
result -= (ex08); 
var ex09 = stripBlanks(thisform.ex09.value);
if (ex09 != '' && ex09 != parseFloat(ex09)) {alert("Katering must be numeric");thisform.ex09.focus();return false;}
result -= (ex09);
var ex10 = stripBlanks(thisform.ex10.value);
if (ex10 != '' && ex10 != parseFloat(ex10)) {alert("Katering must be numeric");thisform.ex10.focus();return false;}
result -= (ex10);
var ex11 = stripBlanks(thisform.ex11.value);
if (ex11 != '' && ex11 != parseFloat(ex11)) {alert("Katering must be numeric");thisform.ex11.focus();return false;}
result -= (ex11);
var ex12 = stripBlanks(thisform.ex12.value);
if (ex12 != '' && ex12 != parseFloat(ex12)) {alert("Katering must be numeric");thisform.ex12.focus();return false;}
result -= (ex12);
var ex13 = stripBlanks(thisform.ex13.value);
if (ex13 != '' && ex13 != parseFloat(ex13)) {alert("Katering must be numeric");thisform.ex13.focus();return false;}
result -= (ex13);
var ex14 = stripBlanks(thisform.ex14.value);
if (ex14 != '' && ex14 != parseFloat(ex14)) {alert("Katering must be numeric");thisform.ex14.focus();return false;}
result -= (ex14);
var ex15 = stripBlanks(thisform.ex15.value);
if (ex15 != '' && ex15 != parseFloat(ex15)) {alert("Katering must be numeric");thisform.ex15.focus();return false;}
result -= (ex15);
var ex16 = stripBlanks(thisform.ex16.value);
if (ex16 != '' && ex16 != parseFloat(ex16)) {alert("Katering must be numeric");thisform.ex16.focus();return false;}
result -= (ex16);
var ex17 = stripBlanks(thisform.ex17.value);
if (ex17 != '' && ex17 != parseFloat(ex17)) {alert("Katering must be numeric");thisform.ex17.focus();return false;}
result -= (ex17);  
thisform.res.value = "RM" + result;}
  

  jQuery(document).ready(function(){
   
    // call doTotals() once when the page loads
    doTotals('calc','total','error_');            
    doTotals('morecalc','anothertotal','error_');
    doTotals('extracalc','plustotal','error_');
   
    // call doTotals() each time one of the 'calc' fields or 'morecalc' fields change
    jQuery('.calc').change(function(){
      doTotals('calc', 'total','error_');
    });
    jQuery('.morecalc').change(function(){
      doTotals('morecalc','anothertotal','error_'); 
    });    
    jQuery('.extracalc').change(function(){
      doTotals('extracalc','plustotal','error_'); 
    });
    
  });
  
 // spiffy function that adds commas to a number
function addCommas(nStr) {
 
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

//  
 
/*
 this function...
  1. adds a group of form fields
  2. formats the form fields
  3. writes the total
  4. highlights any errors
 
usage...
 
  doTotals('calc','total','error_');
  where
    'calc' is a the class name that is present in each form field that you want to add
    and
    'total' is the id of the span/div that you wish to display the total in
    and
    'error_'
    is the name of the span that accompanies each form field.  an error message will be displayed in this span
*/
function doTotals(valuesClassName, totalIdName, errorIdPrefix){
 
 
  var doWriteDollarSignsToFormFields = true;
  var total = 0;
  jQuery('.' + valuesClassName).each(function(){
 
  value = jQuery(this).val();
 
  // remove all whitespace
  while(value.indexOf(' ') != -1){
    value = value.replace(' ', ''); 
  }
 
  // remove all dollar signs
  while(value.indexOf('RM') != -1){
    value = value.replace('RM', '');   
  }
 
  // remove all commas
  while(value.indexOf(',') != -1){
    value = value.replace(',', '');     
  }
 
  // if at this point value is blank, change it to 0
  if(value == ''){
    value = 'placeholder';
  }
 
  // if at this point the value is not blank and is a number...       
  errorFieldName = '#' + errorIdPrefix + jQuery(this).attr('id');
  if(value != '' && ! isNaN(value)){
 
    // write the cleaned value (with commas) to the form field
    if(doWriteDollarSignsToFormFields == true){
      jQuery(this).val('' + addCommas(parseFloat(value).toFixed(2)));
    }else{
      jQuery(this).val(addCommas(parseFloat(value).toFixed(2)));
    }
 
    // convert the value and add it to the total
    valueParsed = parseFloat(value);
 
    total = total + valueParsed;
    // console.log('remove error from span id ' + errorFieldName);
    jQuery(errorFieldName).html("");
  }else{
    // console.log('write error to span id ' + errorFieldName);
    jQuery(errorFieldName).html("");
  }
 
  });
 
  // update the total
  if(doWriteDollarSignsToFormFields == true){
    jQuery('#' + totalIdName).html('RM' + addCommas(parseFloat(total).toFixed(2)));
  }else{
    jQuery('#' + totalIdName).html(addCommas(parseFloat(total).toFixed(2)));
  }
} 

 $(function() {
    $( document ).tooltip();
  });  

  

  

</script>

	
						 </div>

             {{ Form::close() }}

				</div>




</div>

@stop