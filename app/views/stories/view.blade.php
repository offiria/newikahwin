@extends('layouts.default')
@section('title', $story->title )
@section('meta-description', $story->metadesc )
@section('meta-keywords', $story->metakey )
@section('meta')
<meta property="og:title" content="{{{ $story->name }}}" />
<meta property="og:type" content="website" />
<meta property="og:image" content="{{ asset( $story->cover_img ) }}" />
<meta property="og:url" content="{{ Request::fullUrl() }}" />
<meta property="og:site_name" content="iKahwin" />
<meta property="og:description" content="{{{ $story->excerpt }}}" />
<meta property="fb:app_id" content="1547213035507514" />
@stop
@section('content')
<link rel="image_src" href="{{ asset( $story->cover_img ) }}" />
<?php
$videoUrls = $story->getVideoUrls();
?>
<style>
.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px; height: 0; overflow: hidden;
}
 
.video-container iframe,
.video-container object,
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

</style>

<div class="listing-header blog-categories">
	<div class="container">
		<h1 class="listing-title" style="display: none;">Stories</h1>

		<div class="category-bar clearfix">
			<ul class="list-inline center-block">
				{{--
				<li>Categories</li>
				--}}
				@foreach($categories as $category)
					<li><a href="{{ URL::to('blog/category/' . $category->slug) }}">{{ $category->name }}</a></li>
				@endforeach
			</ul>
			<!-- <div class="listing-sorter pull-right">
				<div class="dropdown listing-sortby">
					Sort by
					<button class="btn btn-sort dropdown-toggle" type="button" data-toggle="dropdown">
						{{ ( ! empty($sortBy) ? ucfirst($sortBy) : 'Date') }}
						<i class="fa fa-chevron-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<li><a href="" data-sortby="date">Date</a></li>
						<li><a href="" data-sortby="festivals">Festivals</a></li>
					</ul>
				</div>
			</div> -->
		</div>
	</div>
</div>

<div class="container">

	<div class="story">
		<h1 class="main-title">{{{ $story->title }}}</h1>
		<div class="row">
			
			<div class="col-md-12 item-content">
				<div class="inner-content">			

					<div class="img-container">
						@if( !empty($vimeoUrls))
						<div class="video-container">
         					<iframe width="640" height="360" src="http://www.youtube.com/embed/{{$story->getVideoId()}}" frameborder="0" allowfullscreen></iframe>
						</div>
						@elseif( !empty($videoUrls) )
						<div class="video-container">
         					<iframe width="640" height="360" src="http://www.youtube.com/embed/{{$story->getVideoId()}}" frameborder="0" allowfullscreen></iframe>
						</div>
						
						@else
							@if(count($story->photos) == 0) 
								<img src="{{ asset($story->cover_img) }}" >

							@else

							<div id="carousel" class="festival-carousel carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#carousel" data-slide-to="0" class="active"></li>
									<?php
									$i = 1;
									?>
									@foreach ($story->photos as $photo ) 
										<li data-target="#carousel" data-slide-to="{{ $i }}"></li>
										<?php $i++; ?>
									@endforeach
									
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<img src="{{ asset( $story->cover_img ) }}" >
										<div class="carousel-caption">
										<!-- caption goes here -->
										</div>
									</div>
									
									@foreach ($story->photos as $photo ) 
									<div class="item">
										<img src="{{ asset( $photo->path ) }}" >
										<div class="carousel-caption">
										<!-- caption goes here -->
										</div>
									</div>
									@endforeach
								</div>

								<!-- Controls -->
								<a class="left carousel-control" href="#carousel" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right carousel-control" href="#carousel" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						

							@endif
						@endif
					</div>

					    

				</div>

				<div class="row">
					<div class="col-md-8">
						<div class="inner-left-tags">
							@if($story->tags)
							<i class="fa fa-tags"></i> 
								@foreach($story->tags as $tag)
									<a href="{{ URL::to('blog?tag='. $tag->name) }}" class="btn btn-default btn-xs" target="_blank" rel="no-follow">{{{ $tag->name }}}</a>
								@endforeach								
							
							@endif
							<div class="clearfix" ></div>
						</div>

						<div class="inner-left-excerpt">
							<p>{{{ $story->excerpt }}}</p>
						</div>

						<div class="inner-left-content">
							<p>{{ $story->story }}</p>
						</div>

						@if( count($story->photos) > 0)
							@foreach ($story->photos as $photo ) 
								<div class="thumbnail">
								<img src="{{ asset( $photo->path ) }}" >
								<div class="caption">
									<p>
										This is the photo caption. This should explain what the photo is all about. Maybe even the pricing for example
									</p>
								</div>
								</div>
							@endforeach
						@endif

						<div class="row  visible-xs-block">
						<div class="col-md-12">
							<a href="{{ url('vendors/wedding-gift/charmaine-nash') }}"><img class="img-responsive" src="{{ asset('stamp_banner.jpg') }}" /></a>
							<br/>
						</div>
						</div>

						<div id="disqus_thread"></div>
					    <script type="text/javascript">
					        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
					        var disqus_shortname = 'ikahwin'; // required: replace example with your forum shortname

					        /* * * DON'T EDIT BELOW THIS LINE * * */
					        (function() {
					            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					        })();
					    </script>
					    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>	
						
					</div>

					<div class="col-md-4" style="border-left: 1px solid #eee;">
						<div class="inner-tags-content">
		
							<div class="inner-tags-listing clearfix">
								<div class="fb-like" data-href="{{ Request::fullUrl() }}" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>

							</div>
						</div>

					<h4>Recommended</h4>
						<div class="story-more">
						@foreach ($recommended as $s) 
							<div class="col-md-12" style="border-bottom: 1px solid #eee; padding:8px 0px">
								<img src="{{ asset( $s->cover_thumb ) }}" class="img-responsive" style="float: left;padding-right:4px;max-width: 40%;">
								<div class="box-content" style="float: left;width: 60%;padding-left: 6px;font-size: 16px;">
									<h3><a href="{{ URL::to('blog/' . $s->slug) }}">{{{ $s->title }}}</a></h3>
								</div>
								<div class="clearfix"></div>
							</div>

						@endforeach
						</div>
						
						@include('stories.sidebar')
					</div>

				</div>

				<ul class="list-inline clearfix widget-author hide">
						<li>
							<img src="{{ asset($story->user->image)}}" class="img-circle">
						</li>
						<li class="admin-info">
							<h4>Text &amp; Photos by</h4>
							<h5 class="author">{{{ $story->author->firstname }}} {{{ $story->author->lastname }}}</h5>
							<p class="author-profile">{{{ $story->author->description }}}</p>
							<a href="{{ $story->author->link }}" class="author-link">{{{ $story->author->link }}}</a>
						</li>
					</ul>
					
				
					    

			</div>

			

		</div>
	</div>

</div>
@stop