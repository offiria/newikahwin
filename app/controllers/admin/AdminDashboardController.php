<?php

use Gregwar\Image\Image;

class AdminDashboardController extends BaseController {

	public function getIndex()
	{
		//
	}


	public function postCover($classname, $obj_id){
		$user = Auth::user();
		$uploader = new FileUploader();
		$uploaderPath = '/covers'. '/';

		
		$result = $uploader->handleUpload(public_path() . $uploaderPath);

		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['cover_img'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);
		$result['cover_thumb'] 	= str_replace(public_path(), '', $result['path']. '_thumb.'.$path_parts['extension']);
		$result['original'] 	= str_replace(public_path(), '', $result['path']. '_original.'.$path_parts['extension']);
		$result['fbog'] 		= str_replace(public_path(), '', $result['path']. '_fb.'.$path_parts['extension']);

		// resize image
		Image::open($result['path']. '.'.$path_parts['extension'])
		    ->zoomCrop(994, 460)
		    ->save(public_path().''.$result['cover_img']);

		// resize for fb
		Image::open($result['path']. '.'.$path_parts['extension'])
		    ->zoomCrop(600, 315)
		    ->merge( Image::open( public_path() . '/assets/img/premium.png') , 402, 244)
		    ->save(public_path().''.$result['fbog']);

		// resize for thumb, different size for blog/vendor
		if( strtolower($classname) == 'story'){
			Image::open($result['path']. '.'.$path_parts['extension'])
			    ->zoomCrop(450, 245)
			    ->save(public_path().$result['cover_thumb']);
		} else {
			Image::open($result['path']. '.'.$path_parts['extension'])
			    ->zoomCrop(450, 200)
			    ->save(public_path().$result['cover_thumb']);
		}

		// Copy original file
		copy( $result['path'] . '.'.$path_parts['extension'], public_path().$result['original']);

		// load object
		$coverable = $classname::findOrNew($obj_id);

		// delete old img
	    if(!strstr($coverable->cover_img, 'default')){
	    	@unlink( public_path(). $coverable->cover_img);

	    	// delete fb cover image
	    	$path = $coverable->cover_img;
	        $path = str_replace( '.', '_fb.', $path);
	        if( file_exists(storage_path() . $path)) {
	            @unlink( public_path(). $path);
	        }
	    }

	    if(!strstr($coverable->cover_thumb, 'default')){
	    	@unlink( public_path(). $coverable->cover_thumb);	
	    }

	    // if new, we need to set soem value
	    if( empty($coverable->user_id)){
	    	$coverable->user_id = $user->id;
	    }


		$coverable->cover_img 	= $result['cover_img'];
		$coverable->cover_thumb 	= $result['cover_thumb'];
		$coverable->save();

		// for front-end
		$result['action'] = url( 'admin/'. strtolower( str_plural($classname)) . '/' . $coverable->id . '/edit' );
		$result['src'] = asset($result['cover_thumb']);
		return Response::json($result );
	}

	/**
	 * Upload a photo for the given entry
	 **/
	public function postPhoto($classname, $obj_id){
		$uploader = new FileUploader();
		$uploaderPath = '/photos'. '/';

		
		$result = $uploader->handleUpload(public_path() . $uploaderPath);

		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['path'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);

		// resize image
		// if( class_exists('Imagick')){
		// 	$image = new Imagick($result['path']);
		// 	$image->cropThumbnailImage(1724, 1148); // Crop image and thumb
		// 	$image->writeImage($result['path']);

		// } else {
			
		// }

		Image::open(public_path() . $result['path'])
			    ->zoomCrop(1724, 1148)
			    ->save(public_path().''.$result['path']);
			

		// load object
		$imageable = $classname::find($obj_id);
		$photo = Photo::create([
			'imageable_id' => $imageable->id,
			'imageable_type' => $classname,
			'path' => $result['path']
			]);
		
		// html
		$result['html'] = View::make('admin.common.photolist', ['photo' => $photo ])->render();

		// for front-end
		return Response::json($result );
	}

	/**
	 * Upload a photo for the given entry
	 **/
	public function postAlbumPhoto($classname, $obj_id, $album_id){
		$uploader = new FileUploader();
		$uploaderPath = '/photos'. '/';
		
		$result = $uploader->handleUpload(public_path() . $uploaderPath);


		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['path'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);
		$result['cover_thumb'] 	= str_replace(public_path(), '', $result['path']. '_thumb.'.$path_parts['extension']);
		$result['original'] 	= str_replace(public_path(), '', $result['path']. '_original.'.$path_parts['extension']);

		// resize image
		Image::open(public_path() . $result['path'])
		    ->zoomCrop(1724, 1148)
		    ->save(public_path().''.$result['path']);
		// resize for thumb
		Image::open($result['path']. '.'.$path_parts['extension'])
		    ->zoomCrop(510, 340)
		    ->save(public_path().$result['cover_thumb']);

		// Copy original file
		copy( public_path(). $result['path'] , public_path(). $result['original']);

		// load object
		$imageable = $classname::find($obj_id);
		$photo = Photo::create([
			'imageable_id' => $imageable->id,
			'imageable_type' => $classname,
			'album_id' => $album_id,
			'path' => $result['path']
			]);
		
		// html
		$result['html'] = View::make('admin.common.albumphoto', ['photo' => $photo ])->render();
		$result['albumid'] = $album_id;

		// for front-end
		return Response::json($result );
	}

	/**
	 * Upload a photo for blog
	 **/
	public function postBlogPhoto(){
		$dir = public_path() . '/images/blogphoto'. '/';
		$_FILES['file']['type'] = strtolower($_FILES['file']['type']);
 
		if ($_FILES['file']['type'] == 'image/png'
		|| $_FILES['file']['type'] == 'image/jpg'
		|| $_FILES['file']['type'] == 'image/gif'
		|| $_FILES['file']['type'] == 'image/jpeg'
		|| $_FILES['file']['type'] == 'image/pjpeg')
		{
		    // setting file's mysterious name
		    $filename = md5(date('YmdHis')).'.jpg';
		    $file = $dir.$filename;
		 
		    // copying
		    move_uploaded_file($_FILES['file']['tmp_name'], $file);
		 
		    // displaying file
		    $array = array(
		        'filelink' =>  asset('images/blogphoto/'.$filename)
		    );
		 
		    // echo stripslashes(json_encode($array));
		    return Response::json($array );
		 
		}

		return;


		$uploader = new FileUploader();
		$uploaderPath = '/images/blogphoto'. '/';

		
		$result = $uploader->handleUpload(public_path() . $uploaderPath);
		print_r($result); exit;
		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['path'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);

		// // resize image
		// Image::open(public_path() . $result['path'])
		//     ->zoomCrop(862, 574)
		//     ->save(public_path().''.$result['path']);

		// // load object
		// $imageable = $classname::find($obj_id);
		// $photo = Photo::create([
		// 	'imageable_id' => $imageable->id,
		// 	'imageable_type' => $classname,
		// 	'path' => $result['path']
		// 	]);
		
		// html
		$result['filelink'] = asset($result['path']  );
		//$result['html'] = View::make('admin.common.photolist', ['photo' => $photo ])->render();

		// for front-end
		return Response::json($result );
	}

	

	/**
	 * Upload a photo for the given entry
	 **/
	public function postDelete($id){
		$photo = Photo::find($id);
		$photo->delete();
		$result = ['success' => true];
		$result['id'] = $id;

		// for front-end
		return Response::json($result );
	}

	public function selectPhoto( $type, $id, $element ){
		$type = ucfirst($type);
		$obj = $type::find($id);
		$photos = $obj->photos;

		return View::make('admin/common/photoselect', ['photos' => $photos, 'element'=> '#'.$element]);
	}
}