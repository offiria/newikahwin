@extends('layouts.default')

@section('content')

<div class="packages-container">
	<div class="modal-packages">
	   <div class="modal-dialog">
		  <div class="modal-content">

		  	@if(Session::has('message-package'))
		  	<div class="alert alert-success alert-growl">
		  		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  		{{ Session::get('message-package') }}
		  	</div>
		  	@endif

			 <div class="modal-header">
				<h4 class="modal-title text-center" id="myModalLabel">Hassle-free festive travels.</h4>
				<!-- <h3 class="modal-sub-title text-center">
				   Same price, just save time on research <br>and save money on mistakes.
				</h3> -->
			 </div>
		  
			 <div class="modal-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-tabs-dotted" role="tablist">
				   <li><a href="#customised-package" role="tab" data-toggle="tab">Customised Luxury Package</a></li>
				   @foreach($festival->packages as $package)
				   <li><a href="#package-{{ $package->id }}" role="tab" data-toggle="tab">{{ $package->name }}</a></li>
				   @endforeach
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				   <div class="tab-pane" id="customised-package">
					  <h3 class="title-note text-center">
						 Our agent will get in touch within 24 hours (weekdays only) to offer end-to-end options for a truly pampered festive travel experience.
					  </h3>
					  	{{-- @TODO: Widget --}}
					    {{ Form::open(array('url' => 'festivals/' . $festival->slug . '/packages/orders/create', 'method' => 'post', 'class' => 'form-lm clearfix')) }}

					  	 <h3 class="form-title text-center">Festival Details</h3>
					  	 <label for="">{{ $festival->name }}</label>
					  	 <br/><br/>

					  	  <div class="form-group">
					  	 	<label for="">Departure City</label>
					  	 	{{ Form::text('departure_city', Input::old('departure_city'), array('class' => 'form-control', 'placeholder' => 'Departure City')) }}
					  	  </div>

					  	 <label for="">Dates</label>
					  	 <div class="form-group">
					  		
					  		{{ Form::text('departure_date', Input::old('departure_date'), array('class' => 'form-control datepicker', 'placeholder' => 'Departure Date')) }}
					  		
					  		{{-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> --}}
					  	 </div>

					  	 <div class="form-group">
					  		<label for="" class="sr-only"></label>
					  		{{ Form::text('return_date', Input::old('return_date'), array('class' => 'form-control datepicker', 'placeholder' => 'Return Date')) }}
					  		{{-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> --}}
					  	 </div>

					  	 <label for="">Pax</label>
					  	 <div class="form-group">
					  		{{ Form::text('adults', Input::old('adults'), array('class' => 'form-control', 'placeholder' => 'Number of Adults (12+)')) }}
					  	 </div>
					  	 <div class="form-group">
					  		{{ Form::text('children', Input::old('children'), array('class' => 'form-control', 'placeholder' => 'Number of Children')) }}
					  	 </div>

					    <div class="form-separator"></div>

					  	<h3 class="form-title text-center">Your Details</h3>

					  	<div class="form-group">
					  	{{ Form::text('fullname', Input::old('fullname'), array('class' => 'form-control', 'placeholder' => 'Full Name')) }}
					  	</div>
					  	<div class="form-group">
					  		<div class="input-group">
  							<span class="input-group-addon">+</span>
					  		{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
					  		</div>
					  	</div>
					  	<div class="form-group">
					  	{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'E-Mail Address')) }}
					  	</div>

					  	<div class="modal-footer">
					  	{{ Form::submit('Book Now', array('class' => 'btn btn-green')) }}
					  	</div>

					  	{{ Form::close() }}
				   </div>

				   @foreach($festival->packages as $package)
				   <div class="tab-pane" id="package-{{ $package->id }}">
					  <ul class="list-unstyled package-details text-center">
						 {{-- <li>
							<h5>Hotel</h5>
							• # Star Hotel/ BnB is the closest medium range 
							hotel (there’s a shuttle to the festival from the 
							hotel/ The festival is within walking distance of 
							the festival) x Y nights for 2 persons
						 </li>
						 <li>
							<h5>Breakfast</h5>
							• Including breakfast/Without Breakfast
						 </li>
						 <li>
							<h5>Festival Ticket</h5>
							• Festival ticket (what type)
						 </li>
						 <li>
							<h5>Package</h5>
							2 persons x 4 nights
						 </li> --}}
						 <li>
						 {{ $package->description }}
						 </li>
						 <li class="total-price">
							<!-- <h5>Total Price</h5> 
							{{ $package->price }}
							-->
						 </li>
					  </ul>

					  <?php /* // {{-- --}} not commenting
					  {{ Form::open(array('url' => 'festivals/' . $festival->slug . '/packages/orders/create', 'method' => 'post', 'class' => 'form-lm clearfix')) }}

						 <h3 class="form-title text-center">Festival Details</h3>
						 <label for="">Festival</label>
						 <div class="input-group">
						 	{{ $festival->name }}
							{{ Form::hidden('package_id', $package->id) }}
						 </div><!-- /input-group -->

						 <label for="">Dates</label>
						 <div class="form-group">
							<label for="" class="sr-only"></label>
							{{ Form::text('departure_date', Input::old('departure_date'), array('class' => 'form-control datepicker', 'placeholder' => 'Departure Date')) }}
							{{-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> --}}
						 </div>

						 <div class="form-group">
							<label for="" class="sr-only"></label>
							{{ Form::text('return_date', Input::old('return_date'), array('class' => 'form-control datepicker', 'placeholder' => 'Return Date')) }}
							{{-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> --}}
						 </div>

						 <label for="">Pax</label>
						 <div class="form-group">
							{{ Form::text('adults', Input::old('adults'), array('class' => 'form-control', 'placeholder' => 'Number of Adults (12+)')) }}
						 </div>
						 <div class="form-group">
							{{ Form::text('children', Input::old('children'), array('class' => 'form-control', 'placeholder' => 'Number of Children')) }}
						 </div>

					  <div class="form-separator"></div>

						<h3 class="form-title text-center">Your Details</h3>

						<div class="form-group">
						{{ Form::text('fullname', Input::old('fullname'), array('class' => 'form-control', 'placeholder' => 'Full Name')) }}
						</div>
						<div class="form-group">
							<div class="input-group">
  							<span class="input-group-addon">+</span>
								{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
							</div>
						</div>
						<div class="form-group">
						{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'E-Mail Address')) }}
						</div>

						<div class="modal-footer">
						{{ Form::submit('Book Now', array('class' => 'btn btn-green')) }}
						</div>

						{{ Form::close() }}
				   */ ?>
				   </div>

				   @endforeach
				</div>
			 </div>
		  </div>
	   </div>
	</div>
</div>

{{--
<div class="related-stories features-slider hide">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="slider-title text-center">Related Stories</h2>
				<div id="wheretogo" class="carousel slide" data-ride="carousel">

					<!-- Wrapper for slides -->
					<div class="carousel-inner">

						<div class="item active">
							<ul class="list-unstyled row thumbnails">
								<li class="col-md-4">
									<a href="#">
										
										<div class="img-container">
											<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
										</div>
										
										<div class="carousel-caption">
											<h4>Title of Related Stories</h4>
											<div class="author">
												<span>by</span> Haanim Bamadhaj
											</div>
										</div>
									</a>
								</li>
								<li class="col-md-4">
									<a href="#">
										
										<div class="img-container">
											<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
										</div>
										
										<div class="carousel-caption">
											<h4>Title of Related Stories</h4>
											<div class="author">
												<span>by</span> Haanim Bamadhaj
											</div>
										</div>
									</a>
								</li>
								<li class="col-md-4">
									<a href="#">
										
										<div class="img-container">
											<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
										</div>
										
										<div class="carousel-caption">
											<h4>Title of Related Stories</h4>
											<div class="author">
												<span>by</span> Haanim Bamadhaj
											</div>
										</div>
									</a>
								</li>
							</ul>
						</div>

					</div>

				</div>

			</div>
		</div>
	</div>
</div>
--}}

<script>
$(function() {
	$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
		window.setTimeout(function() {
		    $(window).scrollTop(0); 
		}, 0);
	});

	// Active tab from URL
	var hash = window.location.hash;
	hash && $('ul.nav a[href="' + hash + '"]').tab('show');

	$('.nav-tabs a').click(function (e) {
		$(this).tab('show');
		var scrollmem = $('body').scrollTop();
		window.location.hash = this.hash;
		$('html,body').scrollTop(scrollmem);
	});
});
</script>

@stop