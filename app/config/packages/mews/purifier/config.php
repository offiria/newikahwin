<?php

/*
 * This file is part of HTMLPurifier Bundle.
 * (c) 2012 Maxime Dizerens
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return array(
	'encoding' => 'UTF-8',
    'finalize' => true,
    'preload'  => false,
    'settings' => array(
        // 'default' => [
        //     'HTML.Doctype'             => 'XHTML 1.0 Strict',
        //     'HTML.Allowed'             => 'div,b,strong,i,em,a[href|title],ul,ol,li,p,br,span,table[border|width|style],tbody,tr,td[abbr],th[abbr],img[width|height|alt|src],h1,h2,h3,iframe[width|height|src|frameborder]',
        //     'HTML.SafeIframe'           => true,
        //     'AutoFormat.AutoParagraph' => true,
        //     'AutoFormat.RemoveEmpty'   => true,
        // ]

        'default' => array(
            'HTML.Doctype'             => 'XHTML 1.0 Strict',
            'HTML.Allowed'             => 'div,b,strong,i,em,a[href|title],ul,ol,li,p,br,span,table[summary|border|width|style],tbody,tr,td[abbr],th[abbr],img[width|height|alt|src],h1,h2,h3',
            'CSS.AllowedProperties'    => 'font,font-size,font-weight,font-style,font-family,text-decoration,padding-left,color,background-color,text-align',
            'AutoFormat.AutoParagraph' => true,
            'AutoFormat.RemoveEmpty'   => true,
        ),
    ),
);
