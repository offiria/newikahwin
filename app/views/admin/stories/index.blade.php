@extends('admin.layouts.default')

@section('content')

<a class="btn btn-yellow pull-right" href="<?php echo url('admin/stories/create'); ?>"><i class="fa fa-plus"></i> Add Story</a>

<h2 class="admin-title">Stories</h2>

<div class="admin-search">
	<form role="form" id="user-filter">
		<div class="form-group form-filter">
			<input type="text" class="form-control" id="" placeholder="Search" name="keyword" value="{{{ Input::get('keyword', '') }}}">
		</div>

		<div class="admin-filter">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						{{ Form::select('user_id', $contributors, Input::get('user_id', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('verified', array('' => '-- Status --', 1 => 'On-going', 0 => 'Passed'), Input::get('', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						{{ Form::select('disabled', array('' => '-- Date --', 1 => 'Yes', 0 => 'No'), Input::get('', ''), array('class' => 'form-control')) }}
					</div>
					<div class="col-md-2">
						<a href="{{ url('admin/stories') }}" class="btn btn-default">reset</a>
					</div>
				</div>
			</div>
		</div>

	</form>
</div>

<table class="table table-hover table-bordered admin-table">
	<thead>
	<tr>
		<th width="30%" colspan="2">Story title</th>
		<th width="10%">Owner</th>
		<th width="5%">Featured</th>
		<th width="10%">Publish Date</th>
		<th width="8%" class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($stories as $i => $story)
	<tr>
		<td width="5%"><img src="{{ asset($story->cover_thumb) }}" height="60" />
		<td>{{{ $story->title }}}</td>
		<td> {{ $story->user->firstname }} </td>
		<td>@if($story->featured) <i class="fa fa-star"></i> @else <i class="fa fa-star-o"></i> @endif </td>
		<td> {{$story->created_at}} </td>
		<td class="text-center">
			<a class="btn btn-default btn-fb btn-edit" title="Edit" alt="Edit" href="{{ url('admin/stories/' . $story->id . '/edit') }}"><i class="fa fa-edit"></i></a>
			<a class="btn btn-default btn-warning btn-remove confirmation" title="Delete" alt="Delete" href="{{ url('admin/stories/' . $story->id . '/delete') }}"><i class="fa fa-times"></i></a>
		</td>
	</tr>
	@endforeach
	</tbody>
</table>


<?php /*
<div class="row">
	<div class="col-md-12">
		<div class="festival-content">
			<div id="posts" class="row festival-items">
				
				@foreach($stories as $i => $story)
				<div class="col-md-4 festival-item item">
					<div class="thumbnail">
						<img src="<?php echo e(asset('assets/img/43H.jpg')); ?>">
						<div class="box-content">
							<h3><a href="#">{{{ $story->title }}}</a></h3>
							<div class="date">$story->formatDate('start_date')</div>

						</div>

						<div class="item-action row">
							<div class="col-md-6">
								<a class="btn btn-default btn-fb btn-block" href="<?php echo url('admin/stories/' . $story->id . '/edit'); ?>"><i class="fa fa-edit"></i> Edit</a>
							</div>
							<div class="col-md-6">
								<a class="btn btn-default btn-warning btn-block confirmation" href="<?php echo url('admin/stories/' . $story->id . '/delete'); ?>"><i class="fa fa-times"></i> Delete</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
</div>
*/?>
<div class="text-center">
{{ $stories->links() }}
</div>

@stop

@section('scripts')

<?php /*
<script>
$(document).ready(function(){
	/* activate jquery isotope 
	$('#posts').imagesLoaded( function(){
    $('#posts').isotope({
      itemSelector : '.item'
    });
  });	
});
</script>
*/?>
@stop