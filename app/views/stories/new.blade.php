@extends('layouts.default')

@section('content')

{{ Form::open(array('url' => 'stories/submit', 'class' => '', 'method' => 'post')) }}

	{{ Form::token() }}

	<div class="form-group {{ Form::errorClass('festival') }}">
		<label for="" class="control-label">Name of Festival</label>
		{{ Form::select('festival', array('' => '-- Select --') + $festivals, null, array('class' => 'form-control')) }}
		{{ Form::errorMsg('festival') }}
	</div>

	<div class="form-group {{ Form::errorClass('category') }}">
		<label for="" class="control-label">Story Category</label>
		{{ Form::select('category', array('' => '-- Select --') + $categories, null, array('class' => 'form-control')) }}
		{{ Form::errorMsg('category') }}
	</div>

	<div class="form-group {{ Form::errorClass('title') }}">
		{{ Form::label('title', 'Title', array('class' => 'control-label')) }}
		{{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
		{{ Form::errorMsg('title') }}
	</div>

	<div class="form-group {{ Form::errorClass('story') }}">
		<label for="" class="control-label">Body Text</label>
		{{ Form::textarea('story', null, array('class' => 'form-control', 'rows' => 5)) }}
		{{ Form::errorMsg('story') }}
	</div>

	<div class="form-group form-action">
		{{ Form::submit('Continue', array('class' => 'btn btn-success btn-lg')) }}
	</div>

{{ Form::close() }}

@stop