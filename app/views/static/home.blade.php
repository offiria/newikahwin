<!DOCTYPE html>

<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="iKahwin">
	<meta name="author" content="zuL">
	
	<link rel="shortcut icon" type="image/ico" href="img/favicon.png">
	<link rel="apple-touch-icon" href="http://festivalguideasia.com/">
	<link rel="apple-touch-icon" sizes="144x144" href="#">
	<link rel="apple-touch-icon" sizes="114x114" href="#">

	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/bootstrap.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/font-awesome/css/font-awesome.min.css') }}}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{{ asset('assets/css/style.css') }}}" media="screen">

	<!-- Utilities -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  	<script>window.jQuery || document.write('<script src="js/library/jquery/jquery.min.js">\x3C/script>')</script>
	<script src="{{{ asset('assets/js/bootstrap.min.js') }}}" type="text/javascript"></script>
	<script src="{{{ asset('assets/js/modernizr-latest.js') }}}" type="text/javascript"></script>

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js" type="text/javascript"></script>
	<![endif]-->

	<title>iKahwin</title>

	<script type="text/javascript">
		$('.carousel').carousel({
			interval: false
		});
	</script>

</head>

<body>
	<section id="header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-inverse navbar-default" role="navigation">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.html">iKahwin</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="collapse">
							
							<ul class="nav navbar-nav navbar-right">
								<li class="link-festival"><a href="#">Festival</a></li>
								<li class="link-stories"><a href="#">Stories</a></li>
								<li class="link-submit"><a href="#">Submit</a></li>
								<li class="link-login"><a href="#">Login</a></li>
							</ul>

						</div><!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>
		</div>
	</section>

	<section id="site-search">
		<div class="container">
			<div class="row">
				<form>
					<div class="col-md-3 col-first">
						<label for="">Search Festivals</label>
						<div class="input-group">
							<input type="text" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div><!-- /input-group -->
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<label for="">Or Browse</label>
							</div>
							<div class="form-group col-md-4">
								<input type="text" class="form-control" id="" placeholder="Places">
							</div>
							<div class="form-group col-md-4">
								<label for="" class="sr-only"></label>
								<input type="text" class="form-control" id="" placeholder="Dates">
							</div>
							<div class="form-group col-md-4">
								<label for="" class="sr-only"></label>
								<input type="text" class="form-control" id="" placeholder="Genres">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

	<section id="content">
		<div class="container">
			<div class="hero-slider">
				<div class="row">
					<div class="col-md-12">
						<div id="main-carousel" class="carousel slide" data-ride="carousel">
						
							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="item active">
									<div class="row">
										<div class="col-md-8">
											<div class="carousel-media">
												<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image" style="position: relative; top: -220px;">
											</div>
										</div>
										<div class="col-md-4">
											<div class="carousel-caption text-center">
												<div class="ribbon-title ribbon-yellow clearfix">
													<div class="left"></div>
													<div class="center">
														<h3 class="title">Featured Festival</h3>
													</div>
													<div class="right"></div>
												</div>
												<h1>Strawberry Fields Festival 2014</h1>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius.</p>
												<a href="#">Check festival out</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="content-slider">
				<div class="row">
					<div class="col-md-4">
						<div class="box upcoming">
							<div class="ribbon-title-container clearfix">
								<div class="ribbon-title ribbon-yellow ribbon-left clearfix">
									<div class="left"></div>
									<div class="center">
										<h3 class="title">Upcoming</h3>
									</div>
								</div>
								<div class="ribbon-title ribbon-yellow ribbon-right clearfix">
									<div class="center">
										<h3 class="title">Festivals</h3>
									</div>
									<div class="right"></div>
								</div>
							</div>
							<div id="carousel-upcoming" class="carousel carousel-content slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<div class="upcoming-date">
											<span class="month">Jan 2014</span>
											<span class="date">26</span>
										</div>
										<div class="box-content">
											<h3>Sunburst Festival, Harilaya Farm</h3>
											<a href="#" class="btn btn-hollo">Book Now</a>
										</div>
									</div>

									<div class="item">
										<div class="upcoming-date">
											<span class="month">Jan 2014</span>
											<span class="date">26</span>
										</div>
										<div class="box-content">
											<h3>Sunburst Festival, Harilaya Farm</h3>
											<a href="#" class="btn btn-hollo">Book Now</a>
										</div>
									</div>

									<div class="item">
										<div class="upcoming-date">
											<span class="month">Jan 2014</span>
											<span class="date">26</span>
										</div>
										<div class="box-content">
											<h3>Sunburst Festival, Harilaya Farm</h3>
											<a href="#" class="btn btn-hollo">Book Now</a>
										</div>
									</div>
								</div>

								<ol class="carousel-indicators">
									<li data-target="#carousel-upcoming" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-upcoming" data-slide-to="1"></li>
									<li data-target="#carousel-upcoming" data-slide-to="2"></li>
								</ol>

								<!-- Controls -->
								<a class="left carousel-control" href="#carousel-upcoming" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right carousel-control" href="#carousel-upcoming" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="box stories">
							<div class="ribbon-title-container clearfix">
								<div class="ribbon-title ribbon-yellow ribbon-single clearfix">
									<div class="center">
										<h3 class="title">Stories</h3>
									</div>
									<div class="right"></div>
								</div>
							</div>
							<div id="carousel-stories" class="carousel carousel-content slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<div class="item-avatar">
											<img src="{{{ asset('assets/img/128.jpg') }}}" class="img-circle">
										</div>
										<div class="box-content">
											<h3>Doraemon &amp; Nobita Festival 2014</h3>
											<span>by <strong>Nobita</strong></span>
											<a href="#">View all stories</a>
										</div>
									</div>

									
								</div>

								<ol class="carousel-indicators">
									<li data-target="#carousel-stories" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-stories" data-slide-to="1"></li>
									<li data-target="#carousel-stories" data-slide-to="2"></li>
								</ol>

								<!-- Controls -->
								<a class="left carousel-control" href="#carousel-stories" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right carousel-control" href="#carousel-stories" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="box highlights">
							<div class="ribbon-title-container clearfix">
								<div class="ribbon-title ribbon-yellow ribbon-single clearfix">
									<div class="center">
										<h3 class="title">Highlights</h3>
									</div>
									<div class="right"></div>
								</div>
							</div>
							<div id="carousel-stories" class="carousel carousel-content slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<div class="item-avatar">
											<img src="{{{ asset('assets/img/128.jpg') }}}" class="img-circle">
										</div>
										<div class="box-content">
											<h3>Top Grounds Coffee in Asia </h3>
											<span>by <strong>Todd Carmichael</strong></span>
											<a href="#">View all highlights</a>
										</div>
									</div>

									
								</div>

								<ol class="carousel-indicators">
									<li data-target="#carousel-stories" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-stories" data-slide-to="1"></li>
									<li data-target="#carousel-stories" data-slide-to="2"></li>
								</ol>

								<!-- Controls -->
								<a class="left carousel-control" href="#carousel-stories" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right carousel-control" href="#carousel-stories" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

	<section id="site-slider">
		<div class="container">
			<div class="features-slider">
				<div class="row">
					<div class="col-md-12">
						<h2 class="slider-title text-center">Where to Go</h2>
						<div id="wheretogo" class="carousel slide" data-ride="carousel">
							<!-- Controls -->
							<a class="left carousel-control" href="#wheretogo" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right carousel-control" href="#wheretogo" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>

							<!-- Wrapper for slides -->
							<div class="carousel-inner">

								<div class="item active">
									<ul class="list-unstyled row thumbnails">
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
									</ul>
								</div>

							</div>

						</div>

						<a href="#" class="slider-action text-center">View all Places</a>

					</div>
				</div>
			</div>

			<div class="features-slider">
				<div class="row">
					<div class="col-md-12">
						<h2 class="slider-title text-center">What to Do</h2>
						<div id="wheretogo" class="carousel slide" data-ride="carousel">
							<!-- Controls -->
							<a class="left carousel-control" href="#wheretogo" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right carousel-control" href="#wheretogo" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>

							<!-- Wrapper for slides -->
							<div class="carousel-inner">

								<div class="item active">
									<ul class="list-unstyled row thumbnails">
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
									</ul>
								</div>

							</div>

						</div>

						<a href="#" class="slider-action text-center">View all Places</a>

					</div>
				</div>
			</div>

			<div class="features-slider">
				<div class="row">
					<div class="col-md-12">
						<h2 class="slider-title text-center">Who to See</h2>
						<div id="wheretogo" class="carousel slide" data-ride="carousel">
							<!-- Controls -->
							<a class="left carousel-control" href="#wheretogo" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right carousel-control" href="#wheretogo" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>

							<!-- Wrapper for slides -->
							<div class="carousel-inner">

								<div class="item active">
									<ul class="list-unstyled row thumbnails">
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
										<li class="col-md-4">
											<a href="#">
												<h3>Singapore</h3>
												<div class="img-container">
													<img src="http://upload.wikimedia.org/wikipedia/commons/d/da/European_Balloon_Festival-2008-_night_glow_and_fireworks_1.jpg" alt="Image">												
												</div>
												<span class="info-text">Sponsored by Tourism Singapore</span>
												<div class="carousel-caption">
													<h4>Subheader</h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tincidunt mi vitae feugiat varius. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
												</div>
											</a>
										</li>
									</ul>
								</div>

							</div>

						</div>

						<a href="#" class="slider-action text-center">View all Places</a>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="footer">
		<div class="row">
			<div class="col-md-12">
				<h2 class="footer-title text-center">Suss the scene out before you go.</h2>
			</div>
		</div>

		<div id="footer-bottom">
			<div class="row">
				<div class="col-md-4">
					logo
				</div>
				<div class="col-md-8">
					menu
				</div>
			</div>
		</div>
	</section>
</body>

</html>