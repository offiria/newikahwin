<h3>About</h3>
<p>
	{{ trans('vendor.footer-about') }}
</p>

<ul class="menu">
<li class=""><a href="{{ url('advertise') }}">Advertise with Us</a></li>
<li class=""><a href="{{ url('wedding-checklist') }}">Wedding Checklist</a></li>
<li class=""><a href="{{ url('register/vendor') }}">Vendor Signup</a></li>
<li class=""><a href="{{ url('inspiration') }}">Inspirasi Instagram</a></li>
<li class=""><a href="/editorchoice">Editor's Choice</a></li>
</ul>

<ul class="menu">
<li class=""><a href="{{ url('about') }}">About Us</a></li>
<li class=""><a href="{{ url('contact') }}">Contact</a></li>
<li class=""><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
<li class=""><a href="{{ url('faq') }}">FAQ</a></li>

@if (Auth::check()) 

                <li><a href="#">Logout</a></li>
          @else
<li>            <a href="{{ url('login/fb') }}">
            <span><i class="fa fa-facebook"></i></span>
            <span>Sign in</span>
            </a>
          </li>
          @endif

</ul>