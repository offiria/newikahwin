#!/bin/bash

composer dump-autoload
php artisan cache:clear
php artisan migrate:reset
php artisan migrate --package="toddish/verify"
php artisan migrate
php artisan db:seed
sudo chmod a+rwx vendor/ezyang/htmlpurifier/library/HTMLPurifier