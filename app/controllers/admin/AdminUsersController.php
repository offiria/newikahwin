<?php

use Gregwar\Image\Image;

class AdminUsersController extends BaseController {

	public function getIndex()
	{
		$user = Auth::user();

		// only admin can view list
		if( !User::find( $user->id)->is('administrator') ){
			return Redirect::to('admin')->with('message','Permission denied');
		}

		// Append pagination link with filter queries
		View::composer(Paginator::getViewName(), function($view) {
			$query = array_except( Input::query(), Paginator::getPageName() );
			$view->paginator->appends($query);
		});

		$query = User::query();
		
		$query->select(array('users.id', 'users.firstname', 'users.lastname', 'users.email', 'roles.name as rolename', 'users.verified', 'users.disabled', 'users.created_at'))
			->leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
			->leftjoin('roles', 'roles.id', '=', 'role_user.role_id')
			->groupBy('users.id');

		if(Input::has('keyword')) {
			$keyword = Input::get('keyword');
			$query->where('users.firstname', 'LIKE', '%' . $keyword . '%')
				->orWhere('users.lastname', 'LIKE', '%' . $keyword . '%')
				->orWhere('users.email', 'LIKE', '%' . $keyword . '%');
		}

		if(Input::has('role')) {
			$query->where('roles.id', '=', Input::get('role'));
		}

		if(Input::has('verified')) {
			$query->where('users.verified', '=', Input::get('verified'));
		}

		if(Input::has('disabled')) {
			$query->where('users.disabled', '=', Input::get('disabled'));
		}

		$users = $query->orderBy('users.updated_at', 'desc')->paginate(50);
		$roles = Role::all()->lists('name','id');

		return View::make('admin.users.index', ['users' => $users, 'roles' => $roles]);
	}

	public function getCreate()
	{
		$user = Auth::user();

		// only admin can view list
		if( !User::find( $user->id)->is('administrator') ){
			return Redirect::to('admin')->with('message','Permission denied');
		}

		$roles = Role::all()->lists('name','id');
		return View::make('admin/users/create-edit', ['roles' => $roles]);
	}

	public function getEdit($id)
	{
		$user = User::find($id);
		$my = Auth::user();

		// only admin can view list
		if( !User::find( $my->id)->is('administrator') ){
			if( $id != $my->id ){
				return Redirect::to('admin')->with('message','Permission denied');
			}
		}

		if ($user) {
			$roles = Role::all()->lists('name','id');
			return View::make('admin/users/create-edit', ['user' => $user, 'roles' => $roles]);
		} else {
			return Redirect::to('admin')->with('message', 'User does not exist');
		}
	}

	public function postEdit($id = null)
	{
		$rules = array(
				'firstname' => 'required',
				'email' => 'required|unique:users,email',
				'role' => 'required',
				'new_password' => 'required|confirmed'
			);

		if($id) {
			$rules = array_merge($rules, 
				array(
					'email' => 'required|unique:users,email,' . $id,
					'new_password' => ''
				));
		}

		// dd(Input::all());

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$user = User::FirstOrCreate(['id' => $id]);
			$inputs = Input::all();
			$inputs['description'] = strip_tags($inputs['description']);
			// $inputs['link'] = strip_tags($inputs['link']);
			
			$user->update($inputs);

			if(Input::has('role')) {
				$user->roles()->sync($inputs['role']);

				if(in_array( 2, $inputs['role'])){
					// add to vendor newsletter
					$ch = curl_init();
					ob_start();
					curl_setopt($ch, CURLOPT_URL, 'http://newsletter.bootstrapstyler.com/subscribe');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('name' => $user->firstname, 'email' => $user->email, 'list' => 'oZ0L1JNI8923SkF95zYGoJ3A')));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
					curl_exec($ch);
					curl_close($ch);
					ob_end_clean();

				}
			}

			if(Input::has('new_password')) {
				$user->password = Input::get('new_password');
			}

			if( ! $id) {
				// Verified by default
				$user->verified = 1;
			}

			if($user->save()) {
				
				if($id) {
					return Redirect::back()
						->with('message', 'User updated');
				} else {
					return Redirect::to('admin/users')
						->with('message', 'The user has been created');
				}
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

	public function getAjaxToggle($userId, $field, $value = null)
	{
		$allowedFields = ['verified', 'disabled'];
		$user = User::find($userId);

		if(in_array($field, $allowedFields) && $user) {

			switch ($field) {
				case 'verified':
					if( ! is_null($value) && $value == 'sendmail') {
						// Log::info('Verification e-mail sent to user');
					}
					break;
			}

			$user->$field = ($user->$field == 1) ? 0 : 1;
			$user->save();

			$result = ['success' => 'true', 'status' => $user->$field];
		} else {
			$result = ['success' => 'false'];
		}

		return Response::json($result);
	}

	public function postAvatar($id){
		$user = User::find($id);
		$uploader = new FileUploader();
		$uploaderPath = '/assets/profile_photos'. '/';

		$result = $uploader->handleUpload(public_path() . $uploaderPath);
		// print_r($result); exit;
		// rename file with proper extension
		$path_parts = pathinfo($result['filename']);
		rename($result['path'], $result['path']. '.'.$path_parts['extension']);
		$result['path'] 	= str_replace(public_path(), '', $result['path']. '.'.$path_parts['extension']);

		// resize image
		Image::open(public_path() . $result['path'])
		    ->zoomCrop(350, 350)
		    ->save(public_path().''.$result['path']);

		// delete old photo
		if(!strstr($user->image, 'default')){
	    	@unlink( public_path(). $user->image);
	    }

	    $user->image = $result['path'];
	    $user->save();

		$result['success'] = 1;
		$result['src'] = asset($user->image);

		// for front-end
		return Response::json($result );
	}

}