<?php

class Tag extends \Eloquent {
	protected $fillable = ['tag'];

    public function vendors()
    {
        return $this->morphedByMany('Vendor', 'taggable');
    }

    public function stories()
    {
        return $this->morphedByMany('Story', 'taggable');
    }
}