<?php

class UsersController extends BaseController {

	public function getVendorRegister( $role ='user' )
	{
		return $this->getRegister('vendor');
	}

	public function postVendorRegister( $role='user' )
	{
		return $this->postRegister('vendor');
	}

	public function getRegister( $role ='user' )
	{
		$title = "Register";
		if($role == 'vendor'){
			$title = "Register as trusted wedding Vendor";
		}

		return View::make('users.new', [ 'role' => $role ] )
			->with('title', $title);
	}

	public function postRegister( $role='user' )
	{
		$rules = array(
				'firstname' => 'required',
				'email' => 'required|email|unique:users',
				'password' => 'required|alpha_num|between:4,8|confirmed',
				'password_confirmation' => 'required|alpha_num|between:4,8'
			);

		$oAuth = Input::has('provider_uid');

		if($oAuth) {
			$rules = array_merge($rules, array('password' => '', 'password_confirmation' => '', 'provider' => 'required', 'provider_uid' => 'required'));
		}

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$verificationToken = hash('sha256', uniqid());

			// Create the user
			$user = new Toddish\Verify\Models\User;
			$user->email = Input::get('email');
			$user->password = Input::get('password');
			$user->firstname = Input::get('firstname');
			$user->lastname = Input::get('lastname');
			$user->phone = Input::get('phone');


			if($oAuth) {
				$user->provider = Input::get('provider');
				$user->provider_uid = Input::get('provider_uid');

				// Users don't need to verify
				if(Input::get('usertype') == 'user') {
					$user->verified = 1;
				}
			} else {
				$user->verified = 0;
				$user->verification_token = $verificationToken;
			}

			$user->save();

			// set all as 'user' role
			$roleUser = Toddish\Verify\Models\Role::where('name', '=', 'user')->get()->first();
			$user->roles()->sync([ $roleUser->id ]);

			// set all as 'user' role
			if($role == 'vendor'){
				$roleVendor = Toddish\Verify\Models\Role::where('name', '=', 'vendor')->get()->first();
				$user->roles()->sync([ $roleVendor->id ]);
			}

			if( ! $oAuth) {
				// Send verification e-mail
				Mail::send('emails.auth.validate', array( 'role' => $role, 'email' => urlencode($user->email), 'verificationToken' => $verificationToken, 'firstname' => Input::get('firstname')), function($message) use ($user) {
						$message->subject('Please verify your email address');
						$message->to($user->email);
					});
			}

			// automatically log vendor in
			if($role == 'vendor'){
				Auth::login($user);

				// and send an invite email
				Mail::send('emails.auth.vendor', array( 'firstname' => Input::get('firstname') ), function($message) use ($user) {
						$message->subject('Program Pengesahan Vendor iKahwin');
						$message->to($user->email);
					});

				// add to newsletter
				$ch = curl_init();
				ob_start();
				curl_setopt($ch, CURLOPT_URL, 'http://newsletter.bootstrapstyler.com/subscribe');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('name' => $user->firstname, 'email' => $user->email, 'list' => 'oZ0L1JNI8923SkF95zYGoJ3A')));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
				curl_exec($ch);
				curl_close($ch);
				ob_end_clean();

			}

			return View::make('users.new-complete', ['title' => 'Registration Complete', 'user' => $user, 'role' => $role]);

		} else {

			if($role='vendor')
				return Redirect::to('register/vendor')
					->withInput()
					->withErrors($validation);
			else
				return Redirect::to('register')
					->withInput()
					->withErrors($validation);
		}
	}

	public function getValidate($email, $token)
	{
		$user = User::where('email', '=', $email)->first();

		if($user->verification_token == $token) {
			// Activate the user and clear the validation key
			$user->verified = 1;
			$user->verification_token = '';
			$user->save();

			return Redirect::route('home')
				->with('message', 'Your account has been validated');
		} else {
			return Redirect::route('home')
				->with('message', 'We are unable to verify your account');
		}
	}

	public function getLogin($redirect = null)
	{
		return View::make('users.login')
			->with('title', 'Login')
			->with('redirect', $redirect); 
	}

	public function postLogin()
	{
		$rules = array(
				'email' => 'required',
				'password' => 'required'
			);

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {

			$user = array('email' => Input::get('email'), 'password' => Input::get('password'));

			try {
				Auth::attempt($user);

				if($redirect = Input::get('redirect')) {
					return Redirect::to($redirect);
				} elseif ($redirect = Session::get('redirect')) {
					return Redirect::to($redirect);
				} else {
					// Redirect to homepage
					return Redirect::route('home')->with('message', 'You are logged in!');
				}
			} catch (Exception $e) {
				// Reflash the session data in case we are in the middle of a redirect 
				Session::reflash('redirect');

				return Redirect::route('login')
					->with('message', $e->getMessage())
					->withInput();
			}
		} else {
			return Redirect::to('login')
				->withInput()
				->withErrors($validation);
		}
	}

	public function getLogout()
	{
		if (Auth::check()) {
			Auth::logout();
			return Redirect::route('home')->with('message', 'You have been logged out');
		} else {
			return Redirect::route('home');
		}
	}

	public function getRemind()
	{
		return View::make('users.remind')
			->with('title', 'Recover Password');
	}

	public function postRequest()
	{
		$rules = array(
				'email' => 'required|email'
			);

		$validation = Validator::make(Input::all(), $rules);
			
		if ($validation->passes()) {
			try {
			    $response = Password::remind(Input::only('email'), function($message) {
			    				$message->subject('Your password recovery request');
			    			});
			} catch (Toddish\Verify\UserNotFoundException $e) {
			    $response = Password::INVALID_USER;
			}

			switch ($response)
			{
				case Password::INVALID_USER:
				case Password::REMINDER_SENT:
					return Redirect::back()->with('message', Lang::get($response));
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}


	public function getSelectPackages()
	{

		return View::make('users.packages');
	}

	public function getReset($token)
	{
		if (is_null($token)) { 
			App::abort(404);
		}

		return View::make('users.reset')
			->with('token', $token)
			->with('title', 'Reset Password');
	}

	public function postUpdate()
	{
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password) {
				$user->password = $password;
				$user->save();
			});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('message', Lang::get($response));

			case Password::PASSWORD_RESET:
				return Redirect::to('/');
		}
	}

	public function getLoginFB()
	{
		$facebook = new Facebook(Config::get('facebook'));
		$params = array(
			'redirect_uri' => url('/login/facebook/callback'),
			'scope' => 'email',
		);
		return Redirect::to($facebook->getLoginUrl($params));
	}

	public function getLoginFBCallback()
	{
		$code = Input::get('code');
		if (strlen($code) == 0) return Redirect::to('/')->with('message', 'There was an error communicating with Facebook');

		$facebook = new Facebook(Config::get('facebook'));
		$uid = $facebook->getUser();

		if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');

		$me = $facebook->api('/me');

		$user = User::whereUid($uid)->first();
		
		if (empty($user)) {
			$user = new User;
			$user->firstname = $me['first_name'];
			$user->lastname = $me['last_name'];
			$user->email = $me['email'];
			$user->image = 'https://graph.facebook.com/' . $me['id'] . '/picture?type=large';
			$user->uid = $uid;
			$user->verified = 1;
		}

		$user->access_token = $facebook->getAccessToken();
		$user->save();

		Auth::login($user);

		return Redirect::to('/')->with('message', 'Logged in with Facebook');
	}
}