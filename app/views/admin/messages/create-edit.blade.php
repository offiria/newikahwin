@extends('admin.layouts.default')

@section('content')

<h2 class="admin-title">{{ isset($user) ? 'Edit' : 'New' }} User</h2>

<div class="row admin-row">

	<div class="col-md-9">
		@if(isset($user))
		{{ Form::model($user, array('url' => 'admin/users/' . $user->id . '/edit', 'method' => 'post', 'class' => 'form-lm clearfix')) }}
		@else
		{{ Form::open(array('url' => 'admin/users/create', 'method' => 'post', 'class' => 'form-lm clearfix', 'name' => 'entryForm')) }}
		@endif

		<div class="row">
		<div class="col-md-6">
			<div class="form-group {{ Form::errorClass('firstname') }}">
			{{ Form::label('firstname', 'First Name') }}
			{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('firstname') }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group {{ Form::errorClass('lastname') }}">
			{{ Form::label('lastname', 'Last Name') }}
			{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('lastname') }}
			</div>
		</div>
		</div><!--  end row -->

		<div class="row">
		<div class="col-md-6">
			<div class="form-group  {{ Form::errorClass('email') }}">
			{{ Form::label('email', 'E-Mail') }}
			{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('email') }}
			</div>
		</div>

		</div><!-- end row -->

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label for="">About Me</label>
					{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control nowysiwyg', 'maxlength' =>'158', 'rows' => '3')) }}
				</div>
			</div>
		</div>

		{{--
		<div class="row">
			<div class="col-md-6">
				<div class="form-group {{ Form::errorClass('link') }}">
				{{ Form::label('link', 'Link') }}
				{{ Form::text('link', Input::old('link'), array('class' => 'form-control')) }}
				{{ Form::errorMsg('link') }}
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group {{ Form::errorClass('company') }}">
				{{ Form::label('company', 'Company Name') }}
				{{ Form::text('company', Input::old('company'), array('class' => 'form-control')) }}
				{{ Form::errorMsg('company') }}
				</div>
			</div>
		</div>
		--}}

		<fieldset>
			<legend>Password</legend>

			<div class="form-group {{ Form::errorClass('new_password') }}">
				{{ Form::label('new_password', 'New Password', array('class' => 'control-label')) }}
				{{ Form::password('new_password', array('class'=>'form-control')) }}
				{{ Form::errorMsg('new_password') }}
			</div>

			<div class="form-group {{ Form::errorClass('new_password_confirmation') }}">
				{{ Form::label('new_password_confirmation', 'Confirm New Password', array('class' => 'control-label')) }}
				{{ Form::password('new_password_confirmation', array('class'=>'form-control')) }}
				{{ Form::errorMsg('new_password_confirmation') }}
			</div>
		</fieldset>

		@if(Auth::user()->allowManageUser())
		<fieldset>
			<legend>Roles</legend>

			<div class="form-group {{ Form::errorClass('role') }}">
				<?php
				$roles = Toddish\Verify\Models\Role::all();
				$userRoles = (isset($user)) ? array_fetch($user->roles->toArray(), 'id') : array();
				?>
				@foreach ($roles as $role)
					<label>{{ Form::checkbox('role[]', $role->id, in_array($role->id, $userRoles)) }} {{ $role->name }}</label>
				@endforeach
				{{ Form::errorMsg('role') }}
			</div>
		</fieldset>
		@endif

		{{ Form::submit(((isset($user)) ? 'Update' : 'New'), array('class' => 'btn btn-primary btn-lg')) }}
		{{ Form::close() }}
	</div>

	<div class="col-md-3 profile-sidebar">

		@if(isset($user))
		<div class="thumbnail">
			<img src="{{ asset($user->image)}}" alt="" id="profile-pic">
		</div>
		<div id="profile-pic-error" class="alert alert-danger" style="display:none"></div>
		<div id="avatar-fine-uploader" class="attachment" data-action="{{ url('/admin/users/'.$user->id. '/upload') }}"></div>
		@else
		<div class="alert alert-warning" role="alert">You can add/edit profile image after you save this user</div>
		@endif
		
	</div>
</div>

@stop