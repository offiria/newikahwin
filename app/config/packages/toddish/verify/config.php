<?php
return array(

    'identified_by' => array('email'),

    // The Super Admin role
    // (returns true for all permissions)
    'super_admin' => 'Super Admin',

    // DB prefix for tables
    'prefix' => ''

);
