<?php

use Gregwar\Image\Image;

class ProfileController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	
	|
	*/

	public function getAccount()
	{
		$user = Auth::user();
		return View::make('profile.account', ['user' => $user]);
	}

	public function postAccount()
	{
		$rules = array(
				'firstname' => 'required',
				'email' => 'required|unique:users,email,' . Auth::user()->id
			);

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$user = Auth::user();
			$inputs = Input::all();
			$user->update($inputs);

			if($user->save()) {
				return Redirect::back()
					->with('message', 'Profile updated');
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

	// for lawyers additional info only
	public function additioanlInformation()
	{
		$user = User::find(Auth::user()->id);

		$rules = array(
                
            );

        $validation = Validator::make(Input::all(), $rules);

        // validate
        if ($validation->passes()) {

			$updated = false;

			$inputs = Input::all();
			if(!empty($inputs)){
				$user->update($inputs);
				$user->save();
			}

			// update data
			if (Input::has('areas'))
			{
				// print_r($user); exit;
			    LawyerAreasUser::where('user_id', '=', $user->id)->delete();
			    $data = Input::get('areas');
			    foreach ($data as $value) {
			    	LawyerAreasUser::create(['lawyer_area_id'=> $value, 'user_id'=>$user->id]);
			    }
			    $update = true;
			}

			if (Input::has('specialisations'))
			{
			    LawyerSpecialisationsUser::where('user_id', '=', $user->id)->delete();
			    $data = Input::get('specialisations');
			    foreach ($data as $value) {
			    	LawyerSpecialisationsUser::create(['lawyer_specialisation_id'=> $value, 'user_id'=>$user->id]);
			    }
			    $updated = true;
			}

			$areas = LawyerArea::all();
			$specialisations = LawyerSpecialisation::all();
			$myAreas = $user->areas->lists('lawyer_area_id');
			$myspecialisations = $user->specialisations->lists('lawyer_specialisation_id');

			if($updated){
				Session::flash('message', 'Profile data updated!');
			}

			return View::make('profile.additional-information', ['user' => $user , 
				'areas' => $areas, 
				'myareas' => $myAreas , 
				'myspecialisations' => $myspecialisations, 
				'specialisations'=>$specialisations]);
		}
	}

	public function savAdditioanlInformation()
	{
		$user = User::find(Auth::user()->id);
		$areas = LawyerArea::all();
		$specialisations = LawyerSpecialisation::all();

		return View::make('profile.additional-information', ['user' => $user , 'areas' => $areas, 'specialisations'=>$specialisations]);
	}

	// Change password
	public function getChangePassword()
	{
		$user = Auth::user();

		return View::make('profile.password', ['user' => $user ]);
	}

	public function postChangePassword()
	{
		$rules = array(
				'password' => 'required|passcheck',
				'new_password' => 'required|alpha_num|between:4,8|confirmed',
				'new_password_confirmation' => 'required'
			);

		 $messages = array(
				'passcheck' => 'Your old password was incorrect',
			);

		$user = User::findOrFail(Auth::user()->id);

		Validator::extend('passcheck', function($attributes, $value, $parameters) use ($user) {
			if(Hash::check($user->salt . $value, $user->password)) {
				return true;
			} else {
				return false;
			}
		});

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->passes()) {

			$user->password = Input::get('new_password');
			$user->save();

			return Redirect::to('profile/password')->with('message', 'Your password has been changed');
		} else {
			return Redirect::to('profile/password')->withErrors($validator)->withInput();
		}
	}

	// Change plans
	public function plans()
	{
		$user = Auth::user();
		
		return View::make('profile.plans', ['user' => $user ]);
	}

	/* Change current user profile picture */
	public function uploadProfile(){
		
		$user = User::find(Auth::user()->id);

		$uploader = new FileUploader();
		$uploaderPath = '/assets/profile_pics'. '/';
		$result = $uploader->handleUpload(public_path() . $uploaderPath);

		$path_parts = pathinfo($result['path']);
		$old_parts = pathinfo( $uploader->file->getName());

		// need to fix the file extension
		rename($result['path'], $result['path'] . '.' .  $old_parts['extension']);
		$result['path'] = $result['path'] . '.' .  $old_parts['extension'];

		// resize the image
		Image::open($result['path'])
		    ->zoomCrop(240, 240)
		    ->save($result['path']);

		// delete old picture
		if(file_exists(public_path() . $user->image)){
			@unlink(public_path() . $user->image );
		}

		$user->image = '/assets/profile_pics'. '/'. $path_parts['filename'] . '.' .  $old_parts['extension'];
		$user->save();

		$result['src'] = asset($user->getProfilePictureURL());

		return Response::json($result );
	}
}
