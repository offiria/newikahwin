<?php

return array(
	'title' => "Find <span>AWESOME & RELIABLE</span> wedding vendors in Malaysia",
	'subtitle' => "We've done all the hard work to find the most awesome, reliable wedding vendor in Malaysia.",

	'title_featured' => 'Our featured vendor this week',
	'title_popular_categories' => 'Popular Categories',
	'title_our_blog' => 'Our Blog',

	'title_vendor_list' => 'Complete list of wedding vendors in Malaysia.',
	'subtitle_vendor' => "We've compiled, curate and select some of the best wedding vendors around.",

	'title_this_week' => 'This Week',
	'in-around' => 'In or around',

	'contact_vendor' => 'Hubungi vendor ini',

	'category-makeup-artist' => 'Makeup Artist',
	'category-wedding-card' => 'Wedding Card',
	'category-videographer' => 'Videographer',
	'category-pa-system' => 'PA System',
	'category-marriage-course-center' => 'Marriage Course Center',
	'category-health-beauty' => 'Health & Beauty',
	'category-decoration' => 'Decoration',
	'category-wedding-cake' => 'Wedding Cake',
	'category-wedding-gift' => 'Wedding Gift',
	'category-photographer' => 'Photographer',
	'category-catering' => 'Catering',
	'category-designer' => 'Designer',
	'category-wedding-planner' => 'Wedding Planner',
	'category-bridal' => 'Bridal',
	'category-venue' => 'Venue',
	'category-canopy' => 'Canopy',
	'category-event-organizer' => 'Event Organizer',
	'category-honeymoon' => 'Honeymoon',
	'category-specialties' => 'Specialties',


	'register-name' => 'First Name',
	'register-email' => 'Email',
	'register-phone' => 'Mobile Number (SMS/Whatsapp)',
	'register-password' => 'Password',
	'register-password-confirm' => 'Confirm Password',
	'register-complate' => 'Registration Complete',
	'register-complate-note' => 'Thank you for you registering. Please check your e-mail to activate your account.',


	'footer-about' => 'iKahwin is a one-stop wedding portal for all your wedding needs.',
	'footer-products' => 'Check out our awesome, custom-made cards and wedding stamps.',
	'footer-newsletter' => 'Get fresh wedding ideas and latest promotion from our vendors, delivered to your inbox',

	'other-vendors' => 'Check out these other Vendors',

	'blog-food-drinks' => 'Food & Drinks', 
	'blog-travel-destination' => 'Travel & Destination', 
	'blog-decor-details' => 'Decor & Details', 
	'blog-fashion-style' =>'Fashion/Style', 
	'blog-diy' =>'DIY',
	'blog-advice' => 'Advice', 
	'blog-real-wedding' =>'Real Wedding', 
	'blog-deals' =>'Deals',
	'blog-by' => 'By',
	'blog-stories' => 'Stories'
	);