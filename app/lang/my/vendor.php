<?php

return array(
	'title' => "Vendor perkahwinan yang <span>TERBAIK</span> & boleh <span>DIPERCAYAI</span>",
	'subtitle' => "We've done all the hard work to find the most awesome, reliable wedding vendor in Malaysia.",

	'title_featured' => 'Vendor pilihan minggu ini',
	'title_popular_categories' => 'Kategori Popular',
	'title_our_blog' => 'Blog Kami',

	'title_vendor_list' => 'Senarai lengkap vendor perkahwinan di Malaysia.',
	'subtitle_vendor' => "Kami telah mencari and memilih vendor perkahwinan terbaik di kawasan anda.",

	'title_this_week' => 'Minggu ini',
	'in-around' => 'di negeri',

	'contact_vendor' => 'Hubungi vendor ini',

	'category-makeup-artist' => 'Mak Andam',
	'category-wedding-card' => 'Kad Kahwin',
	'category-videographer' => 'Juruvideo',
	'category-pa-system' => 'PA System',
	'category-marriage-course-center' => 'Kursus Perkahwinan',
	'category-health-beauty' => 'Spa & Kecantikan',
	'category-decoration' => 'Hiasan',
	'category-wedding-cake' => 'Kek Kahwin',
	'category-wedding-gift' => 'Hadiah',
	'category-photographer' => 'Jurufoto',
	'category-catering' => 'Catering',
	'category-designer' => 'Designer',
	'category-wedding-planner' => 'Perancang Perkahwinan',
	'category-bridal' => 'Pelamin',
	'category-venue' => 'Lokasi Kahwin',
	'category-canopy' => 'Canopy',
	'category-event-organizer' => 'Event Organizer',
	'category-honeymoon' => 'Berbulan Madu',
	'category-specialties' => 'Lain-lain',


	'register-name' => 'Nama Penuh',
	'register-email' => 'Email',
	'register-phone' => 'No Telefon Bimbit (Untuk SMS/Whatsapp)',
	'register-password' => 'Kata laluan',
	'register-password-confirm' => 'Taip semula kata laluan',
	'register-complate' => 'Pendaftaran Selesai',
	'register-complate-note' => 'Thank you for you registering. Please check your e-mail to activate your account.',


	'footer-about' => 'Portal perkahwinan No 1 di Malaysia.',
	'footer-products' => 'Produk-produk menarik oleh iKahwin',
	'footer-newsletter' => 'Dapatkan idea terkini serta promosi hebat melalui email setiap hari',

	'other-vendors' => 'Vendor-vendor lain yang anda mungkin berminat',

	'blog-food-drinks' => 'Makanan & Minuman', 
	'blog-travel-destination' => 'Destinasi', 
	'blog-decor-details' => 'Hiasan', 
	'blog-fashion-style' =>'Fashion/Style', 
	'blog-diy' =>'DIY',
	'blog-advice' => 'Nasihat', 
	'blog-real-wedding' =>'Real Wedding', 
	'blog-deals' =>'Jualan',
	'blog-by' => 'Oleh',
	'blog-stories' => 'Cerita'
	);