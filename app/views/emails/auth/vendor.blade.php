<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>iKahwin</title>

  
  <style type="text/css">
    .full {width:100% !important;}
    @media only screen and (max-width: 480px) {
      .flexible {
        width:100% !important;
        margin-left:0px !important;
        margin-right:0px !important;
        border-top-left-radius: 0px !important;
        border-top-right-radius: 0px !important;
        border-bottom-left-radius: 0px !important;
        border-bottom-right-radius: 0px !important;
      }
      .logo {
        padding-left: 12% !important;
        width: 150px !important;
        height: 33px !important;
      }
      .footer {
        padding-left: 7% !important;
        padding-right: 7% !important;
      }
      h1 {
        font-size:21px !important;
      }
      h2 {
        font-size:17px !important;
      }
      h3 {
        font-size:17px !important;
      }
      h4 {
        font-size:14px !important;
      }
      p {
        font-size:14px !important;
      }
      .contact-info p {
        font-size:12px !important;
      }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background: #edeff0;width:100% !important;-webkit-text-size-adjust:none;margin:0;padding:0;">
  <center>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background:#edeff0;font-family:Helvetica,sans-serif;height:100% !important;margin:0;padding:0;width:100% !important;font-size:14px;color:#9ba6b0;">
      <tr>
        <td align="center" style="vertical-align:top;padding-bottom:15px;border-collapse:collapse;">
          <!-- // BEGIN CONTAINER -->
          <table class="flexible" border="0" cellpadding="0" cellspacing="0" style="width:90%;max-width:600px;margin-right:5%;margin-left:5%;display:block;">
            <tr>
              <td align="center" style="vertical-align:top;border-collapse:collapse;">
                <!-- // BEGIN PREHEADER -->
                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                  <tr>
                    <td align="left" width="192" style="width:192px;vertical-align:top;padding-top:21px;padding-bottom:21px;border-collapse:collapse;">
                      <a href="http://www.ikahwin.my"><img class="logo"  style="border:0;line-height:100%;outline:none;text-decoration:none;" src="http://new.ikahwin.my/assets/img/ikahwin-logo.png"></a>
                    </td>
                    <td>
                      &nbsp;&nbsp;
                    </td>
                  </tr>
                </table>
                <!-- END PREHEADER \\ -->
              </td>
            </tr>

            <tr>
    <td align="center" style="vertical-align:top;border-collapse:collapse;">
        <!-- // BEGIN HEADER -->
        <table class="flexible" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFF;border-top-left-radius:5px;border-top-right-radius: 5px;">
            <tr>
                <td align="center" style="vertical-align:top;border-collapse:collapse;padding-top:3%;padding-right:7%;padding-left:7%;">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;font-size:18px;font-weight:bold;line-height:100%;padding-bottom:25px;text-align:left;border-collapse:collapse;">
                                <h1 style="font-family:Helvetica,sans-serif;color:#384047;display:block;font-size:24px;font-weight:bold;line-height:130%;letter-spacing:normal;margin-right:0;margin-top:15px;margin-bottom:15px;margin-left:0;text-align:left;">
                                    Hi {{$firstname}}, selamat datang ke iKahwin.my
                                </h1>
                                <h2 style="font-family:Helvetica,sans-serif;color:#8d9aa5;display:block;font-size:18px;font-weight:normal;line-height:150%;letter-spacing:normal;margin-right:0;margin-top:15px;margin-bottom:10px;margin-left:0;text-align:left;">
                                    Kami sangat berbesar hati kerana anda sudi mengiklankan perkhidmatan anda menerusi laman web kami, <a style="font-family:Helvetica,sans-serif;font-weight:bold;color:#657380;text-decoration:none;" href="http://new.ikahwin.my">iKahwin.my</a>. 
                                </h2>
                                <h2 style="font-family:Helvetica,sans-serif;color:#8d9aa5;display:block;font-size:18px;font-weight:normal;line-height:150%;letter-spacing:normal;margin-right:0;margin-top:15px;margin-bottom:10px;margin-left:0;text-align:left;">
                                    Misi kami adalah untuk membantu anda memasarkan produk dan perkhidmatan ada kepada bakal-bakal pengantin secara "online" dengan lebih berkesan.
                                </h2>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- END HEADER \\ -->
    </td>
</tr>
<tr>
    <td align="center" style="vertical-align:top;">
        <!-- // BEGIN BODY -->
        <table class="flexible" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%;background-color:#FFF;border-bottom-left-radius:5px;border-bottom-right-radius:5px;">
            <!-- GENERIC TEXT BLOCK -->
            <tr>
                <td align="center" style="vertical-align:top;border-collapse:collapse;padding-left:7%;padding-right:7%;padding-bottom:3%;">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;color:#8d9aa5;font-size:14px;line-height:150%;text-align:left;border-top: 1px solid #e2e5e8;padding-top:20px;padding-bottom:10px;border-collapse:collapse;">

                            	<h4 style="font-family:Helvetica,sans-serif;color:#384047;display:block;font-size:16px;font-weight:bold;line-height:130%;letter-spacing:normal;margin-right:0;margin-left:0;margin-top:15px;margin-bottom:15px;text-align:left;">
                                    Pengesan Vendor Perkahwinan
                                </h4>

                                <p style="font-family:Helvetica,sans-serif;line-height:160%;margin-top:13px;margin-bottom:15px;">Seperti yang ada sedia maklum, ada terdapat vendor-vendor yang kurang ber-etika dan ini menyusahkan bakal-bakal pengantin and memburukan industri perkahwinin. Untuk kemudaha bakal pengantin, pihak iKahwin memerlukan setiap vendor disahkan sebagai peniaga yang sebenar.</p>

                                <h4 style="font-family:Helvetica,sans-serif;color:#384047;display:block;font-size:16px;font-weight:bold;line-height:130%;letter-spacing:normal;margin-right:0;margin-left:0;margin-top:15px;margin-bottom:15px;text-align:left;">
                                    Bagaimana prosess pengesaha ini?
                                </h4>

                                <p style="font-family:Helvetica,sans-serif;line-height:160%;margin-top:13px;margin-bottom:15px;">Anda cuma perlu menghantarkan salinan dokumen berikut kepada kami melalui email, admin@ikahwin.my atau WhatsApp, 016-2386155</p>

                                <pre style="font-family:Helvetica,sans-serif;color:#8d9aa5;padding-top:15px;padding-right:20px;padding-bottom:13px;padding-left:20px;background:#f3f5f6;font-size:14px;white-space:normal;line-height:160%;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:5px;border-bottom-right-radius:5px;">
                                  1. Borang pendaftaran perniagaan
                                </pre>
                                
                                <p style="font-family:Helvetica,sans-serif;line-height:160%;margin-top:13px;margin-bottom:15px;">dan</p>

                                <pre style="font-family:Helvetica,sans-serif;color:#8d9aa5;padding-top:15px;padding-right:20px;padding-bottom:13px;padding-left:20px;background:#f3f5f6;font-size:14px;white-space:normal;line-height:160%;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:5px;border-bottom-right-radius:5px;">
                                  2. Salinan kad pengenalan (hadapan sahaja)
                                </pre>

                                <h4 style="font-family:Helvetica,sans-serif;color:#384047;display:block;font-size:16px;font-weight:bold;line-height:130%;letter-spacing:normal;margin-right:0;margin-left:0;margin-top:35px;margin-bottom:15px;text-align:left;">
                                    Pertanyaan?
                                </h4>

                                <p style="font-family:Helvetica,sans-serif;line-height:160%;margin-top:15px;margin-bottom:15px;">Jika anda mempunyai sebarang pertanya, anda boleh menghubungi kali melalui email <a style="font-family:Helvetica,sans-serif;font-weight:bold;color:#657380;text-decoration:none;" href="mailto:admin@ikahwin.my">admin@ikahwin.my</a> ataupun telefon kami di nombor 016-6162009.</p>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- END BODY \\ -->
    </td>
</tr>


            <tr>
              <td align="left" style="vertical-align:top;border-collapse:collapse;">
                <!-- // BEGIN FOOTER -->
                <table class="footer" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                  <tr>
                    <td class="contact-info" style="text-align:center;vertical-align:top;padding-top:25px;border-collapse:collapse;">
                      <p style="font-family:Helvetica,sans-serif;line-height:160%;font-family:Helvetica,sans-serif;color:#b7c0c7;font-size:12px;margin-top:0;margin-bottom:0;">
                      &copy; Aushlab Sdn Bhd
                      </p>
                      <p style="font-family:Helvetica,sans-serif;line-height:160%;color:#b7c0c7;font-size:12px;margin-top:0;margin-bottom:0;">
                      <a href="mailto:admin@ikahwin.my" style="font-weight:bold;text-decoration:none;color:#9ba6b0;">admin@ikahwin.my</a>&nbsp;&nbsp;<span style="color:#b7c0c7;">|</span>
                      </p>
                    </td>
                  </tr>
                </table>
                <table class="footer" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                  <tr>
                    <td style="vertical-align:top;text-align:center;padding-top:30px;padding-bottom:5px;border-collapse:collapse;">
                      
                    </td>
                  </tr>
                </table>
                <!-- END FOOTER \\ -->
              </td>
            </tr>
          </table>
          <!-- END CONTAINER \\ -->
        </td>
      </tr>
    </table>
  </center>

<img src="http://email.teamtreehouse.com/wf/open?upn=xyShGiEZyea-2F7HRuD4XEnCMZgKxHT3WGY-2BeV4uVRtmrzAi1p0GWHIVVYxptDNqfMPfAQmSg5FSIYr2A-2FywfUeB4bnxUruNyJ-2Bue26tiVV2vbkrQvh-2F-2BSfwNdLKI0NxQj-2BNBxs4JNwgGS0Np68i-2FPSis-2F61m80sqxKrQI6E6ckoRgEOWDHlYvheFY5kuzqJCzcggvP4h7y1xFIN44drtoPQ-3D-3D" alt="" width="1" height="1" border="0" style="height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;"/>
</body>
</html>