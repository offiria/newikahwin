<?php

class Report extends \Eloquent {
	protected $fillable = ['user_id', 'vendor_id', 'reason', 'message', 'ip'];
}