@extends('layouts.default')
@section('title', "iKahwin")
@section('content')


<section id="site-hero">	
	<div class="container">
		<div class="col-md-7">
			<h1>{{ trans('vendor.title') }}</h1>
			<p>{{ trans('vendor.subtitle') }}<p>	
		</div>
		<div class="col-md-5">
			<div class="hide" style="margin-top:30px;border-radius:10px">
				<img src="{{ asset('assets/img/feature.jpg') }}" style="max-width:100%"/>
			</div>
		</div>

		<div class="clearfix"></div>
			<div class="search-wrapper"><!-- start search wrapper -->
			
			<div class="row"><!-- start search row -->
								<div class="col-md-5 col-sm-5 col-xs-12">
								{{ Form::open( array('url' => 'vendors', 'method' => 'get')) }}	
								<div class="form-group">
									<label for="" class="sr-only"></label>
									<?php $genre = Input::get('category'); ?>
									
									<div class="input-group" data-toggle="dropdown">
										<input type="text" class="form-control" id="" placeholder="Vendor" name="category" value="{{{ isset( $genre ) ? Genre::currentName() : '' }}}">
										<span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
									</div>
									<ul class="dropdown-menu" role="menu" aria-labelledby="genreDropdown">
										<?php
										$genres = Genre::all();
										$genres->sort( function($a, $b){ 
											return strcasecmp( trans('vendor.category-'.$a->slug), trans('vendor.category-'.$b->slug)); 
										}); 
										?>
										@foreach( $genres as $g)
										<?php
										$httpQuery = ['category' => $g->slug];
										if(Input::get('state')){
											$httpQuery['state'] = Input::get('state');
										}
										?>
									    <li><a role="menuitem" tabindex="-1" href="{{ url('vendors?') . http_build_query( $httpQuery ) }}">{{trans('vendor.category-'.$g->slug)}}</a></li>
									    @endforeach
									  </ul>
								</div>
								{{Form::close()}}
								</div>
								<div class="col-md-2 col-sm-2 text-separator">
									<span>{{ trans('vendor.in-around') }}</span>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-12">
								{{ Form::open( array('url' => 'vendors', 'method' => 'get')) }}	
								<div class="form-group">
									<div class="input-group" data-toggle="dropdown">
										<input type="text"    class="form-control" id="" placeholder="State" name="state" value="{{{ Input::get('state') }}}">
										<span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
									</div>
									<ul class="dropdown-menu" role="menu" aria-labelledby="contryDropdown">
										<?php
										$states = ['Johor', 'Kedah', 'Kelantan', 'Kuala Lumpur', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Perak',  'Perlis', 'Pulau Pinang', 'Putrajaya', 'Sabah', 'Sarawak', 'Selangor', 'Terengganu', 'Wilayah Persekutuan - Labuan'];
										?>
										@foreach( $states as $c)
										<?php
										$httpQuery = [];
										if(Input::get('category')){
											$httpQuery['category'] = Input::get('category');
										}
										$httpQuery['state'] = $c;
										?>
									    <li><a role="menuitem" tabindex="-1" href="{{ url('vendors?') . http_build_query( $httpQuery ) }}">{{ $c }}</a></li>
									    @endforeach
									  </ul>
								</div>
								{{Form::close()}}
								</div>

						

							</div><!-- end search-row -->
						</div><!-- end search-wrapper -->
	</div>


</section>

<section id="site-featured">	
	<div class="container">

		<div class="features-slider">
			<h2 class="slider-title text-center">{{ trans('vendor.title_featured') }}</h2>

		<div id="carousel-example-generic" class="carousel slide " data-ride="carousel">
  		<!-- Indicators -->
		<ol class="carousel-indicators hide">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<?php
			$hasActive = false;
			?>
			@foreach ($featured as $feature) 
				<div class="item @if(!$hasActive) active @endif">
					<img src="{{ asset( $feature->cover_img ) }}" >
					<div class="carousel-caption">
						<h4><a href="{{ URL::to('vendors/'. $feature->genres->last()->slug .'/' . $feature->slug) }}">{{{ $feature->name }}}</a></h4>
					</div>
				</div>
				<?php $hasActive = true; ?>
			@endforeach

		</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
	</div>

</section>

<section id="site-slider">
	<div class="container">

		<div class="features-slider genre-slider">
			
			<h2 class="slider-title text-center">{{ trans('vendor.title_popular_categories') }}</h2>
			
			<?php 
			$popCats = [
			'videographer',
			'makeup-artist',
			'bridal',
			
			'wedding-planner',
			'catering',
			'wedding-gift',

			'wedding-cake',
			'photographer',
			'wedding-card',
			
			'honeymoon',
			'health-beauty',
			'venue',
			/*
			'pa-system',
			'marriage-course-center',
			'decoration',			
			'designer',
			'canopy',
			'event-organizer',
			'specialties'
			*/
			];
			?>

			<div class="row popular-category">
				@foreach ($popCats as $cat) 
				<?php
				$category = Genre::findSlug($cat)->first();
				?>
				<div class="col-md-4 popular-category-item-container ">
					<div class="popular-category-item popular-category-{{ $cat }}">
						<a href="{{ url('top/'.$cat) }}"><h4>{{ trans('vendor.category-'.$category->slug) }}</h4></a>
					</div>
				</div>
				@endforeach
			</div>
		</div>


	</div>
</section>

<section id="site-stories">
	<div class="container">
		<div class="genre-slider">
		<h2 class="slider-title text-center">{{ trans('vendor.title_our_blog') }}</h2>

		<div class="row">
			<div class="col-md-4">
				<h4>{{ trans('vendor.title_this_week') }}</h4>
				<div class="story-more">
				@foreach ($popularStories as $s) 
					<div class="col-md-12" style="border-bottom: 1px solid #eee; padding:8px 0px">
						<img src="{{ asset( $s->cover_thumb ) }}" class="img-responsive" style="float: left;padding-right:4px;max-width: 40%;">
						<div class="box-content" style="float: left;width: 60%;padding-left: 6px;font-size: 16px;">
							<h3><a href="{{ URL::to('blog/' . $s->slug) }}">{{{ $s->title }}}</a></h3>
						</div>
						<div class="clearfix"></div>
					</div>

				@endforeach
				</div>
			</div>
			

			<div class="col-md-4">
				<h4>Popular</h4>
				<div class="story-more">
				@foreach ($latestStories as $s) 
					<div class="col-md-12" style="border-bottom: 1px solid #eee; padding:8px 0px">
						<img src="{{ asset( $s->cover_thumb ) }}" class="img-responsive" style="float: left;padding-right:4px;max-width: 40%;">
						<div class="box-content" style="float: left;width: 60%;padding-left: 6px;font-size: 16px;">
							<h3><a href="{{ URL::to('blog/' . $s->slug) }}">{{{ $s->title }}}</a></h3>
						</div>
						<div class="clearfix"></div>
					</div>

				@endforeach
				</div>
			</div>

			<div class="col-md-4">
				
				<h4>Pilihan Kami</h4>
				<div class="story-more">
				@foreach ($featuredStories as $s) 
					<div class="col-md-12" style="border-bottom: 1px solid #eee; padding:8px 0px">
						<img src="{{ asset( $s->cover_thumb ) }}" class="img-responsive" style="float: left;padding-right:4px;max-width: 40%;">
						<div class="box-content" style="float: left;width: 60%;padding-left: 6px;font-size: 16px;">
							<h3><a href="{{ URL::to('blog/' . $s->slug) }}">{{{ $s->title }}}</a></h3>
						</div>
						<div class="clearfix"></div>
					</div>

				@endforeach
				</div>
			
			</div>
		</div><!-- End row -->
	</div><!-- End slider -->
	</div>
</section>

<section id="as-seen">
	<div class="container" style="padding-top:30px">
		<center>
<a href="/" class="beveled-media-box rect">
      <img width="69px" height="70px" alt="" class="" src="http://ikahwin.my/PHOTO/ntv7.jpg">

    </a><a href="http://goo.gl/CGncSR" class="beveled-media-box rect">
      <img width="159px" height="56px" alt="" class="" src="http://ikahwin.my/PHOTO/harianmetro.jpg">

    </a>
<a href="http://goo.gl/pBLBk3" class="beveled-media-box rect">
      <img width="109" height="57px" alt="" class="" src="http://ikahwin.my/PHOTO/thestar.jpg">

    </a>
<a href="http://goo.gl/64RiZs" class="beveled-media-box rect">
      <img width="167px" height="51px" alt="" class="" src="http://ikahwin.my/PHOTO/kosmo.jpg">

    </a>
<a href="/" class="beveled-media-box rect">
      <img width="139px" height="63px" alt="" class="" src="http://ikahwin.my/PHOTO/makethepitch.jpg">

    </a>
<a href="http://goo.gl/ufQKXF" class="beveled-media-box rect">
      <img width="117px" height="61px" alt="" class="" src="http://ikahwin.my/PHOTO/startuphub.jpg">

    </a></center>
	</div>
</section>
@stop