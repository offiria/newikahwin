<?php

return array(
	'company-name' => 'Company Name',
	'excerpt' => 'Short description',
	'phone-primary' => 'Primary phone number',
	'email' => 'Email Address',
,	'coverage' => 'Coverage'
	'work-hours' => 'Working Hours',
	'contact-info' => 'Additional Contact info',

	'street' => 'Street Address',
	'city' => 'District/City',

	'album-gallery' => 'Album/Gallery',
	'packages' => 'Packages',
	'deals' => 'Special Offers'
	);