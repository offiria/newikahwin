<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

    	// Roles
		$roleAdmin = new Toddish\Verify\Models\Role;
		$roleAdmin->name = 'administrator';
		$roleAdmin->level = 9;
		$roleAdmin->save();

		$roleOwner = new Toddish\Verify\Models\Role;
		$roleOwner->name = 'vendor';
		$roleOwner->level = 8;
		$roleOwner->save();

		$roleContributor = new Toddish\Verify\Models\Role;
		$roleContributor->name = 'contributor';
		$roleContributor->level = 7;
		$roleContributor->save();

		$roleUser = new Toddish\Verify\Models\Role;
		$roleUser->name = 'user';
		$roleUser->level = 6;
		$roleUser->save();

		// Admin
		$userAdmin = new Toddish\Verify\Models\User;
		$userAdmin->username = 'admin';
		$userAdmin->email = 'admin@ikahwin.my';
		$userAdmin->password = 'password'; // This is automatically salted and encrypted
		$userAdmin->verified = 1;
		$userAdmin->firstname = 'Admin';
		$userAdmin->save();
		$userAdmin->roles()->sync(array($roleAdmin->id, $roleOwner->id, $roleContributor->id ));

		// owner
		$userOwner = new Toddish\Verify\Models\User;
		$userOwner->username = 'Owner';
		$userOwner->email = 'owner@ikahwin.my';
		$userOwner->password = 'password'; // This is automatically salted and encrypted
		$userOwner->verified = 1;
		$userOwner->firstname = 'Owner';
		$userOwner->save();
		$userOwner->roles()->sync(array($roleOwner->id));

		$csvData = file_get_contents( app_path() . '/database/seeds/user.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$userArray = array();
		foreach ($lines as $line) {
		    $adata = str_getcsv($line);
		    $userArray[ $adata[0] ] = $adata;
		}

		foreach ($userArray as $u) {
			$user = new Toddish\Verify\Models\User;
			$user->username = isset($u[4]) ? $u[4] : '';
			$user->email = isset($u[7]) ? $u[7] : '';
			$user->password = 'password';
			$user->verified = 1;
			$user->firstname = isset($u[3]) ? $u[3] : '';
			$user->lastname = '';
			$user->save();

			$user->roles()->sync(array($roleUser->id));
		}

		// contributor
		$userContributor = new Toddish\Verify\Models\User;
		$userContributor->username = 'Contributor';
		$userContributor->email = 'contributor@ikahwin.my';
		$userContributor->password = 'password'; // This is automatically salted and encrypted
		$userContributor->verified = 1;
		$userContributor->firstname = 'Contributor';
		$userContributor->save();
		$userContributor->roles()->sync(array($roleContributor->id));

		// Users
		$user = new Toddish\Verify\Models\User;
		$user->username = 'user';
		$user->email = 'user@ikahwin.my';
		$user->password = 'password';
		$user->verified = 1;
		$user->firstname = 'User';
		$user->save();
		$user->roles()->sync(array($roleUser->id));

		for ($i=0; $i < 100; $i++) { 
			$user = new Toddish\Verify\Models\User;
			$user->username = $faker->userName();
			$user->email = Config::get('app.emailprefix'). '+'. $user->username . Config::get('app.emaildomain');
			$user->password = 'password';
			$user->verified = 1;
			$user->firstname = $faker->firstName();
			$user->lastname = $faker->lastName();
			$user->save();

			$user->roles()->sync(array($roleUser->id));
		}
	}

}