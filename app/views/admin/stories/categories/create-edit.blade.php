@extends('admin.layouts.default')

@section('content')

<h1>{{ isset($category) ? 'Edit' : 'New' }} Story</h1>
<hr>

{{ Form::token() }}

<div class="row">
	<div class="">
		@if(isset($category))
		{{ Form::model($category, array('url' => 'admin/stories/categories/' . $category->id . '/edit', 'method' => 'post', 'class' => 'form-lm clearfix')) }}
		@else
		{{ Form::open(array('url' => 'admin/stories/categories/create', 'method' => 'post', 'class' => 'form-lm clearfix')) }}
		@endif

		<div class="">
			<div class="form-group {{ Form::errorClass('name') }}">
			{{ Form::label('name', 'Category Name') }}
			{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('name') }}
			</div>
		</div>

		<div class="">
			<div class="form-group {{ Form::errorClass('description') }}">
			{{ Form::label('description', 'Description') }}
			{{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
			{{ Form::errorMsg('description') }}
			</div>
		</div>

		{{ Form::submit(((isset($category)) ? 'Update' : 'New'), array('class' => 'btn btn-primary btn-lg')) }}
	</div>
</div>

@stop