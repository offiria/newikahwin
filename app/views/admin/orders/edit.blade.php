@extends('admin.layouts.default')

@section('content')

<div class="row">
	<div class="col-md-8">
	<h2 class="admin-title">Order #{{ $order->id }}. ( {{ $order->created_at->diffForHumans() }} ) </h2>
	</div>
</div>

<div class="row">
	{{ Form::model($order, array('url' => 'admin/orders/' . $order->id . '/edit', 'method' => 'post', 'class' => 'form-lm clearfix', 'id' => 'main-form')) }}
	<div class="form-group ">
		
		<div class="row">
			<div class="col-md-8">
				<div class="form-group {{ Form::errorClass('name') }}">
					{{ Form::label('name', 'Customer Name') }}
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
					{{ Form::errorMsg('name') }}
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group {{ Form::errorClass('data') }}">
					{{ Form::label('data', 'Order Details') }}
					{{ Form::textarea('data', Input::old('data'), array('class' => 'form-control input-lg')) }}
					{{ Form::errorMsg('data') }}
				</div>

			</div>

			<div class="col-md-4">
				<div class="form-group {{ Form::errorClass('address') }}">
					{{ Form::label('address', 'Address') }}
					{{ Form::textarea('address', Input::old('address'), array('class' => 'form-control input-lg')) }}
					{{ Form::errorMsg('address') }}
				</div>

			</div>

		</div>
	</div>

	<hr/>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group {{ Form::errorClass('status') }}">
				{{ Form::label('status', 'Status') }}
				{{ Form::select('status', array(
					'0' => 'Pending', 
					Order::STATUS_FAIL => 'Failed', 
					Order::STATUS_SUCCESS => 'Completed',
					Order::STATUS_APPROVAL => 'Pending Approved',
					Order::STATUS_APPROVED => 'Design Approved',
					Order::STATUS_CONSIGNMENT => 'Consignment',
					Order::STATUS_DELIVERED => 'Delivered'), Input::old('status'), array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group {{ Form::errorClass('payment_method') }}">
				{{ Form::label('payment_method', 'Payment Method') }}
				{{ Form::select('published', ['iPay88', 'Cash', 'Cimb', 'Maybank', 'PayPal'], Input::old('payment_method'), array('class' => 'form-control')) }}
				{{ Form::errorMsg('payment_method') }}

			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group {{ Form::errorClass('tracking_code') }}">
				{{ Form::label('tracking_code', 'Tracking Code') }}
				{{ Form::text('tracking_code', Input::old('tracking_code'), array('class' => 'form-control')) }}
				{{ Form::errorMsg('tracking_code') }}
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group {{ Form::errorClass('total') }}">
				{{ Form::label('total', 'Total Amount') }}
				{{ Form::text('total', Input::old('total'), array('class' => 'form-control')) }}
				{{ Form::errorMsg('total') }}
			</div>
		</div>
	
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group {{ Form::errorClass('note') }}">
				{{ Form::label('note', 'Note') }}
				{{ Form::textarea('note', Input::old('note'), array('class' => 'form-control input-lg')) }}
				{{ Form::errorMsg('note') }}
			</div>
		</div>
		
	</div>
	{{ Form::submit(((isset($order)) ? 'Update' : 'New'), array('class' => 'btn btn-primary btn-lg')) }}
	{{ Form::close() }}
</div>

@stop