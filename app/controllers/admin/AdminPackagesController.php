<?php

class AdminPackagesController extends BaseController {

	public function getCreateModal($festivalId)
	{
		return View::make('admin.packages.create-edit-modal', ['festivalId' => $festivalId]);
	}

	public function getEditModal($festivalId, $packageId)
	{
		$package = Package::find($packageId);

		return View::make('admin.packages.create-edit-modal', ['festivalId' => $festivalId, 'package' => $package]);
	}

	public function postStore($festivalId)
	{
		$packageId = Input::get('packageid');

		$package = Package::findOrNew($packageId);
		$package->name = Input::get('name');
		$package->description = Input::get('description');
		$package->excerpt = Input::get('excerpt');
		$package->price = Input::get('price');
		$package->festival_id = $festivalId;
		$package->save();

		return Redirect::to('admin/vendors/' . $festivalId . '/edit#tab-package')
				->with('message', 'Package ' . (($packageId) ? 'updated' : 'created'));
	}

	public function getDelete($festivalId, $packageId)
	{
		Package::destroy($packageId);

		return Redirect::to('admin/vendors/' . $festivalId . '/edit#tab-package')
			->with('message', 'Package deleted');
	}
}
