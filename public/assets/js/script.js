$(document).ready(function() {
	var thumbWidth = $('.album-container li').width();
	jQuery('.thumb-container').nailthumb({width:thumbWidth,height:thumbWidth});

	// setup drop down for date selection
	$('.input-group.date').datepicker({
		minViewMode: 1,
		calendarWeeks: true,
		autoclose: true,
		format: 'MM yyyy'
		
	}).on('changeDate', function(ev){
		if (ev.date.valueOf()){
			dateSearch.submit();
		}
	});
	
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});

	// set up gallery
	$('.gallery').each(function() { 
		$(this).magnificPopup({
			delegate: 'a',
			type:'image',
			gallery:{enabled:true}
		});
	});

	// Update/add query string parameters
	function updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			return uri + separator + key + "=" + value;
		}
	}

	// Sort filtered data
	$('.dropdown.listing-sortby li a').click(function(e){
		e.preventDefault();
		window.location.search = updateQueryStringParameter(window.location.search, 'sortby', $(this).data('sortby'));
 	});

	// Order form fields ending with * are required
 	function endsWith(str, suffix) {
 		return str.indexOf(suffix, str.length - suffix.length) !== -1;
 	}

 	$('#orderForm .control-label').each(function(i, obj) {
 		if(endsWith($(obj).text(), '*')) {
 			// Add new validation
 			$('#orderForm').bootstrapValidator('addField', $(obj).closest('.form-group').find(':input').attr('name'), {
 				validators: {
 					notEmpty: {
 						message: 'The field is required'
 				   }
 			   }
 			});
 		}
 	});



 	// Likes
 	$('button#like').on( 'click',  function() {
		$.ajax({ 
			url: $(this).data('ajax'),
			type: 'POST',
			context: this
		})
		.success(function(data) {
			$(this).data('ajax', data.ajax);

			if($(this).hasClass('btn-unlike')){
				$(this).removeClass('btn-unlike');
				$(this).addClass('btn-like');
			} else {
				$(this).removeClass('btn-like');
				$(this).addClass('btn-unlike');
			}
			$(this).blur();
		})
		.fail(function() {
		    alert( "Please login first" );
		  });
 	});

 	
});