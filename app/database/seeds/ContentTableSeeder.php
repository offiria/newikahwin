<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ContentTableSeeder extends Seeder {

	function stripAttributes($html,$attribs) {
		return preg_replace("/<([a-z][a-z0-9]*)(?:[^>]*(\ssrc=['\"][^'\"]*['\"]))?[^>]*?(\/?)>/i",'<$1$2$3>', $html);

	    $dom = new DOMDocument;                 // init new DOMDocument
		$dom->loadHTML($html);                  // load HTML into it
		$xpath = new DOMXPath($dom);            // create a new XPath
		$nodes = $xpath->query('//*[@style]');  // Find elements with a style attribute
		foreach ($nodes as $node) {              // Iterate over found elements
		    $node->removeAttribute('style');    // Remove style attribute
		}
		return $dom->saveHTML(); 
	}

	public function run()
	{
		$faker = Faker::create();

		// Create tags
		foreach(range(1, 20) as $index)
		{
			Tag::create(['name' => $faker->word()]);
		}

		// create pricing
		$pricings = [ '10' => 'Budget',  '20' => 'Premium', '30' => 'Luxury'];
		$pricingModel = [];
		foreach ($pricings as $key=> $value) {
			$pricingModel[] = Pricing::create([
				'id' => $key,
				'name' => $value
				]);
		}

		$states = ['Johor', 'Kedah', 'Kelantan', 'Kuala Lumpur', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Perak',  'Perlis', 'Pulau Pinang', 'Putrajaya', 'Sabah', 'Sarawak', 'Selangor', 'Terengganu', 'Wilayah Persekutuan - Labuan'];

		$vendorCategories = [ 110 =>'Bridal', 112 =>'Canopy', 107 =>'Catering', 103 => 'Decoration', 108=> 'Designer', 113 => 'Event Organizer', 102=> 'Health & Beauty', 114=>'Honeymoon', 97=>'Makeup Artist', 101=> 'Marriage Course Center', 100=>'PA System', 106=>'Photographer', 115=>'Specialties', 111=>'Venue', 99=>'Videographer', 104=>'Wedding Cake', 98=>'Wedding Card', 105=> 'Wedding Gift', 109 =>'Wedding Planner'];

		$csvData = file_get_contents( app_path() . '/database/seeds/vendors.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$array = array();
		foreach ($lines as $line) {
		    $array[] = str_getcsv($line);
		}

		$csvData = file_get_contents( app_path() . '/database/seeds/location.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$locationArray = array();
		foreach ($lines as $line) {
		    $adata = str_getcsv($line);
		    $locationArray[ $adata[0] ] = $adata;
		}

		$csvData = file_get_contents( app_path() . '/database/seeds/job.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$jobArray = array();
		foreach ($lines as $line) {
		    $adata = str_getcsv($line);
		    $jobArray[ $adata[0] ] = $adata;
		}

		$csvData = file_get_contents( app_path() . '/database/seeds/product_attr.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$attrArray = array();
		foreach ($lines as $line) {
		    $adata = str_getcsv($line);
		    $attrArray[ $adata[0] ] = $adata;
		}

		$csvData = file_get_contents( app_path() . '/database/seeds/description.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$descArray = array();
		foreach ($lines as $line) {
			$adata = str_getcsv($line);
		    $descArray[$adata[0]] = str_getcsv($line);
		}

		$csvData = file_get_contents( app_path() . '/database/seeds/meta.csv');
		$lines = preg_split("/\\r\\n|\\r|\\n/", $csvData);
		$metaArrayContact = array();
		$metaArrayOpening = array();
		foreach ($lines as $line) {
			$adata = str_getcsv($line);
			if(isset($adata[1])){
				if($adata[1] == 7)
			    	$metaArrayOpening[$adata[0]] = str_getcsv($line);

			    if($adata[1] == 9)
			    	$metaArrayContact[$adata[0]] = str_getcsv($line);
			}
		}

		foreach( $vendorCategories as $index => $vc)
		{
			// Genres
			$genre = Genre::create([
				'id' => $index,
				'name' => $vc,
				'description' => $faker->paragraph(rand(5,20)),
				'cover_img' => 'assets/cover/genres/default.jpg'
			]);

			// $tidy = new tidy();
			// $tidyConfig = array (
			//             'indent' => true,
			//             'show-body-only' => true,
			//             'clean' => true,
			//             'output-xhtml' => false,
			//             'preserve-entities' => false 
			//     );

			// Festivals
			foreach($array as $liveData)
			{
				if($liveData[1] == $index ) {
					$locationData = isset( $locationArray[ $liveData[0] ] ) ? $locationArray[ $liveData[0] ] : ["","","","","", ""];
					$jobData = isset( $jobArray[ $liveData[0] ] ) ? $jobArray[ $liveData[0] ] : ["","","","","", ""];
					$descData = isset( $descArray[ $liveData[0] ] ) ? $descArray[ $liveData[0] ] : ["","","","","", ""];
					$oldId = isset($liveData[0]) ? $liveData[0]: 0;
					$attrData = isset( $attrArray[ $liveData[0] ] ) ? $attrArray[ $liveData[0] ] : ["","","","","", ""];
					$metaContact = isset( $metaArrayContact[ $liveData[0] ] ) ? $metaArrayContact[ $liveData[0] ] : ["","","","","", ""];
					$metaOpening = isset( $metaArrayOpening[ $liveData[0] ] ) ? $metaArrayOpening[ $liveData[0] ] : ["","","","","", ""];

					$vendor = Vendor::create([
						'id' => $liveData[0],
						'old_id' => $oldId,
						'name' => $liveData[2],
						'description' => $this->stripAttributes( $descData[3], ['style'] ), //Purifier::clean($descData[3]),
						'excerpt' => $faker->sentence(rand(5,12)),
						'featured' => ($index == 1) ? 1 : 0,
						'coverage' => $jobData[1],
						'contact_info' => isset($metaContact[2]) ?  $metaContact[2] : '', //isset($jobData[5])? $jobData[5] : '',
						'work_hours' =>  isset($metaOpening[2]) ?  $metaOpening[2] : '', 
						'phone' => isset($jobData[6]) ? $jobData[6] : '',
						'district' => $faker->city(),
						'state' => $locationData[2],
						'address' => $locationData[1],
						'status' => 1,
						'email' => $liveData[3],
						'country_id' => 107,
						'announcement_date' => '2014-07-01',
						'user_id' => 1,
						'street' => $faker->streetName(),
						'latitude' => $locationData[4],
						'longitude' => $locationData[5],
						'facebook' => $attrData[1],
						'website' => $attrData[2],

					]);



					$vendor->genres()->save($genre);
					$vendor->pricings()->save($pricingModel[0]);
					$vendor->pricings()->save($pricingModel[1]);

					foreach(range(1, 5) as $index3)
					{
						//Taggable::create(['tag_id' => rand(1, 20), 'taggable_type' => 'Vendor', 'taggable_id' => $vendor->id]);
					}

					// Create default album
					$album = Album::create([
						'vendor_id' => $vendor->id,
						'name' => 'Album'
						]);

					$resources = ImportResource::where('fk_i_item_id', '=', $vendor->id)->get();
					foreach ($resources as $res) {
						Photo::create([
							'album_id' => $album->id,
							'imageable_id' => $vendor->id,
							'imageable_type' => 'Vendor',
							'path' => $res->s_path .$res->pk_i_id .'.' . $res->s_extension
							]);	
					}

					// // Packages
					// foreach(range(1, 2) as $index4)
					// {
					// 	$package = Package::create([
					// 		'festival_id' => 0,
					// 		'name' => $faker->sentence(rand(1,2)),
					// 		'excerpt' => $faker->sentence(rand(1,3)),
					// 		'description' => $faker->paragraph(rand(20,50)),
					// 		'price' => $faker->randomNumber(2)
					// 	]);

					// 	$vendor->packages()->save($package);
					// }
				}
			}
		}

		

		$storyCategories = ['Food & Drinks', 'Travel & Destination', 'Decor & Details', 'Fashion/Style', 'DIY', 'Advice', 'Real Wedding', 'Deals'];
		foreach($storyCategories as $c)
		{
			// Story Category
			$category = StoryCategory::create([
				'name' => $c,
				'description' => $faker->paragraph(rand(5,20))
			]);

			// Stories
			if($category->id == 4 ){
				$importContents = ImportContent::where('catid', '=', 8)->get();
				foreach( $importContents as $data )
				{
					
					$content = Purifier::clean($data->introtext . $data->fulltext);
					$content = str_replace('src="images', 'src="/images', $content);

						$story = Story::create([
							'user_id' => rand(1,5),
							'festival_id' => rand(1,50),
							'title' => $data->title,
							'old_id' => $data->id,
							'slug' => $data->id .'-'. $data->alias,
							'story' => $content,
							'excerpt' => $faker->sentence(rand(40, 60)),
							'featured' => (in_array($index, array(1, 2, 3))) ? 1 : 0
						]);

						$story->category()->associate($category)->save();

						// foreach(range(1, 5) as $index)
						// {
						// 	Taggable::create(['tag_id' => rand(1, 20), 'taggable_type' => 'Story', 'taggable_id' => $story->id]);
						// }
					
				}
			}
		}
	}

}