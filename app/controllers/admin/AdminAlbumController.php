<?php

class AdminAlbumController extends BaseController {

	public function getCreateModal($vendorId)
	{
		return View::make('admin.albums.create-edit-modal', ['festivalId' => $vendorId]);
	}

	public function getEditModal($vendorId, $packageId)
	{
		$package = Album::find($packageId);

		return View::make('admin.albums.create-edit-modal', ['festivalId' => $vendorId, 'package' => $package]);
	}

	public function postStore($vendorId)
	{
		$packageId = Input::get('packageid');

		$package = Album::findOrNew($packageId);
		$package->name = Input::get('name');
		$package->description = Input::get('description');
		$package->vendor_id = $vendorId;
		$package->save();

		return Redirect::to('admin/vendors/' . $vendorId . '/edit#tab-albums')
				->with('message', 'Album ' . (($packageId) ? 'updated' : 'created'));
	}

	public function getDelete($vendorId, $packageId)
	{
		Album::destroy($packageId);

		return Redirect::to('admin/vendors/' . $vendorId . '/edit#tab-package')
			->with('message', 'Album deleted');
	}
}
