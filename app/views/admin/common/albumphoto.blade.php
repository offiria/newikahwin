<li id="photo-{{$photo->id}}" class="col-md-3 col-sm-6"><div class="photo"><img src="{{ asset($photo->path)}}" style="width:100%"/> 
	<textarea name="photocaption[{{$photo->id}}]"  maxlength="50" class="form-control nowysiwyg" placeholder="add caption for this photo" style="border-right: none;border-left: none;border-top: none;">{{ $photo->caption }}</textarea><span class=""><a href="#deletePhoto" data-ajax="{{ url('/admin/photoDelete/'.$photo->id) }}">Delete</a></span>
</div>
</li>