<?php

class VendorsController extends BaseController {

	public function getIndex( $category = '' )
	{

		$searchName = Input::get('name');
		$searchLocation = Input::get('location');
		$searchCountry = Input::get('country');
		$searchDate = Input::get('date');
		$searchGenre = !empty($category)? $category : Input::get('category');
		$searchTag = Input::get('tag');
		$searchSortBy = Input::get('sortby');
		$searchState = Input::get('state');
		$showFeatured = true; // should we show the main 3 articles?

		$query = Vendor::query();
		$queryB = null;
		$queryString= [];

		// page title
		$pageTitle = trans('vendor.title_vendor_list');

		// @TODO: Add filters instead of replacing them?

		

		// search by location
		if(!empty($searchLocation)){
			$query = Vendor::searchLocation($searchLocation);
		}

		// search by location
		if(!empty($searchCountry)){
			$query = Vendor::searchCountry($searchCountry);
		}


		// search by location
		if(!empty($searchState)){
			$query = $query->searchState($searchState);
			$queryString[] = "state=".$searchState;
			$pageTitle = "Wedding vendors in $searchState, Malaysia";
		}

		// search by name
		if(!empty($searchName)){
			$query = $query->searchNameAndTag($searchName);
			$pageTitle = "Search result for \"$searchName\"";

			if(!empty($searchState)){
				$pageTitle .= " in $searchState, Malaysia";
			}
		}

		// search by genre
		if(!empty($searchGenre)){
			$query = $query->searchGenre($searchGenre);
			$queryString[] = "category=".$searchGenre;
			$category = Genre::findSlug($searchGenre)->first();

			// if we cannot find it, user might type some shit in the category section
			// assume they are doing word search
			if(empty($category)){
				$searchName = $searchGenre;
				$query = Vendor::searchNameAndTag($searchName);
				$pageTitle = "Search result for \"$searchName\"";
			} else {
				$pageTitle = "Senarai lengkap vendor ({$category->name}) di Malaysia";
			}

		}

		// search by tag
		if(!empty($searchTag)){
			$query = Vendor::searchWithTag($searchTag);
			$pageTitle = "Senarai lengkap vendor perkahwinan \"$searchTag\" di Malaysia";
		}

		// if( ! empty($searchSortBy)) {
		// 	switch ($searchSortBy) {
		// 		case 'title':
		// 			$query->orderBy('vendors.name', 'ASC');
		// 			break;
		// 		case 'country':
		// 			$query->select(array('vendors.*'))
		// 				->leftjoin('countries', 'vendors.country_id', '=', 'countries.id')
		// 				->orderBy('countries.name', 'ASC');
		// 			break;
		// 	}
		// } else {
		// 	$query->orderBy('vendors.updated_at', 'DESC');
		// }

		// skip unpublished
		$query->published();

		// get featured, which is the premium vendor from current collection
		$featuredCount = Config::get('app.vendors_featured');
		$featuredQuery = clone $query;
		$premium = $featuredQuery->premium()->get()->slice(0, $featuredCount);
		$featuredId = $premium->lists('id');
		$featured = $premium;

		// if not enough premium, select preferred
		if( count($featuredId) < $featuredCount){
			$featuredQuery = clone $query;
			$verified = $featuredQuery->verified()->get()->slice(0, count($featuredId));
			$featuredId = array_merge($featuredId, $verified->lists('id'));
			$featured = $premium->merge($verified);
		}

		// get the rest of the vendors and exclude the featured ones
		if( count($featuredId ) > 0)
			$vendors = $query->whereNotIn('id', $featuredId )->paginate(21);
		else
			$vendors = $query->paginate(21);


		// other info
		$uniqueCountries = Vendor::distinct()->get();

		// get all categories
		$categories = Genre::all();

		$countries = [];
		foreach ($uniqueCountries as $f) {
			$countries[] = $f->country;
		}

		$genre = Genre::all();
		

		// we need to remember where the search if from (should we use cookie?)
		if( !empty($queryString) ){
			$queryString = '?'. implode('&', $queryString);
		} else {
			$queryString = '';
		}

		// hide featured section on pagination
		if( Input::get('page') && Input::get('page') != 1){
			$showFeatured = false;
		}


		return View::make('vendors.index', ['queryString' => $queryString, 
			'categories' => $categories, 'featured' => $featured,  'vendors' => $vendors, 
			'countries' => $countries, 'genre' => $genre, 'sortBy' => $searchSortBy,
			'pageTitle' => $pageTitle, 'showFeatured' => $showFeatured]);
	}


	public function getIndexTag( $searchTag = '' )
	{
		 $searchTag = urldecode( $searchTag);
		$searchSortBy = Input::get('sortby');
		$showFeatured = true; // should we show the main 3 articles?

		$query = Vendor::query();
		$queryB = null;
		$queryString= [];

		// page title
		$pageTitle = trans('vendor.title_vendor_list');

	
		// search by tag
		if(!empty($searchTag)){
			$query = Vendor::searchWithTag($searchTag);
			$pageTitle = "Senarai lengkap vendor perkahwinan \"$searchTag\" di Malaysia";
		}

		$query->published();

		// get featured, which is the premium vendor from current collection
		$featuredCount = Config::get('app.vendors_featured');
		$featuredQuery = clone $query;
		$premium = $featuredQuery->premium()->get()->slice(0, $featuredCount);
		$featuredId = $premium->lists('id');
		$featured = $premium;

		// if not enough premium, select preferred
		if( count($featuredId) < $featuredCount){
			$featuredQuery = clone $query;
			$verified = $featuredQuery->verified()->get()->slice(0, count($featuredId));
			$featuredId = array_merge($featuredId, $verified->lists('id'));
			$featured = $premium->merge($verified);
		}

		// get the rest of the vendors and exclude the featured ones
		if( count($featuredId ) > 0)
			$vendors = $query->whereNotIn('id', $featuredId )->paginate(21);
		else
			$vendors = $query->paginate(21);


		// other info
		$uniqueCountries = Vendor::distinct()->get();

		// get all categories
		$categories = Genre::all();

		$countries = [];
		foreach ($uniqueCountries as $f) {
			$countries[] = $f->country;
		}

		$genre = Genre::all();
		

		// we need to remember where the search if from (should we use cookie?)
		if( !empty($queryString) ){
			$queryString = '?'. implode('&', $queryString);
		} else {
			$queryString = '';
		}

		// hide featured section on pagination
		if( Input::get('page') && Input::get('page') != 1){
			$showFeatured = false;
		}


		return View::make('vendors.index', ['queryString' => $queryString, 
			'categories' => $categories, 'featured' => $featured,  'vendors' => $vendors, 
			'countries' => $countries, 'genre' => $genre, 'sortBy' => $searchSortBy,
			'pageTitle' => $pageTitle, 'showFeatured' => $showFeatured]);
	}

	public function getCountriesIndex()
	{
		$countries = Country::select(array('countries.*', DB::raw('COUNT(*) festivalcount')))
			->join('vendors', 'festivals.country_id', '=', 'countries.id')
			->groupBy('vendors.country_id')
			->orderBy('festivalcount', 'desc')
			->paginate(15);

		return View::make('countries.index', ['countries' => $countries]);
	}

	public function getRedirect($id){
		$vendor = Vendor::find($id);
		return Redirect::to('vendors/'. $vendor->genres->first()->slug . '/' . $vendor->slug, 301); 
	}

	public function getGenresIndex()
	{
		$genres = Genre::hasFestival()->orderBy('name', 'DESC')->paginate(15);
		// filter our genre with no festival
		foreach ($genres as $genre) {
			if( $genre->festivals()->published()->count() == 0 ){
				unset($genre);
			}
		}
		return View::make('genres.index', ['genres' => $genres]);
	}

	public function getTopIndex($category)
	{
		$category = Genre::findSlug($category)->first();
		
		$featured = $category->vendors()->featured()->orderBy('subscription', 'DESC')->get();

		return View::make('vendors.index', ['featured' => $featured, 'pageTitle' => "Our top vendors in \"{$category->name}\" " , 
			'vendors' => [], 'queryString' => '',  'showFeatured' => true]);
	}

	public function getView($cat, $slug, $old_id =0)
	{
		$user = Auth::user();
		$searchGenre = Input::get('category');
		$searchState = Input::get('state');

		if($old_id != 0 ) {
			$vendor = Vendor::where('old_id', '=', $old_id)->first();
			return Redirect::to('vendors/'. $vendor->genres->first()->slug . '/' . $vendor->slug, 301); 
		} else	
			$vendor = Vendor::where('slug', '=', $slug)->first();

		if(empty( $vendor ))
			App::abort(404);

		$vendor->hits ++;
		$vendor->save();

		// other related vendors (up to 4)
		// based on similar genre
		// $genre = $vendor->genre;
		$othervendors = Vendor::findGenre( $vendor->genres->random(1)->id )->otherThan($vendor->id)->get();
		$max = ( $othervendors->count() >= 3 ) ?  3 : $othervendors->count();
		
		if($othervendors->count() > 0 ){
			$othervendors = $othervendors->random($max);
		}

		// we need to remember where the search if from (should we use cookie?)
		$queryString = [];
		// search by location
		if(!empty($searchState)){
			$queryString[] = "state=".$searchState;
		}

		// search by genre
		if(!empty($searchGenre)){
			$queryString[] = "category=".$searchGenre;
		}
		if( !empty($queryString) ){
			$queryString = '?'. implode('&', $queryString);
		} else {
			$queryString = '';
		}

		// like
		$liked = false;
		if( isset($user) )
			$liked = Like::where( 'user_id', '=', $user->id)->where('likeable_type', '=', 'Vendor')->where('likeable_id', '=', $vendor->id)->count();

		return View::make('vendors.view', ['queryString' => $queryString, 'vendor' => $vendor, 'otherVendors' => $othervendors, 'liked' => $liked]);
	}
	
	public function getCreate()
	{
		return View::make('vendors.new');
	}

	public function postCreate()
	{
		$rules = array(
				'name' => 'required|min:5|max:70',
				'website' => 'required|url'
			);

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$festival = new Festival();
			$festival->name = Input::get('name');
			$festival->website = Input::get('website');
			$festival->save();

			return Redirect::to('festivals/' . $festival->slug)
					->with('message', 'Your festival has been submitted!');
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}

	/**
	 * 
	 */
	public function postSms($cat, $slug){

		$rules = [
			'name' => 'required',	
			'message' => 'required',
			'mobile' => 'required',
			'recaptcha_response_field' => 'required|recaptcha',
		];

		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {

			$vendor = Vendor::where('slug', '=', $slug)->first();
			$message = Input::get('message');
			$fromName = Input::get('name');
			$fromMobile = Input::get('mobile');

			TextMessage::create(['name' => $fromName, 'mobile' => $fromMobile, 'vendor_id' => $vendor->id, 'message' => $message]);

			$sms = new NexmoMessage();

			// clean phone number
			$phoneNo = trim($vendor->phone);
			$phoneNo = str_replace('+6', '', $phoneNo);
			$phoneNo = str_replace('-', '', $phoneNo);
			$phoneNo = str_replace(' ', '', $phoneNo);
			$phoneNo = "+6" . $phoneNo;

	    	$sms->sendText( $phoneNo, '+601117225017', "Your iKahwin listing enquiry:\n\n". $message ."\n\nFrom: " . $fromName ."\n" .  $fromMobile);
	    	
	    	return Redirect::to('vendors/' . $vendor->genres->first()->slug . '/' . $vendor->slug)
						->with('message', 'Your enquiry has been submitted!');
		}
	}

	//
	public function smsAllVendor(){
		return;
		$vendors = Vendor::where('id', '>', 619)->get();
		foreach ($vendors as $vendor) {
		
			$message = 'iKahwin.my tampil dengan wajah baru! Sila periksa dan kemaskini profil perniagaan anda di '. url('vendors/'.$vendor->id) .' - Hubungi kami di 011-39051272';

			$sms = new NexmoMessage();

			// clean phone number
			$phoneNo = trim($vendor->phone);
			$phoneNo = str_replace('+6', '', $phoneNo);
			$phoneNo = str_replace('-', '', $phoneNo);
			$phoneNo = str_replace(' ', '', $phoneNo);
			$phoneNo = "+6" . $phoneNo;

	    	$sms->sendText( $phoneNo, '+601117225017', $message );

	    	TextMessage::create(['name' => 'iKahwin Team', 'mobile' => '+601117225017', 'vendor_id' => $vendor->id, 'message' => $message]);
	    }
	}

	public function postReport($cat, $slug){

		$rules = [
			'reason' => 'required',	
		];

		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {

			$vendor = Vendor::where('slug', '=', $slug)->first();
			$message = Input::get('message');
			$reason = Input::get('reason');
			$ip = $_SERVER['REMOTE_ADDR'];
			$user_id = Auth::check() ? Auth::user()->id : 0;

			Report::create(['user_id' => $user_id, 'reason' => $reason, 'vendor_id' => $vendor->id, 'message' => $message]);


	    	return Redirect::to('vendors/' . $vendor->genres->first()->slug . '/' . $vendor->slug)
						->with('message', 'We have received your report and will be reviewing them');
		}

	}

}
