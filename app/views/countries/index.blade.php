@extends('layouts.default')

@section('content')

<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">Places</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="places-content">
				<div class="row place-items">
					@foreach($countries as $country)
					@if( $country->festivals()->published()->count() > 0 )
					<div class="col-md-3 place-item">
						<div class="thumbnail">
							<img src="{{ asset($country->festivals()->published()->random()->first()->cover_img) }}">
							<div class="box-content">
								<h3><a href="{{ URL::to('festivals?country=' . $country->slug) }}">{{ $country->name }}</a></h3>
							</div>
						</div>
					</div>
					@endif
					@endforeach
				</div>				
			</div>
		</div>
		<div class="text-center">
			{{ $countries->links() }}
		</div>
	</div>
</div>

@stop