
	
	<div class="col-md-{{ 12 / $col_size }} vendor-item">
		<div class="thumbnail">
			<a href="{{ URL::to('vendors/'. $vendor->genres->last()->slug .'/' . $vendor->slug) . $queryString  }}">
			<img src="{{ asset( $vendor->cover_thumb ) }}" >
			</a>
			@if( isset($showtag))
				@if($vendor->subscription == 2)
					<img src="{{ asset( 'assets/img/premium.png') }}" class="vendor-tag" style=""/>
				@elseif($vendor->subscription == 1)
					<img src="{{ asset( 'assets/img/verified.png') }}" class="vendor-tag" style=""/>
				@else
					<img src="{{ asset( 'assets/img/unverified.png') }}" class="vendor-tag" style=""/>
				@endif
			@else
				@if($vendor->subscription == 2)
					<img src="{{ asset( 'assets/img/premium_thumb.png') }}" class="vendor-tag mini-tag" style=""/>
				@elseif($vendor->subscription == 1)
					<img src="{{ asset( 'assets/img/verified_thumb.png') }}" class="vendor-tag mini-tag" style=""/>
				@else
					<img src="{{ asset( 'assets/img/unverified_thumb.png') }}" class="vendor-tag mini-tag" style=""/>
				@endif
			@endif
			<div class="type">{{ trans( 'vendor.category-'. $vendor->genres->last()->slug) }}</div>
			<div class="box-content">
				
				<h3><a href="{{ URL::to('vendors/'. $vendor->genres->last()->slug .'/' . $vendor->slug) . $queryString  }}" class="ellipsis">{{ $vendor->name }}</a></h3>
				<div class="date">{{ $vendor->district }}, {{ $vendor->state }}</div>
				<div class="places">{{ $vendor->formatDate('start_date') }}</div>
				
			</div>

			
		</div>

	</div>

