<?php

class AdminCountriesController extends BaseController {

	public function getIndex()
	{
		$countries = Country::all();

		return View::make('admin/countries/index', ['countries' => $countries]);
	}


	public function postEdit()
	{	
		$id = Input::get('id');
		$country = Country::find($id);
		$country->cover_img = Input::get('cover_img');
		$country->description = Input::get('description');
		$country->traveltips = Input::get('traveltips');
		$country->save();

		return Response::json($country );
	}
}