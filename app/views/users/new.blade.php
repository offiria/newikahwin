@extends('layouts.default')

@section('content')

@if($role == 'vendor')
<div class="container">
	<div class="row">
		<div class="col-md-12">
		<div class="stepwizard">
		    <div class="stepwizard-row">
		        <div class="stepwizard-step">
		            <button type="button" class="btn btn-primary btn-circle">1</button>
		            <p>Daftar Akaun</p>
		        </div>
		        <div class="stepwizard-step">
		            <button type="button" class="btn btn-default btn-circle">2</button>
		            <p>Maklumat Vendor</p>
		        </div>
		        <div class="stepwizard-step">
		            <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
		            <p>Pilih Pakej</p>
		        </div> 
		    </div>
		</div>
		</div>					
	</div>
</div>
@endif

<section id="content">
	<div class="container">

		<div class="listing-header">
			<h1 class="listing-title">{{ $title }}</h1>
			<small></small>
		</div>

		<div class="row">
			<hr>
		</div>

		<div class="row">
			<div class="col-md-7 col-md-offset-1">


				
				<div class="inner-col login-content">

		

					{{ Form::open(array('class' => 'form-signin', 'method' => 'post')) }}

						{{ Form::token() }}

						<div class="form-group {{ Form::errorClass('firstname') }}">
							{{ Form::label('firstname', trans('vendor.register-name') , array('class' => 'control-label')) }}
							{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'placeholder' => 'Nama Penuh')) }}
							{{ Form::errorMsg('firstname') }}
						</div>

						{{--<div class="form-group {{ Form::errorClass('lastname') }}">
																			{{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}
																			{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) }}
																			{{ Form::errorMsg('lastname') }}
																		</div>--}}

						<div class="form-group {{ Form::errorClass('email') }}">
							{{ Form::label('email', trans('vendor.register-email'), array('class' => 'control-label')) }}
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'username@email.com')) }}
							{{ Form::errorMsg('email') }}
						</div>

						<div class="form-group {{ Form::errorClass('phone') }}">
							{{ Form::label('phone', trans('vendor.register-phone'), array('class' => 'control-label')) }}
							{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => '016-6661166')) }}
							{{ Form::errorMsg('phone') }}
						</div>

						<div class="form-group {{ Form::errorClass('password') }}">
							{{ Form::label('password', trans('vendor.register-password'), array('class' => 'control-label')) }}
							{{ Form::password('password', array('class' => 'form-control')) }}
							{{ Form::errorMsg('password') }}
						</div>

						<div class="form-group {{ Form::errorClass('password_confirmation') }}">
							{{ Form::label('password_confirmation', trans('vendor.register-password-confirm')) }}
							{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
							{{ Form::errorMsg('password_confirmation') }}
						</div>

						<div class="form-group form-action">
							{{ Form::submit('Continue', array('class' => 'btn btn-block btn-hollo btn-lg')) }}
						</div>

					{{ Form::close() }}

					@if($role != 'vendor')
					<span class="text-separator">or</span>

					<a class="fb-login btn btn-lg btn-block btn-hollo" href="{{ URL::to('login/facebook') }}"><i class="fa fa-facebook"></i> Login with Facebook</a>
					@endif

				</div>
			</div>

			<div class="col-md-4">
				<img src="http://www.ikahwin.my/images/signup_side.png" style="
    width: 100%;
    margin-top: 50px;
">
				<!-- <h5>Why Should I Join iKahwin?</h5>
				<ul>
					<li>All the info you need in 1 place</li>
					<li>Personalized advice and suggestions</li>
					<li>Search & Book your vendor</li>
					<li>Ask us questions anytime</li>
					

				</ul> -->
			</div>
		</div>
	</div>
</section>

@stop