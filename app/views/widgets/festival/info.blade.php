<?php /*
<dl class="widget-package">
<dt>Location</dt>
<dd>{{{ $festival->district }}}, {{{ $festival->country->name }}}</dd>

<dt>Date</dt>
<dd>{{{ $festival->formatDate('start_date') }}}</dd>

<dt>Distance from you</dt>
<dd>New World Park, Singapore</dd>

<dt>Age restriction</dt>
<dd>
	<ul class="age list-inline list-unstyled">
		<li class="adult">
			<span><img src="<?php echo e(asset('assets/img/age-male.png')); ?>"></span>
			<span><img src="<?php echo e(asset('assets/img/age-female.png')); ?>"></span>
			<span class="age-checked"><img src="<?php echo e(asset('assets/img/age-checked.png')); ?>"></span>
		</li>
		<li class="child">
			<span><img src="<?php echo e(asset('assets/img/age-male-child.png')); ?>"></span>
			<span><img src="<?php echo e(asset('assets/img/age-female-child.png')); ?>"></span>
			<span class="age-remove"><img src="<?php echo e(asset('assets/img/age-remove.png')); ?>"></span>
		</li>
	</ul>
</dd>

<button class="btn btn-hollo" data-toggle="modal" data-target="#packageModal">
  Get me there, now
</button>
</dl> 
*/ ?>

@if($festival->packages && $festival->allow_ticketsale)
<div class="ribbon-stick">
	<h4 class="ribbon ribbon-black">
		<strong class="ribbon-content">Get Me There!</strong>
	</h4>
</div>

<div class="widget-package">
	<ul class="list-unstyled">
		<li>
			<h3>Customised Luxury Package</h3>
			<a href="{{ URL::to('vendors/' . $festival->slug . '/packages#customised-package') }}" class="btn btn-green">Customize Now</a>
		</li>
		@foreach($festival->packages as $package)
		<li>
			<h3>{{ $package->name }}</h3>
			<h5>{{ $package->excerpt }}</h5>
			<a href="{{ URL::to('vendors/' . $festival->slug . '/packages#package-' . $package->id) }}" class="btn btn-green"> {{ $package->price }}</a>
		</li>
		@endforeach
	</ul>
</div>
@endif