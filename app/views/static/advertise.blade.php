@extends('layouts.default')

@section('content')


<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">Sertai iKahwin sekarang!</h1>
		<small></small>
	</div>

	<div class="row">
		<hr/>
	</div>
</section>

<section>
<div class="container">
	<ul class="whyjoin clearfix list-unstyled">
		<li class="col-md-6">
			<div class="icons pull-left">
				<i class="fa fa-globe"></i>
			</div>
			<div class="content">
				<h3>Lebih 100,000 pembaca</h3>
				<p>iKahwin.my mempunyai lebih dari 100,000 pembaca setiap bulan, terutamanya dari golongan muda. Pembaca kami amat mahir menggunakan internet untuk melakukan carian dalam persiapan perkahwinan mereka.</p>
			</div>
		</li>
		<li class="col-md-6">
			<div class="icons pull-left">
				<i class="fa fa-mobile"></i>
			</div>
			<div class="content">
				<h3>SMS terus dari pelanggan</h3>
				<p>Bakal pengantin boleh menghantar SMS secara percuma, terus ke telefon bimbit anda. Vendor yang telah disahkan boleh menerima sehingga 30 pertanyaan setiap bulan manakala premium vendor pula boleh menerima sehingga 100 pertanyaan sebulan.	</p>
			</div>
		</li>
		<li class="col-md-6">
			<div class="icons pull-left">
				<i class="fa fa-photo"></i>
			</div>
			<div class="content">
				<h3>Muat-naik gambar produk anda</h3>
				<p>Gambar yang menarik merupakan tarikan utama pembaca kami. iKahwin membolehkan para vendor memuat-naik sebanyak mana gambar yang ada tanpa had (premium vendor).</p>
			</div>
		</li>
		<li class="col-md-6">
			<div class="icons pull-left">
				<i class="fa fa-github"></i>
			</div>
			<div class="content">
				<h3>"Branding"</h3>
				<p>A large number of employees are dedicated to data entry and paper processing. Managers waste valuable time dealing with paperwork. Eliminate all of these tasks and significantly reduce your labour costs.</p>
			</div>
		</li>
	</ul>
	<hr/>
</div>
</section>


<div class="container">
	<div class="about" style="margin-top:30px">


		<Div class="row">

		</div>

		<div class="row">
			<div class="col-md-12 about-content item-content pricing-table">
				    <div class="row">
				        <div class="col-xs-12 col-md-4">
				            <div class="panel panel-primary">
				                <div class="panel-heading">
				                    <h3 class="panel-title">
				                        Regular Vendor</h3>
				                </div>
				                <div class="panel-body">
				                    <div class="the-price">
				                        <h1>
				                            RM 0.00<span class="subscript"></span></h1>
				                        <small></small>
				                    </div>
				                    <table class="table  table-striped">
				                        <tr>
				                            <td>
				                                2 SMS/month
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                1 Album/20 Photos
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                -
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                No search result priority
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                -
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                SEO Optimized Page
				                            </td>
				                        </tr>
				                    </table>
				                </div>
				                <div class="panel-footer">
				                    <a href="{{ url('register/vendor') }}" class="btn btn-success" role="button">Sign Up</a>
				                    </div>
				            </div>
				        </div>
				        <div class="col-xs-12 col-md-4">
				            <div class="panel panel-success">
				                <div class="cnrflash">
				                    <div class="cnrflash-inner">
				                        <span class="cnrflash-label">MOST
				                            <br>
				                            POPULAR</span>
				                    </div>
				                </div>
				                <div class="panel-heading">
				                    <h3 class="panel-title">
				                        Verified Vendor</h3>
				                </div>
				                <div class="panel-body">
				                    <div class="the-price">
				                        <h1>
				                            RM30<span class="subscript">/mo</span></h1>
				                        <small></small>
				                    </div>
				                    <table class="table table-striped">
				                        <tr>
				                            <td>
				                                30 SMS/month
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                Unlimited Album/Photo upload
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                -
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                Search result priority
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                -
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                SEO Optimized Page
				                            </td>
				                        </tr>
				                    </table>
				                </div>
				                <div class="panel-footer">
				                    <a href="{{ url('register/vendor') }}" class="btn btn-success" role="button">Sign Up</a>
				                    </div>
				            </div>
				        </div>
				        <div class="col-xs-12 col-md-4">
				            <div class="panel panel-info">
				                <div class="panel-heading">
				                    <h3 class="panel-title">
				                        Premium Vendor</h3>
				                </div>
				                <div class="panel-body">
				                    <div class="the-price">
				                        <h1>
				                            $50<span class="subscript">/mo</span></h1>
				                        <small></small>
				                    </div>
				                    <table class="table table-striped">
				                        <tr>
				                            <td>
				                                100 SMS/month
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                Unlimited Album/Photo upload
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                Featured in our top vendor list
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                Search result priority
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
				                                Linked from related blog post
				                            </td>
				                        </tr>
				                        <tr class="">
				                            <td>
				                                SEO Optimized Page
				                            </td>
				                        </tr>
				                    </table>
				                </div>
				                <div class="panel-footer">
				                    <a href="{{ url('register/vendor') }}" class="btn btn-success" role="button">Sign Up</a> </div>
				            </div>
				        </div>
				    </div>
				


			</div>

		</div>
	</div>
</div>

@stop