<?php

use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		for ($i=0; $i < 100; $i++) { 
	    	$order = Order::create([
	    		'name' => $faker->name,
	    		'email' => $faker->email,
	    		'phone' => $faker->phoneNumber,
	    		'product' => 'PRODUCT',
	    		'penghantaran' => 'PENGHANTARAN',
	    		'total' => 0,
	    		'payment_method' => '',
	    		'address' => $faker->address,
	    		'note' => $faker->address,
	    		'tracking_code' => $faker->randomNumber(10)
	    	]);
    	}
	}
}