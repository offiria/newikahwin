<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUsersRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$vendors = Vendor::all();

		foreach ($vendors as $vendor) {
			// find user with similar email and set their roles
			$user = User::where('email', '=', $vendor->email )->first();
			if($user){
				$user->roles()->sync([2,4]);
				$vendor->user_id = $user->id;
				$vendor->save();
			}
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
