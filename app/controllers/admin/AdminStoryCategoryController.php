<?php

class AdminStoryCategoryController extends BaseController {

	public function getIndex()
	{
		$categories = StoryCategory::orderBy('id', 'DESC')->paginate(20);

		return View::make('admin/stories/categories/index', ['categories' => $categories]);
	}

	public function getCreate()
	{
		$festivals = Vendor::all()->lists('name','id');
		$categories = StoryCategory::all()->lists('name','id');

		return View::make('admin/stories/categories/create-edit', ['vendors' => $festivals, 'categories' => $categories]);
	}

	public function getEdit($id)
	{
		$category = StoryCategory::find($id);

		return View::make('admin/stories/categories/create-edit', ['category' => $category]);
	}

	public function postEdit($id = null)
	{
		$rules = [
			'name' => 'required'
		];

		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {
			// clean input
			$input['story'] = Purifier::clean($input['story']);
			
			$story = StoryCategory::firstOrCreate(['id' => $id]);
			$story->update($input);

			if($story->save()) {
				if($id) {
					return Redirect::back()
						->with('message', 'Story category updated');
				} else {
					return Redirect::to('admin/stories')
							->with('message', 'The story category has been created');
				}
			}
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}
}