<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Story extends \BaseModel implements SluggableInterface {
	
	use SluggableTrait;
	
	protected $sluggable = ['build_from' => 'title', 'save_to' => 'slug'];
	protected $fillable = ['artist_id', 'festival_id', 'start_date', 'category_id', 'user_id', 'title', 'story', 'published',  'excerpt', 'metakey', 'metadesc',
		'featured'];


	
	public function category()
	{
		return $this->belongsTo('StoryCategory');
	}

	public function author() {
		return $this->belongsTo('User', 'user_id', 'id');
	}

	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');    
	}
	
	public function tags()
	{
		return $this->morphToMany('Tag', 'taggable');
	}

	public function photos()
	{
		return $this->morphMany('Photo', 'imageable');
	}

	public function scopePublished($query){
        return $query->where('published', '=', 1);
    }

	public function scopeFeatured($query)
	{
		return $query->where('featured', '=', '1');
	}

	// Return recommended story based on the given story
	// just search within the same category for bow
	public function scopeRecommended($query, $story){
		$storyCategory = StoryCategory::find( $story->category->id );
		$query->where('category_id', '=', $storyCategory->id);
		return $query;
	}

	// some random 5 post from the last 20
	public function scopePopular($query){
		return $query;
	}

	/**
	 * Return festivals only with the given tag
	 */
	public function scopeSearchWithTag($query, $tag)
	{
		$tagObj = Tag::where('name', '=', $tag)->first();
		return $query
			->join('taggables', 'taggables.taggable_id', '=', 'stories.id')
			->where('taggables.taggable_type', '=', 'Story')
			->where('taggables.tag_id', '=', $tagObj->id)
			->groupBy('stories.id');
	}

	public function getVideoUrls(){
        $message = strip_tags($this->story);

        $urls = array();
        // Get all links from message
        // From, stackoverflow.com/questions/5830387/php-regex-find-all-youtube-video-ids-in-string
        preg_match_all('~
            # Match non-linked youtube URL in the wild. (Rev:20111012)
            https?://         # Required scheme. Either http or https.
            (?:[0-9A-Z-]+\.)? # Optional subdomain.
            (?:               # Group host alternatives.
              youtu\.be/      # Either youtu.be,
            | youtube\.com    # or youtube.com followed by
              \S*             # Allow anything up to VIDEO_ID,
              [^\w\-\s]       # but char before ID is non-ID char.
            )                 # End host alternatives.
            ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
            (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
            (?!               # Assert URL is not pre-linked.
              [?=&+%\w]*      # Allow URL (query) remainder.
              (?:             # Group pre-linked alternatives.
                [\'"][^<>]*>  # Either inside a start tag,
              | </a>          # or inside <a> element text contents.
              )               # End recognized pre-linked alts.
            )                 # End negative lookahead assertion.
            [?=&+%\.\w-]*        # Consume any URL (query) remainder.
            ~ix',
                $message, $matches);
                
        
        if( isset($matches) && !empty($matches) ){
            $urls = array_merge($urls, $matches[0]);
        }
        
        // match vimeo
        preg_match_all('~
            https?://(www\.)?vimeo\.com/(\d+)
            ~ix',
                $message, $matches);
        
        if( isset($matches) && !empty($matches) ){
            $urls = array_merge($urls, $matches[0]);
        }
        
        return $urls;
    }

    public function getVideoId()
	{
		$url = $this->getVideoUrls();

		preg_match_all('~
	        # Match non-linked youtube URL in the wild. (Rev:20111012)
	        https?://         # Required scheme. Either http or https.
	        (?:[0-9A-Z-]+\.)? # Optional subdomain.
	        (?:               # Group host alternatives.
	          youtu\.be/      # Either youtu.be,
	        | youtube\.com    # or youtube.com followed by
	          \S*             # Allow anything up to VIDEO_ID,
	          [^\w\-\s;]       # but char before ID is non-ID char.
	        )                 # End host alternatives.
	        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
	        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
	        (?!               # Assert URL is not pre-linked.
	          [?=&+%\w]*      # Allow URL (query) remainder.
	          (?:             # Group pre-linked alternatives.
	            [\'"][^<>]*>  # Either inside a start tag,
	          | </a>          # or inside <a> element text contents.
	          )               # End recognized pre-linked alts.
	        )                 # End negative lookahead assertion.
	        [?=&+%\w]*        # Consume any URL (query) remainder.
	        ~ix', 
			$url[0], $matches);
		
    	if( isset($matches) && !empty($matches[1]) ){
    		return $matches[1][0];
		}
		
		return false;
	}
}