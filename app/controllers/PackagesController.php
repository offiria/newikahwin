<?php

class PackagesController extends BaseController {

	public function getView($festivalSlug)
	{
		$festival = Vendor::where('slug', '=', $festivalSlug)->first();

		return View::make('packages.view', ['festival' => $festival]);
	}

	public function postCreateOrder($festivalSlug)
	{
		$rules = array(
			);

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->passes()) {
			$festival = Vendor::where('slug', '=', $festivalSlug)->first();

			$packageOrder = new PackageOrder();
			$packageOrder->festival_id = $festival->id;
			$packageOrder->package_id = Input::get('package_id');
			$packageOrder->departure_date = Input::get('departure_date');
			$packageOrder->return_date = Input::get('return_date');
			$packageOrder->adults = Input::get('adults');
			$packageOrder->children = Input::get('children');
			$packageOrder->fullname = Input::get('fullname');
			$packageOrder->phone = Input::get('phone');
			$packageOrder->email = Input::get('email');
			$packageOrder->save();

			// Notify admin
			Mail::send(['html' => 'emails.packages.order'], ['packageOrder' => $packageOrder], function($message) use ($packageOrder)
			{
				$packageName = (isset($packageOrder->package->name)) ? $packageOrder->package->name : 'Customized Package';

			    $my = Auth::user();
			    $message->from('noreply@festivalguideasia.com', 'FestivalGuideAsia.com');
			    $message->to('travel@festivalguideasia.com', 'Admin')->subject('New order [#' . $packageOrder->id . '] ' . $packageName);
			});

			$packageEl = Input::has('package_id') ? '#package-' . $packageOrder->package_id : '#customised-package';

			return Redirect::to('festivals/' . $festival->slug . '/packages' . $packageEl)
				->with('message-package', 'Your order has been submitted');
		} else {
			return Redirect::back()
				->withInput()
				->withErrors($validation);
		}
	}
}
