<?php

class VendorGenre extends \Eloquent {
	protected $fillable = ['name', 'description', 'vendor_id'];
}