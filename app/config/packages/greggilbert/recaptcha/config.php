<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LcF5foSAAAAAGTu1XL5hnB6PGrfHs_AVxv-89AU',
	'private_key'	=> '6LcF5foSAAAAADg-bX5S9x2W1zgVLkPPAm5EohyS',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	'options' => array(
        'theme' => 'white',
    ),
	
);