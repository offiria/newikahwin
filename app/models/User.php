<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Toddish\Verify\Models\User as VerifyUser;

class User extends VerifyUser implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['firstname', 'lastname', 'email', 'description', 'link', 'company', 'phone'];

	public function allowManageUser(){
		return User::find( Auth::user()->id)->is('administrator');
	}

	public function allowManageFestival(){
		return User::find( Auth::user()->id)->is('vendor') || User::find( Auth::user()->id)->is('administrator');
	}

	public function allowManageStory(){
		 return User::find( Auth::user()->id)->is('contributor') || User::find( Auth::user()->id)->is('administrator');
	}

	public function getRole()
	{
		$roles = $this->roles->toArray();
		if( ! empty($roles)) {
			// Since the user can only have one role, return the first
			return $roles[0]['id'];
		}
	}


	public function likes()
    {
    	return $this->hasMany('Like', 'user_id')->orderBy('updated_at', 'desc');
    }

	public function getFullname()
	{
		return $this->firstname . ' ' . $this->lastname;
	}

}
