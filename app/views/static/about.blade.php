@extends('layouts.default')

@section('content')
<div class="container">
	<div class="listing-header">
		<h1 class="listing-title">About Us</h1>
		<small></small>
	</div>

	<div class="row">
		<hr/>
	</div>
</section>


<div class="container">
	<div class="about">
		<div class="row">
			<div class="col-md-12 about-content item-content">

				<div class="about-secondary">
					<p>
						Kahwin was founded on 6th January 2013. Today, we are a leading wedding planning website in Malaysia and listed over 533 over vendors on our website. Our office is located at Jacaranda, Cyberjaya.
</p>
<p>Over the months, we have worked really hard on gathering all the useful informations about wedding and uploaded it online. This fast growing startup culture has won us the best subscription and marketplace startup website during the MAD Tech Ventures 2013.
					</p>

				
				</div>

				
				<div class="about-team">
					<h2 class="text-center">The Team</h2>

					<div class="group-inner clearfix">
					<div class="row">
						<div class="col-md-3 ">
								<div><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-frc3/285148_10151282669510127_322228688_n.jpg" height="220" width="220"></div>
								<h4>Azmierul Che Mat</h4>
							<p class="description" style="font-size:16px">Chief Wedding Maker and co-founder of iKahwin.my. He is responsible on business development,networking, sales and marketing of the company. A self-taught developer and love to fold origami during his past time.</p>
						</div>
						<div class="col-md-3 ">
								<div><img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-prn1/555194_10151214100693931_1187005549_n.jpg" height="220" width="220"></div>
								<h4>Charmaine Nash</h4>
							<p class="description" style="font-size:16px">Charmaine is the Chief Content Officer, designer, and storyteller. She produces most of the interesting contents on iKahwin. Before joining iKahwin.my, Charmaine is a senior photographer at a family portrait studio.</p>
						</div>
						<div class="col-md-3 ">
								<div><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/205903_10150256227256564_8037437_n.jpg" height="220" width="220"></div>
								<h4>Azrul Rahim</h4>
							<p class="description" style="font-size:16px">Director and also Technical Advisor at iKahwin.my. He leads our developers to deliver user friendly website and mentoring the founding team on building a fast growing startup. Azrul has been in the software business for more than 15 years.</p>
						</div>
						<div class="col-md-3 col-last">
								<div><img src="http://www.ikahwin.my/images/sidebar/Untitled-2.jpg" height="220" width="220"></div>
								<h4>Fakaruzaman Jusoh</h4>
							<p class="description" style="font-size:16px">Chief Operation Officer and co-founder of iKahwin.my. His works mostly revolves on the operations and day to day activities. Prior to joining iKahwin, he owns two restaurants in Jerteh. Faka also has 5 years of experience running a small medium businesses.</p>
						</div>
					</div>
				</div>
					
				</div>
			</div>

		</div>
	</div>
</div>

@stop