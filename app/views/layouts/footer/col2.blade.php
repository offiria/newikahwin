<h3>Our products</h3>
<p>
	{{ trans('vendor.footer-products') }}
</p>

<ul class="menu">
<li class=""><a href="{{ url('order/weddingstamp')}}">Order Wedding Stamps</a></li>
<li class=""><a href="{{ url('order/weddingcard')}}">Order Wedding Cards</a></li>
<li class=""><a href="{{ url('order/thankyoutag')}}">Order Thank You Tags</a></li>
</ul>


<ul class=" navbar-nav social-media menu hidden-xs" style="width:100%">
	<li><a href="https://www.facebook.com/ikahwin.my" target="_blank"><i class="fa fa-facebook"></i></a></li>
	<li><a href="https://twitter.com/ikahwin" target="_blank"><i class="fa fa-twitter"></i></a></li>
	<li><a href="http://www.pinterest.com/ikahwin/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
	<li><a href="http://instagram.com/ikahwinmy" target="_blank"><i class="fa fa-instagram"></i></a></li>
</ul>