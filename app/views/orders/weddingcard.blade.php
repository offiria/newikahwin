@extends('layouts.default')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<img src="http://ikahwin.my/images/banners/coverformcard.png" style="width:100%">

			<!-- <h1 class="main-title">Borang Tempahan Kad Kahwin</h1> -->
			<div class="listing-header center-block" style="margin-bottom:30px">
				<h1 class="listing-title" style="text-align: center;">Borang Tempahan Kad Kahwin</h1>
				<small></small>
			</div>

<p>Isikan maklumat tempahan anda disini dan kemudian semak dahulu sebelum membuat tempahan agar maklumat yang diberikan betul. 
Sebarang masalah, kemusykilan, ataupun soalan boleh hubungi kami di sales@ikahwin.my ataupun 013 - 972 1080</p>
		<hr/>
		</div>
		<div class="col-md-8 col-md-offset-1">
			{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal clearfix', 'id' => 'orderForm')) }}
				
				<!-- Required details -->
				<h4 class="form-title">Nama pembeli</h4>
				<div class="fieldset">
					{{ Form::bootstrapInput('name', 'Name Penuh *', 'Cth: Nurul Khalisha', ['class' => 'form-control input-lg', 'placeholder' => 'Cth: Nurul Khalisha']) }}
					{{ Form::bootstrapInput('email', 'Email *') }}
					{{ Form::bootstrapInput('phone', 'Nombor Telefon *', 'Pastikan Kami boleh Whatsapp/SMS melalui nombor ini' ) }}
				</div>

				<h4 class="form-title">Order details</h4>
				<div class="fieldset">
					{{ Form::bootstrapDropdown('Pakej', 'Pakej Kad Kahwin', [
						'100pcs Kad Kahwin  - MYR 60.00',
						'500pcs Kad Kahwin  - MYR 300.00',
						'1000pcs Kad Kahwin  - MYR 550.00',
						'1500pcs Kad Kahwin  - MYR 750.00',
						'2000pcs Kad Kahwin  - MYR 950.00',
						' - ',
						'100pcs Kad Kahwin Tiket - MYR 90.00',
						'200pcs Kad Kahwin Tiket - MYR 180.00',
						'300pcs Kad Kahwin Tiket - MYR 270.00',
						'400pcs Kad Kahwin Tiket - MYR 360.00',
						'500pcs Kad Kahwin Tiket - MYR 450.00',
						'1000pcs Kad Kahwin Tiket - MYR 800.00',
						'1500pcs Kad Kahwin Tiket - MYR 1,200.00',
						'2000pcs Kad Kahwin Tiket - MYR 1,400.00'
						 ] ) }}


					{{ Form::bootstrapInput('Kod Design', 'Kod Design Kad Kahwin *', 'Cth: KK01 / KK02 / Kk03') }}
					{{ Form::bootstrapInput('Nama Pasangan', 'Nama Pasangan *', 'Cth: Farah Natasya & Hazim Afiq') }}
					{{ Form::bootstrapInput('Nama Ibu Bapa Pasangan', 'Nama Ibu Bapa Pasangan', 'Cth: Haji Hamzah Zainuddin & Hajah Fatimah') }}
					{{ Form::bootstrapInput('Tarikh Majlis', 'Tarikh Majlis', 'Cth: 11 Januari 2015 | 8 Zulhijjah 1946H') }}
					{{ Form::bootstrapTextarea('Alamat Majlis', 'Alamat Majlis') }}


					{{ Form::bootstrapRadio('Peta Lokasi', 'Peta Lokasi', [
						'No - MYR 0.00',
						'Yes - MYR 50.00'
						 ], 'Tambahan peta lokasi memerlukan lebih masa' ) }}

				</div>

				<h4 class="form-title">Aturcara Majlis</h4>
				<div class="fieldset">
					{{ Form::bootstrapInput('Waktu Jamuan', 'Waktu Jamuan', 'Cth: Jamuan Makan : 11:00 Pagi ~ 5.00 Petang') }}
					{{ Form::bootstrapInput('Ketibaan', 'Ketibaan Pengantin', 'Cth: Ketibaan Pengantin : 2.00 Petang') }}
					{{ Form::bootstrapTextarea('Nombor Untuk Dihubungi', 'Nombor Untuk Dihubungi', "Cth: Rumah: 03 -5192 9980<br/>Hj Samsudin: 012-23459811<br/>Hjh Sabariah : 012-8932289") }}
				</div>

				<!-- Required details -->
				<h4 class="form-title">Maklumat Penghantaran</h4>


				<div class="fieldset">

					{{ Form::bootstrapRadio('source', 'Dari mana anda tahu mengenai kami? *', [
						'Rakan-Rakan', 
						'Facebook Group',
						'iKahwin FB Page',
						'Instagram',
						'Blog iKahwin'] ) }}
						
					{{ Form::bootstrapTextarea('address', 'Alamat Penuh *', 'Pastikan alamat ditulis dengan betul. Jika terdapat kesalahan, anda mungkin terpaksa membayar kos penghantaran semula') }}

					{{ Form::bootstrapRadio('penghantaran', 'Penghantaran', [
						'Self Pickup Cyberjaya ( Hari Khamis Sahaja 9 Pagi - 5 Ptg ) - MYR 0.00',
						'Semenanjung - MYR 40.00',
						'Sabah / Sarawak - MYR 80.00',
						'Brunei (300pcs Below) - MYR 120.00',
						'Brunei (300pcs Above) - MYR 200.00',
						'Singapore (300pcs Below) - MYR 100.00',
						'Singapore (300pcs Above) - MYR 180.00',
						'Express Order (Malaysia) - MYR 150.00'
						 ] ) }}

					{{ Form::bootstrapDropdown('payment_method', 'Kaedah Pembayaran', [
						Order::PAYMETHOD_ONLINETRANSFER, 
						Order::PAYMETHOD_CREDITCARD,
						Order::PAYMETHOD_OFFLINE ] ) }}
				</div>

				{{ Form::totalAmount( '0.00', ['names']) }}
				<div class="col-md-8 col-md-offset-4">
				{{ Form::submit( 'Order Now', array('class' => 'btn btn-primary btn-lg')) }}
				</div>

			{{ Form::close() }}
		</div>
</div>

@stop