<?php

class ImportContent extends \Eloquent {
	protected $fillable = ['name', 'description', 'vendor_id'];

}