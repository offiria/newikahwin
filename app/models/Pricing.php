<?php

class Pricing extends \Eloquent {
	protected $fillable = ['id', 'name'];

	public function vendors()
	{
		return $this->belongsToMany('Vendor', 'vendor_pricing');    
	}
}