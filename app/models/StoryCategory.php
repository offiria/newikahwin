<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class StoryCategory extends \Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug'];
	protected $fillable = ['name', 'description'];

	public function story()
	{
		return $this->hasMany('Story', 'category_id');
	}
}