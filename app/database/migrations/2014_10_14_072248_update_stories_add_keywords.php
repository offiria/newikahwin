<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateStoriesAddKeywords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stories', function(Blueprint $table)
		{
			$table->text('metakey')->nullable();
			$table->text('metadesc')->nullable();
			
		});


		$importContents = ImportContent::where('catid', '=', 8)->get();
		foreach( $importContents as $data )
		{
			$story = Story::where('slug', '=', $data->id .'-'. $data->alias )->first();
			if($story){
				$story->metadesc = $data->metadesc;
				$story->metakey = $data->metakey;

				$story->save();
			}
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
