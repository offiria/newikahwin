<?php
use Carbon\Carbon;

class AdminOrdersController extends BaseController {

	public function getIndex()
	{
		// Append pagination link with filter queries
		View::composer(Paginator::getViewName(), function($view) {
			$query = array_except( Input::query(), Paginator::getPageName() );
			$view->paginator->appends($query);
		});

		$query = Order::query();


		if(Input::has('keyword')) {
			$keyword = Input::get('keyword');
			if(is_numeric($keyword))
				$query->where('phone', 'LIKE', '%'. $keyword);
			else
				$query->where('name', 'LIKE', '%'. $keyword .'%');
		}

		if(Input::has('product')) {
			$product = Input::get('product');
			$query->where('orders.product', '=', $product);
		}

		if(Input::has('paymethod')) {
			$paymethod = Input::get('paymethod');
			$query->where('orders.payment_method', '=', $paymethod);
		}

		if(Input::has('status')) {
			$status = Input::get('status');
			$query->where('orders.status', '=', $status);
		}

		if(Input::has('month')) {
			$month = Input::get('month');
			$date = Carbon::createFromFormat('Y-m-d', $month);
			$query->where('orders.created_at', '>=', (string)$date->startOfMonth() );
			$query->where('orders.created_at', '<=', (string)$date->endOfMonth() );
			echo $date->startOfMonth() . '-' . $date->endOfMonth();
		}

		$query->orderBy('orders.updated_at', 'desc');

		$orders = $query->paginate(50);

		// calculate total amount
		$allOrders = $query->get();
		$total = 0;
		foreach ($allOrders as $order) {
			$total += $order->total;
		}

		return View::make('admin/orders/index', ['orders' => $orders, 'total' => $total]);
	}

	public function getEdit($id)
	{
		$order = Order::find($id);

		return View::make('admin/orders/edit', ['order' => $order]);
	}


	public function getDelete($id)
	{
		$user = Auth::user();
		$order = Order::find($id);

		// check for permission
		if( !User::find( $user->id)->is('administrator') ){	
			if($user->id != $vendor->user_id){
				return Redirect::to('admin')->with('message','Permission denied');
			}
		}

		$order->delete();

		return Redirect::back()
			->with('message', 'Order deleted');
	}

	public function postEdit($id)
	{
		$order = Order::find($id);

		$order->note = Input::get('note');
		$order->data = Input::get('data');
		$order->address = Input::get('address');
		$order->tracking_code = Input::get('tracking_code');
		$order->update( Input::get() );
		$order->save();

		return Redirect::back()
			->with('message', 'Order updated');
	}
}
