<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		// get featured festival
		$featured = Vendor::premium()->random()->take(3)->get();
		
		$featuredStories = Story::featured()->take(3)->get();

		// Get genres sorted by the number of festivals
		$topGenres = Genre::select(array('genres.*', DB::raw('COUNT(*) genrecount')))
			->join('vendor_genre', 'vendor_genre.genre_id', '=', 'genres.id')
			->groupBy('vendor_genre.genre_id')
			// ->orderBy('genrecount', 'desc')
			->orderBy(DB::raw('RAND()'))
			->take(3)->get();

		// popular story
		$popular = Story::popular()->get()->random(3);
		$featuredStory = Story::featured()->get()->random(3);

		$latest = Story::orderBy('updated_at', 'desc')->take(3)->get();
		

		return View::make('home', ['featured' => $featured, 'stories' => $featuredStories, 
			'topGenres' => $topGenres, 'popularStories' => $popular, 'latestStories' => $latest, 'featuredStories' => $featuredStory,
			'isHomepage' => true]);
	}

}
