<?php

class TagsController extends BaseController {

	public function getTags()
	{
		$tags = Tag::all()->lists('name');

		return Response::json($tags);
	}
}
