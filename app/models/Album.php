<?php

class Album extends \Eloquent {
	protected $fillable = ['name', 'description', 'vendor_id'];

	public function photos()
    {
    	return $this->hasMany('Photo', 'album_id')->orderBy('updated_at', 'desc');
    }
}