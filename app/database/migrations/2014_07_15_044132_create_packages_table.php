<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('packages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('festival_id');
			$table->string('name');
			$table->string('excerpt');
			$table->text('description');
			$table->string('price');
			$table->timestamps();
		});

		Schema::create('photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('album_id')->default(0);
			$table->integer('imageable_id');
			$table->string('imageable_type');
			$table->string('path');
			$table->string('caption')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('packages');
		Schema::drop('photos');
	}

}
