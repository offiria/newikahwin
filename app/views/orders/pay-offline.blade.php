@extends('layouts.default')


@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-1">
			<h1>Terima Kasih!</h1>
<p>Maklumat tempahan telah kami terima, untuk proses tempahan En/Cik perlu
buat bayaran ke account seperti yang dipaparkan.</p>

<div class="well"><p>Aushlabs Sdn Bhd / CIMB Bank / 8003599571</p></div>

<p>OR</p>

<div class="well"><p>Aushlabs Sdn Bhd / Maybank Berhad / 512866170445</p></div>

<p>Sekiranya anda mempunyai sebarang masalah, boleh hubungi bahagian 
sokongan kami di sales@ikahwin.my ataupun 013 - 972 1080</p>

		</div>
</div>

@stop