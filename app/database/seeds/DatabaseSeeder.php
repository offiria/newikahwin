<?php
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		Config::set('mail.pretend', true);

		$this->call('UsersTableSeeder');
		$this->call('CountriesTableSeeder');
		$this->call('ContentTableSeeder');
		$this->call('OrdersTableSeeder');

		Config::set('mail.pretend', false);
	}

}
