@extends('layouts.default')

@section('content')

<div class="page-title no-title"></div>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="inner-col login-content">
					<h2 class="text-center">
						{{ $title }}
					</h2>

					{{ Form::open(array('url' => 'login', 'method' => 'post', 'class' => 'form-login clearfix')) }}

					{{ Form::token() }}

					<div class="form-group {{ Form::errorClass('email') }}">
						{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>'E-mail address')) }}
						{{ Form::errorMsg('email') }}
					</div>
					
					<div class="form-group {{ Form::errorClass('password') }}">
						{{ Form::password('password', array('class' => 'form-control', 'placeholder'=>'Password')) }}
						{{ Form::errorMsg('password') }}
					</div>

					<div class="form-group">
						{{ Form::submit('Sign In', array('class' => 'btn btn-lg btn-block btn-hollo')) }}
					</div>

					@if($redirect)
					{{ Form::hidden('redirect', $redirect) }}
					@endif

					{{ Form::close() }}
					<div class="form-link">
					<a href="{{ url('/login/reset')}}" class="text-center">Recover password</a>
					</div>

					<span class="text-separator">or</span>

					<a class="fb-login btn btn-lg btn-block btn-hollo" href="{{ URL::to('login/facebook') }}"><i class="fa fa-facebook"></i> Login with Facebook</a>

					
				</div>
			</div>
		</div>
	</div>
</section>

@stop