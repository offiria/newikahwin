@extends('layouts.default')


@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="well well-sm">
			<img src="http://ikahwin.my/ALL%20Background/weddingstamp.jpg" style="width:100%">
				</div>

			<!-- <h1 class="main-title">Borang Tempahan Wedding Stamp</h1> -->
			<div class="listing-header center-block" style="margin-bottom:30px">
				<h1 class="listing-title" style="text-align: center;">Borang Tempahan Wedding Stamp</h1>
				<small></small>
			</div>

			<p>Isikan maklumat tempahan anda disini dan kemudian semak dahulu sebelum membuat tempahan agar maklumat yang diberikan betul. Sebarang masalah, kemusykilan, ataupun soalan boleh hubungi kami di sales@ikahwin.my ataupun 013 - 972 1080</p>
		<hr/>
		</div>

		<div class="col-md-8 col-md-offset-1">
			{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal clearfix', 'id' => 'orderForm')) }}
				
				<!-- Required details -->
				<h4 class="form-title">Butiran Pembeli</h4>
				<div class="fieldset">
					{{ Form::bootstrapInput('name', 'Name Penuh *', 'Isikan nama penuh anda bukan nama glamor!',['class' => 'form-control input-lg', 'placeholder' => 'Cth: Nurul Khalisha']) }}
					{{ Form::bootstrapInput('email', 'Email *','Resit pembayaran dan pengesahan akan kami hantarkan disini.',['class' => 'form-control input-lg', 'placeholder' => 'Cth: khalisha@gmail.com']) }}
					{{ Form::bootstrapInput('phone', 'Nombor Telefon *',  'Pastikan Kami boleh Whatsapp/SMS melalui nombor ini',['class' => 'form-control input-lg', 'placeholder' => 'Cth: 013-9721080'] ) }}
				</div>

				<h4 class="form-title">Order details</h4>
				<div class="fieldset">
					{{ Form::bootstrapDropdown('Jenis stamp', 'Saiz stamp', [
						'PROMO A - Buy 2 Free 1 Rubber Stamp (Standard Size) + 50pcs Premium Tag - MYR 98.00',
						'PROMO B - Buy 2 Rubber Stamp (Standard Size) + FREE 100pcs Printed Tag - MYR 70.00',
						'PROMO C - Buy 2 Rubber Stamp (Large Size) - MYR 100',
						'  ',
						'4cm Round - MYR 40.00',
						'5cm Round - MYR 40.00',
						'6cm Round - MYR 60.00',
						'7cm Round - MYR 60.00',
						'  ',
						'3cm x 6cm - MYR 40.00',
						'3cm x 8cm - MYR 40.00',
						'4cm x 6cm - MYR 40.00',
						'4cm x 8cm - MYR 40.00',
						'5cm x 7cm - MYR 40.00',
						'5cm x 10cm - MYR 40.00',
						'7cm x 9cm - MYR 60.00',
						'  ',
						'4cm x 4cm  - MYR 40.00',
						'5cm x 5cm - MYR 40.00',
						'6cm x 6cm - MYR 60.00',
						'7cm x 7cm - MYR 60.00',
						'  ',
						'Stamp Pad Large - MYR 12.00'
						] ) }}

					{{ Form::bootstrapInput('Kod Design', 'Kod Design *', 'Pilih daripada lebih 40 reka-corak menarik dari laman <a target="_blank" href="https://www.facebook.com/media/set/?set=a.1573308432895208.1073741827.1571059343120117&amp;type=3">Facebook kami</a> .Sekiranya anda order lebih dari 2 rubber stamp, tulis kod design kesemua didalam kotak ini.',['class' => 'form-control input-lg', 'placeholder' => 'Cth: WS07, WS09, WS29'] ) }}

					{{ Form::bootstrapTextarea('What to write', 'Perkataan apakah yang ingin ditulis di rubber stamp? *', 'Perkataan apakah yang ingin ditulis di rubber stamp?' ) }}

					{{ Form::bootstrapRadio('Design', 'Design Sendiri', [
						'No (FREE) MYR 0.00', 
						'Yes - MYR 5.00',
						'Yes (2 Stamps) - MYR 10.00',
						'Yes (3 Stamps) - MYR 15.00',] ) }}

					{{ Form::bootstrapRadio('approval', 'Approval ( Kami akan hantar draft design )', [
						'WhatsApp', 
						'Email',
						'Tak perlu tunggu approval (Express)'] ) }}

					{{ Form::bootstrapRadio('source', 'Dari mana anda tahu mengenai kami? *', [
						'Rakan-Rakan', 
						'Facebook Group',
						'iKahwin FB Page',
						'Instagram',
						'Blog iKahwin'] ) }}
				</div>

				<!-- Required details -->
				<h4 class="form-title">Maklumat Penghantaran</h4>
				<div class="fieldset">

					{{ Form::bootstrapTextarea('address', 'Alamat Penuh *', 'Pastikan alamat ditulis dengan betul. Jika terdapat kesalahan, anda mungkin terpaksa membayar kos penghantaran semula' ) }}

					{{ Form::bootstrapRadio('penghantaran', 'Penghantaran *', [
						'Semenanjung (FREE) MYR .00', 
						'Sabah / Sarawak - MYR 5.00',
						'Brunei - MYR 35.00',
						'Singapore - MYR 40.00' ] ) }}

					{{ Form::bootstrapDropdown('payment_method', 'Kaedah Pembayaran', [
						Order::PAYMETHOD_ONLINETRANSFER, 
						Order::PAYMETHOD_CREDITCARD,
						Order::PAYMETHOD_OFFLINE ] ) }}
				</div>

				{{ Form::totalAmount( '0.00', ['names']) }}
				<div class="col-md-8 col-md-offset-4">
				{{ Form::submit( 'Order Now', array('class' => 'btn btn-primary btn-lg')) }}
				</div>

			{{ Form::close() }}

			<!-- <div class="row">
				<div class="col-md-4">
					<img class="img-responsive" src="{{ asset('testimonials/_1__Wedding_Stamp_1.png') }}"/>
				</div>

				<div class="col-md-4">
					<img  class="img-responsive" src="{{ asset('testimonials/_1__Wedding_Stamp_2.png') }}"/>
				</div>

				<div class="col-md-4">
					<img  class="img-responsive" src="{{ asset('testimonials/_1__Wedding_Stamp_3.png') }}"/>
				</div>
			</div> -->
		</div>
</div>



<!-- Design Modal -->
<div class="modal fade" id="stampSelectCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Select design</h4>
      </div>
      <div class="modal-body">
        <ul class="list-unstyled">

		  <li><a onclick="return wstamp_select_design('WS08')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS08.jpg"><span>WS08</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS29')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS29.jpg"><span>WS29</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS42')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS42.jpg"><span>WS42</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS49')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS49.jpg"><span>WS49</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS01')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS01.png"><span>WS01</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS02')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS02.png"><span>WS02</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS03')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS03.png"><span>WS03</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS04')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS04.png"><span>WS04</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS05')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS05.png"><span>WS05</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS06')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS06.png"><span>WS06</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS07')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS07.png"><span>WS07</span></a></li>
		  <li><a onclick="return wstamp_select_design('WS08')"><img class="img-responsive"  src="http://www.ikahwin.my/images/stamps/WS08.png"><span>WS08</span></a></li>



  </ul>
  <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
      	<span class="pull-left">Pilih daripada lebih 40 reka-corak menarik dari laman <a target="_blank" href="https://www.facebook.com/media/set/?set=a.1573308432895208.1073741827.1571059343120117&amp;type=3">Facebook kami</a></span>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
jQuery( document ).ready(function() {
  //if(screen.width >= 400){
  jQuery('input[name="Kod Design"]').css('width', '60%').css('display', 'inline').after('<a style="padding-left:20px;margin-left: 10px;margin-top: -8px;" href="#stampSelectCode" role="button" class="btn btn-default" data-toggle="modal">Pilih Design</a>');
  //}
});




function wstamp_select_design(code){
	var current = jQuery('input[name="Kod Design"]').val();
	if(current.length > 0){
		current += ', ';
	}
	jQuery('input[name="Kod Design"]').val(current + code);
	jQuery('#stampSelectCode').modal('hide')
	return true;
}
</script>

@stop


