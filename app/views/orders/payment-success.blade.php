@extends('layouts.default')
@section('content')


	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="inner-col new-complete">

					<div class="listing-header center-block" style="margin-bottom:30px">
						<h1 class="listing-title" style="text-align: center;">Terima Kasih. Bayaran telah diterima</h1>
						<small></small>
					</div>

					
					<p class="text-center alert alert-info">Terima kasih kerana mempercayai kami untuk memeriahkan lagi majlis perkahwinan anda. Kami akan memberikan yang terbaik untuk anda!
					<br />
					</p>

					<h4>Pertanyaan?</h4>
					<p>Bayaran telah diterima, sila semak email anda untuk resit bayaran. Sekiranya anda mempunyai sebarang masalah, boleh hubungi bahagian 
sokongan kami di <a href="mailto:sales@ikahwin.my">sales@ikahwin.my</a> ataupun 013 - 972 1080</p>


					<h4>Apa seterusnya?</h4>
					<p>Anda mungkin tertanya-tanya apa yang perlu dibuat seterusnya?</p>
					<div class="well">
						<p>1. Pereka grafik kami akan menghubungi anda (melalui SMS/WhatsApp) untuk pengesah rekaan.</p>
						<p>2. Setelah anda meluluskan (approved) "design" dan rekaan, Pasukan Pengeluaran akan memulakan proses pengeluaran.</p>
						<p>3. Setelah semuanya siap, kami akan memaklumkan "tracking code" untuk setiap penghantaran.</p>
					</div>

				</div>
			</div>
		</div>
	</div>

@stop